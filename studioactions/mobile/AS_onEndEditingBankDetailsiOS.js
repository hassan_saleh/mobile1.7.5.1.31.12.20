function AS_onEndEditingBankDetailsiOS(eventobject, changedtext) {
    return AS_TextField_eeabdaa8cf0b48d7b085bb94938a2bda(eventobject, changedtext);
}

function AS_TextField_eeabdaa8cf0b48d7b085bb94938a2bda(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxBeneBankDetails.text !== "") {
        frmAddExternalAccountKA.flxUnderlineBankDetails.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineBankDetails.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblBeneBankDetails", frmAddExternalAccountKA.tbxBeneBankDetails.text);
}