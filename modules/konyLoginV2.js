kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.log = kony.sdk.mvvm.log || {};
kony.sdk.mvvm.constants = kony.sdk.mvvm.constants || {};
kony.boj = kony.boj || {};
kony.boj.selectedPayee = {};
var isDeviceRegistered = "T"; //it means ask for device registration
var iphoneModelforFaceId = ["iPhone X" , "iPhone XR", "iPhone XS", "iPhone XS Max", "iPhone 11", "iPhone 11 Pro", "iPhone 11 Pro Max" ];
var isDevRegDone = "F";
var ISSIRIENABLEDAppLevel = "N";
var isSiriEnabled = "N";
var siriTransactionLimit = "";
var bojcontactnumber = "";
var bojemailID = "";
var bojWEBSITE = "";
var bojJMP_Limit = 0.0;
var CURRENCY = "";
var LINKEDIN = "";
var TWITTER = "";
var FACEBOOK = "";
var INSTAGRAM = "";
var YOUTUBE = "";
var serverAppVersion = 0.0;
var custid = "";
var loginFlowRN = false;
var user_id = "";
var sendUnamecase = "";
var TnCFlag = "F";
var tncVersion = "0";
var flagClicked = "";
var cmpCustIDRes = "";
var subMenuFlag = true;
var tncAcceDate = "";
var deviceid = "";
var gblEntryFormForSettings = "";
var EstmtpopupFlag = true;
var p_primary_email_id = "";
var p_sec_email_id = "";
var releaseNoteFlag = "";
var gblforceflag = false;
var forceChangeUserNameflag = "N";
var forceChangePasswordflag = "N";
var serviceCallIOSOBJ = "";
var apptype = {
  "APPTYPE": "MFAPP"
};
var gblDefaultAccTransferCurr = "";
kony.sdk.mvvm.KonyApplicationContext.init(apptype);
kony.sdk.mvvm.userObj = {};
var menuIconForceTouch = false;
var isLoggedIn = false;
var defaultPage = "frmAccountsLandingKA";
var frmauthMode = "None";
var diffFlowFlag = "";
var channel = "MOBILE";
var gblOtpFromforRemovingOTP = "";
var engOTPmsg = "";
var OTPHashCode = "N/ZfZ9Fd/76"; //"x70tNQCsjgK";
var araOTPmsg = "";
var devlistSelectedObj = "";
var engSUNMmsg = "";
var araSUNMmsg = "";
var userName = "";
var errorHeaderText = geti18Value("i18n.common.internalHeader");
var excLogcall = false;
var gblDownloadPDFFlow = false;
var gbldownloadPDFFlag  = "PDF";
var gblsubacclist = "";
var en_ReleaseNotes = "";
var ar_ReleaseNotes = "";
var RELNOTES_Heading_EN = "";
var RELNOTES_Heading_AR = "";
var gblmapSearchKeypad = true;
//var gblPayAppFlow = false;
var model = kony.os.deviceInfo().model;
var  gblLaunchModeOBJ = {"lauchMode" : false , "accno" : "" , "iban" : "", "appFlow" :""};
var arrColour = ["EA4335", "FC8102", "2ECCFA", "3c34a8","167047","c027e0"];
var gblUnsubscribe = {
  "ksid" : "",
  "appId" : "",
  "deviceId" : "",
  "authToken" : ""
};
var savePDFObject;
var Siri_Limit = 0;
var gblOTPSMSAutoRead = true;
var SHARELINKURL       = "http://ec2-13-58-77-77.us-east-2.compute.amazonaws.com:8487/Share/ShareLink";
var bitly_parameters   ="longUrl=";
var bitly_requestURL   = "https://api-ssl.bitly.com/v4/shorten";

var SCANCARDLICENSEIOS = "sRwAAAEbY29tLmJhbmtvZmpvcmRhbi5ybW9iaWxlYXBwLdiHIH9GyChNIE7ZEnTJmf3J6r+Ee7+eins/6HabOG7bqJVPrtIYn5rtdxJotJM7mEm6MBs+5IMqQJdjs9tWZb2xOi41bJ+kImroOYLx5gt2e5XCg6WeriVenZp3TFAK4LQBLl9vakM2/t7+XeAZZePfRvTZ1wcSX3uK8i6J9QDUVrcfczo=";//ios license 2019 prod
var SCANCARDLICENSEANDROID  = "sRwAAAAaY29tLmJhbmtvZmpvcmRhbi5tb2JpbGVhcHDSnj0RPnGEukiSyLSCetJsJ1K4tPhK65A0ZvxMlioak1YozfpQ3FMd1DHDJU4XjTAzq9ur8AKc3t6kSiAEcSEVO2x92NwcYfwWfeeqZwtB7EoUk/EAcn6K9vQsS5BsJCXjHjEEiNUJ8LdbCnVjGGQih4duGXzTCk1Eht30UZimjQlZ3lEHtA==";//Android license 2019
var ksid = "";

function ondonePassword() {

  if (kony.sdk.isNetworkAvailable()) {
    frmLoginKA.lblClosePass.setVisibility(false);
    frmLoginKA.lblShowPass.setVisibility(false);
    animateLabel("DOWN", "lblPassword", frmLoginKA.passwordTextField.text);
    loginKeyboardInactive();
    frmLoginKA.passwordTextField.text = frmLoginKA.passwordTextField.text.trim();
    var username = frmLoginKA.tbxusernameTextField.text;
    var passsword = frmLoginKA.passwordTextField.text;
    username = username.trim();
    passsword = passsword.trim();
    if (username !== null && username !== "" && passsword !== null && passsword !== "") {
      if (frmLoginKA.passwordTextField.text !== null && frmLoginKA.passwordTextField.text !== "") {
        frmLoginKA.flxbdrpwd.skin = "skntextFieldDividerGreen";
      } else {
        frmLoginKA.flxbdrpwd.skin = "skntextFieldDivider";
      }
      deviceRegFrom = "login";
      frmSettingsKA.destroy();
      kony.sdk.mvvm.LoginAction(username, passsword, "U", "", "", "");
    } else {
      showInvalidCredLabel();
    }
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}


function onClickLogin() {
  if (kony.sdk.isNetworkAvailable()) {
    frmLoginKA.lblClose.setVisibility(false);
    frmLoginKA.lblClosePass.setVisibility(false);
    frmLoginKA.lblShowPass.setVisibility(false);
    var username = frmLoginKA.tbxusernameTextField.text.trim();
    var passsword = frmLoginKA.passwordTextField.text.trim();
    frmLoginKA.tbxusernameTextField.text = username;
    frmLoginKA.passwordTextField.text = passsword;
    if (username != null && username != "" && passsword != null && passsword != "") {
      if (frmLoginKA.passwordTextField.text !== null && frmLoginKA.passwordTextField.text !== "") {
        frmLoginKA.flxbdrpwd.skin = "skntextFieldDividerGreen";
      } else {
        frmLoginKA.flxbdrpwd.skin = "skntextFieldDivider";
      }
      deviceRegFrom = "login";
      frmSettingsKA.destroy();
      kony.sdk.mvvm.LoginAction(username, passsword, "U", "", "", "");
    } else {
      showInvalidCredLabel();
    }
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }

}



kony.sdk.mvvm.LoginAction = function(userName, password, Loginflow, custId, pin_pass, touchSuccessFlag) {
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  kony.sdk.mvvm.KonyApplicationContext.init();
  if (kony.retailBanking.util.validation.isValidUsername(userName) && kony.retailBanking.util.validation.isValidUsername(password)) {

    kony.print("Perf Log: username - start-->" + userName);
    kony.print("Perf Log: password - start->" + password);
    kony.print("Perf Log: Loginflow - start->" + Loginflow);
    kony.print("Perf Log: custId - start->" + custId);
    kony.print("Perf Log: pin_pass - start->" + pin_pass);
    kony.print("Perf Log: touchSuccessFlag - start->" + touchSuccessFlag);
    var flowFlag = Loginflow;
    var Token = Dvfn(kony.store.getItem("soft_token"));
    kony.print("Perf Log: Token - start->" + Token);

    var devID = deviceid;
    if (kony.boj.imeiPermissionFlag)
      imei = kony.os.deviceInfo().uid;
    else
      imei = "";
    var deviceName = kony.os.deviceInfo().model;
    var langSelected = kony.store.getItem("langPrefObj");
    kony.print("Language Selected while login:: " + langSelected);
    var Language = langSelected.toUpperCase();

    var currentAppVersion = parseFloat(appConfig.appVersion);
    kony.print("currentAppVersion :: " + currentAppVersion);

    var configParams = {
      "ShowLoadingScreenFunction": ShowLoadingScreen
    };
    var ipAddress = getDeviceIP();
    var params = {
      "authParams": {
        "username": userName,
        "password": password,
        "language": Language,
        "deviceId": devID,
        "devicedetail": deviceName,
        "Imei": imei,
        "flowFlag": flowFlag,
        "Token": Token,
        "custId": custId,
        "pin_pass": pin_pass,
        "ipAddress": ipAddress,
        "Loginchannel": channel,
        "TouchFlag": touchSuccessFlag,
        "currentAppVersion": currentAppVersion,
        "loginOptions": {
          "isOfflineEnabled": false
        }
      },
      "options": {
        "access": "online"
      },
      "identityServiceName": "CustomLogin",
      "configParams": configParams
    };
    kony.print("params->" + params);
    kony.print("params->" + JSON.stringify(params));
    kony.sdk.mvvm.KonyApplicationContext.appServicesLogin(params, applicationInitialSuccessCallback, applicationErrorCallback);

  } else {
    showInvalidCredLabel();
  }
};

function showInvalidCredLabel() {
  var currentForm = kony.application.getCurrentForm();
  if (currentForm.id === "frmLanguageChange" && frmLanguageChange.flxLogin.isVisible === true) {
    frmLanguageChange.lblInvalidCredentialsKA.text = geti18Value("i18n.login.invalidCredentials");
    frmLanguageChange.lblInvalidCredentialsKA.isVisible = true;
    frmLanguageChange.passwordTextField.text = "";
    currentForm.passwordTextField.secureTextEntry = true;
    frmLoginKA.lblShowPass.text = "A";
    frmLanguageChange.borderBottom.skin = "skntextFieldDividerOrange";
    frmLanguageChange.flxbdrpwd.skin = "skntextFieldDividerOrange";
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    return;
  }
  if (frmLoginKA.loginMainScreen.isVisible) {
    frmLoginKA.lblInvalidCredentialsKA.text = geti18Value("i18n.login.invalidCredentials");
    frmLoginKA.lblInvalidCredentialsKA.isVisible = true;
    frmLoginKA.passwordTextField.text = "";
    frmLoginKA.passwordTextField.secureTextEntry = true;
    frmLoginKA.lblShowPass.text = "A";
    frmLoginKA.borderBottom.skin = "skntextFieldDividerOrange";
    frmLoginKA.flxbdrpwd.skin = "skntextFieldDividerOrange";
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    return;
  } else if (frmLoginKA.loginMainPINScreen.isVisible) {
    clearProgressFlexLogin();
    login_pass = "";
    frmLoginKA.pinReTry.text = "";
    frmLoginKA.pinReTry.text = geti18Value("i18n.login.invalidPin");
    frmLoginKA.pinReTry.isVisible = true;
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    return;
  } else if (frmLoginKA.flxtouchIdAndrd.isVisible) {
    frmLoginKA.lblTryAgain.text = kony.i18n.getLocalizedString("i18n.touchId.tryAgain");
    frmLoginKA.flxTouchIdtryAgain.setVisibility(true);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }
}

function applicationInitialSuccessCallback() {
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  frmauthMode = "";

  if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance()) {
    var konyRef = kony.sdk.getCurrentInstance();
    var userData;
    var identityObject = konyRef.getIdentityService("CustomLogin");
    identityObject.getUserAttributes(successCallBack, errorCallBack);

    function successCallBack(response) {
      userData = response;
      //registerPush();
      kony.print("Inside Success callback identityObject response--> " + JSON.stringify(response));
      try {
        if (response.status != "F" && response.status != "" && response.custid != null && response.custid != "") {

          //Boj's App usage timeout
          try {
            kony.timer.schedule("bojTimer", timeCallback, kony.boj.retailBanking.globals.APP_USAGE_TIMEOUT, false);
          } catch (err) {
            kony.timer.cancel("bojTimer");
            kony.timer.schedule("bojTimer", timeCallback, kony.boj.retailBanking.globals.APP_USAGE_TIMEOUT, false);
          }

          kony.application.registerForIdleTimeout(kony.boj.retailBanking.globals.IDLE_TIMEOUT, timeOutCallback);
          kony.retailBanking.globalData.session_token = kony.sdk.getCurrentInstance().tokens["CustomLogin"].provider_token.value;
          c = kony.retailBanking.globalData.globals.configPreferences.currency;

          kony.print("response.custid -- >" + response.custid);
          setValues(response);

        } else {
          if (response.errorMsg !== null && response.errorMsg !== "") {
            kony.print("Error msg--> " + response.errorMsg);
            if (response.errorCode === "S0006") {
              customAlertPopup(geti18Value("i18n.common.alert"), kony.i18n.getLocalizedString("i18n.errorMsg.accountLocked"), popupCommonAlertDimiss, null, geti18nkey("i18n.settings.ok"), null);
            } else {

              if (frmLoginKA.loginMainScreen.isVisible || frmLanguageChange.flxLogin.isVisible) {
                var currentForm = kony.application.getCurrentForm();
                currentForm.lblInvalidCredentialsKA.text = response.errorMsg;
                currentForm.lblInvalidCredentialsKA.isVisible = true;
                currentForm.passwordTextField.text = "";
                currentForm.passwordTextField.secureTextEntry = true;
                frmLoginKA.lblShowPass.text = "A";
                currentForm.borderBottom.skin = "skntextFieldDividerOrange";
                currentForm.flxbdrpwd.skin = "skntextFieldDividerOrange";
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
              } else if (frmLoginKA.loginMainPINScreen.isVisible) {
                clearProgressFlexLogin();
                login_pass = "";
                frmLoginKA.pinReTry.text = response.errorMsg;
                frmLoginKA.pinReTry.isVisible = true;
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
              } else if (frmLoginKA.flxtouchIdAndrd.isVisible) {
                if (response.errorMsg.length > 50)
                  frmLoginKA.lblTryAgain.text = response.errorMsg;
                else
                  frmLoginKA.lblTryAgain.text = kony.i18n.getLocalizedString("i18n.touchId.tryAgain"); //response.errorMsg;
                frmLoginKA.flxTouchIdtryAgain.setVisibility(true);
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
              }

            }
          } else {
            kony.print("Error message is null");
            showInvalidCredLabel();
          }
        }
      } catch (err) {
        kony.print("in catch of successCallBack of identityObject : " + JSON.stringify(err));
        exceptionLogCall("successCallBack:IdentituOBj-LOgin", "UI ERROR", "UI", err);
        applicationErrorCallback(err);
      }
    }

    function errorCallBack() {
      kony.print("in errorCallBack of identityObject");
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      fetchApplicationPropertiesWithoutLoading();
      showInvalidCredLabel();
    }

  } else {
    kony.sdk.mvvm.log.error("Application is not initialized");
    applicationErrorCallback("Error during the user authentication for unknown reason");
    fetchApplicationPropertiesWithoutLoading();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    showInvalidCredLabel();
  }
}

function setValues(response) {
  autoTouchShowFlag = true;
  custid = response.custid;
  user_id = response.user_id;
  ksid = response.ksId;
  if (response.tncAcceDate !== null && response.tncAcceDate !== "") {
    tncAcceDate = response.tncAcceDate;
    kony.print("tncAcceDate-->" + tncAcceDate);
  }

  if (response.releaseNoteFlag !== null && response.releaseNoteFlag !== "") {
    releaseNoteFlag = response.releaseNoteFlag;
    kony.print("releaseNoteFlag-->" + releaseNoteFlag);
  }else{
    releaseNoteFlag = "";
  }

  //force change password flag
  if (response.forceChangePasswordflag !== undefined && response.forceChangePasswordflag !== null && response.forceChangePasswordflag !== "") {
    forceChangePasswordflag = response.forceChangePasswordflag;
    kony.print("forceChangePasswordflag-->" + forceChangePasswordflag);
  }else{
    forceChangePasswordflag = "N";
  }

  //force change user name flag
  if (response.forceChangeUserNameflag !== undefined && response.forceChangeUserNameflag !== null && response.forceChangeUserNameflag !== "") {
    forceChangeUserNameflag = response.forceChangeUserNameflag;
    kony.print("forceChangeUserNameflag-->" + forceChangeUserNameflag);
  }else{
    forceChangeUserNameflag = "N";
  }

  kony.print("custid : " + custid + "--> user_id : " + user_id);

  if (response.ShowPageFlag !== null && response.ShowPageFlag !== "") {
    isDeviceRegistered = response.ShowPageFlag;
    kony.print("ShowPageFlag-->" + isDeviceRegistered);
  }

  if (response.DevRegFlag !== null && response.DevRegFlag !== "") {
    isDevRegDone = response.DevRegFlag;
    kony.print("isDevRegDone-->" + isDevRegDone);

  }

  //Is siri enabled by user on this device
  if (response.is_siri_enabled !== undefined &&response.is_siri_enabled !== null && response.is_siri_enabled !== "") {
    isSiriEnabled = response.is_siri_enabled;
    kony.print("isSiriEnabled-->" + isSiriEnabled);
  }
  
  // siri transaction limit of SIRI user level 
  if (response.Siri_TransactionLimit !== undefined &&response.Siri_TransactionLimit !== null) {
    siriTransactionLimit = response.Siri_TransactionLimit;
    kony.print("siriTransactionLimit-->" + siriTransactionLimit);
  }
  
  
    
  if (response.soft_token !== "" && response.soft_token !== null) {
   try{
    var resSotToken = EVfn(response.soft_token);
    var storeData = isEmpty(resSotToken) ? "" : resSotToken;
    kony.store.setItem("soft_token", storeData);
    kony.print("soft token : " + response.soft_token);
    }catch(err){
          kony.print(err);
           exceptionLogCall("EVfn","UI ERROR Login","UI",err);
          
        } 
    siriObject.softToken = response.soft_token;
    siriObject.deviceID = deviceid;
    //#ifdef iphone
    com.siriSetup.sendSoftTokenToIOS(siriObject);
    //#endif
    //#ifdef ipad
    com.siriSetup.sendSoftTokenToIOS(siriObject);
    //#endif
  } else {
    siriObject.softToken = "";
    siriObject.deviceID = deviceid;
    kony.store.removeItem("soft_token");
    //#ifdef iphone
    com.siriSetup.sendSoftTokenToIOS(siriObject);
    //#endif
    //#ifdef ipad
    com.siriSetup.sendSoftTokenToIOS(siriObject);
    //#endif
  }
  if (response.is_touch_enabled != null && response.is_touch_enabled != "" && response.is_touch_enabled == "Y") {
    kony.store.setItem("isTouchSupported", "Y");
  } else {
    kony.store.removeItem("isTouchSupported");
  }
  if (response.is_pin_enabled != null && response.is_pin_enabled != "" && response.is_pin_enabled == "Y") {
    kony.store.setItem("isPinSupported", "Y");
  } else {
    kony.store.removeItem("isPinSupported");
  }

  if (response.TnCFlag !== "" && response.TnCFlag !== null) {
    TnCFlag = response.TnCFlag;
    kony.print("TnCFlag-->" + TnCFlag);
  }


  if (response.tncVersion !== "" && response.tncVersion !== null) {
    tncVersion = response.tncVersion;
    kony.print("tncVersion-->" + tncVersion);
  } else {
    tncVersion = "0";
  }

  if (response.Defaultpayment !== "" && response.Defaultpayment !== null) {
    gblDefaultAccPayment = response.Defaultpayment;
    kony.print("gblDefaultAccPayment-->" + gblDefaultAccPayment);
  }

  if (response.DefaultTransfer !== "" && response.DefaultTransfer !== null) {
    gblDefaultAccTransfer = response.DefaultTransfer;
    kony.print("gblDefaultAccTransfer-->" + gblDefaultAccTransfer);
  }

  if (response.QbalFlag !== "" && response.QbalFlag !== null) {
    gblQbalShowFlag = response.QbalFlag;
    kony.print("gblQbalShowFlag-->" + gblQbalShowFlag);
  }

  if (response.QtransFlag !== "" && response.QtransFlag !== null) {
    gblQtransFlagShowFlag = response.QtransFlag;
    kony.print("gblQtransFlagShowFlag-->" + gblQtransFlagShowFlag);
  }

  isLoggedIn = true;
  loginFlowRN = true;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  kony.sdk.mvvm.appInit(INSTANCE);
}



function timeCallback() {  
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); //geti18nkey("i18n.common.appusageTimeout")
  customAlertPopup(geti18nkey("i18n.common.appusageTimeout"), geti18nkey("i18n.common.AppUsaTimeText"), popupCommonAlertDimiss, "");
}


function applicationErrorCallback(err) {
  kony.print("applicationErrorCallback :: " + err);
  if (err.code !== null && err.code == "10007") {
    fetchApplicationPropertiesWithoutLoading();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }
  showInvalidCredLabel();
}

function ShowLoadingScreen() {
  kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
}

kony.sdk.mvvm.pinLogin = function(pinEntered) {
  kony.sdk.mvvm.KonyApplicationContext.init();
  var savedCredential = kony.sdk.mvvm.KonyApplicationContext.getRecentlyLoggedUserDetails();
  var username = DecryptValue(kony.store.getItem("userName"));
  var configParams = {
    "ShowLoadingScreenFunction": ShowLoadingScreen
  };
  var params = {
    "authParams": {
      "username": username,
      "pin": login_pass,
      "deviceId": deviceid,
      "loginOptions": {
        "isOfflineEnabled": false
      }
    },
    "options": {
      "access": "online"
    },
    "identityServiceName": "CustomLogin",
    "configParams": configParams
  };

  kony.sdk.mvvm.KonyApplicationContext.appServicesLogin(params, applicationInitialSuccessCallback, pinLoginErrorCallBack);

}

function pinLoginErrorCallBack(error) {
  kony.sdk.mvvm.log.error("failed to load app" + error);
  if (error instanceof kony.sdk.mvvm.Exception) {
    switch (error.code) {
      case 70:
      case 79:
      case 125:
        error.message = i18n_deviceConnectionError;
        return;
      case 25:
      case 69:
      case 12:
        login_pass = "";
        frmLoginKA.PinEntryLabel.setVisibility(false);
        frmLoginKA.pinReTry.setVisibility(true);
        animation_wrong_pin();
        kony.timer.schedule("loginTimer", clearProgressFlexLogin, 0.05, false);
        return;
      case 17:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 26:
        error.message = i18n_appLaunchError;
        break;
      default:
        login_pass = "";
        frmLoginKA.PinEntryLabel.setVisibility(false);
        frmLoginKA.pinReTry.setVisibility(true);
        animation_wrong_pin();
        kony.timer.schedule("loginTimer2", clearProgressFlexLogin, 0.05, false);
        return;

    }
    //alert(error.message);
  } else {}
  //alert(error.errorMessage);

}


function timeOutCallback() {
  kony.sdk.mvvm.LogoutAction();
  kony.retailBanking.globalData.session_token = "";
  frmLoginKA.show();
}

function loadConfigurations() {
  //USE THIS kony.retailBanking.globalData.globals.userObj.
  //
  kony.servicesapp.loadAndConfigureApp(kony.retailBanking.globalData.globals.userObj.role, kony.sdk.getCurrentInstance().customReportingURL.split('/services')[0], loadConfigurationSuccess);
}

function loadConfigurationSuccess() {

  if (PreferenceConfigHandeller.getInstance().getPreferenceValue("PREF_CURRENCY_CODE") !== null) {
    setConfigToGlobal();
  }
}

function setConfigToGlobal() {
  kony.retailBanking.globalData.globals.configPreferences.currency = PreferenceConfigHandeller.getInstance().getPreferenceValue("PREF_CURRENCY_CODE");
  kony.retailBanking.globalData.globals.configPreferences.rdc = PreferenceConfigHandeller.getInstance().getPreferenceValue("PREF_RDC_MODULE");
  kony.retailBanking.globalData.globals.configPreferences.accountInsight = PreferenceConfigHandeller.getInstance().getPreferenceValue("PREF_ACCOUNTS_INSIGHTS");
  kony.retailBanking.globalData.globals.configPreferences.newAccount = PreferenceConfigHandeller.getInstance().getPreferenceValue("PREF_NEWACCOUNT_MODULE");
  kony.retailBanking.globalData.globals.CurrencyCode = kony.retailBanking.globalData.globals.configPreferences.currency;

}

function loginshow() {
  frmLoginKA.show();
}

/*
----->>Logout Functionality Start <<<-----------
*/

kony.sdk.mvvm.LogoutAction = function() {

  ShowLoadingScreen();
  destroyForms();
   if(gblPayAppFlow && gblLaunchModeOBJ.lauchMode){ 
     kony.print("No clearGlobals");
   }else{
  clearGlobals();
   }
  isLoggedIn = false;

  kony.timer.cancel("bojTimer");
  if (kony.sdk.mvvm.isNetworkAvailabile()) {
    try {
      var serviceName = "BOJRestServices";
      var operationName = "logout";
      var headers = null;

      var inputParams = {
        "custId": custid
      };
      kony.print("Input params to logout" + JSON.stringify(inputParams));
      var mobileFabricConfiguration = kony.sdk.getCurrentInstance();
      mobileFabricConfiguration.integrationObj = mobileFabricConfiguration.getIntegrationService("BOJRestServices");
      mobileFabricConfiguration.integrationObj.invokeOperation(operationName, headers, inputParams, sucCallback, errCallback);

    } catch (e) {
      kony.print("in exception LogoutAction :: " + e);
      if(gblPayAppFlow && gblLaunchModeOBJ.lauchMode){  
    navtoForgotPasswordFloww();
      }else{
      sucCallback();
      }
    }
  } else {
    if(gblPayAppFlow && gblLaunchModeOBJ.lauchMode){  
    navtoForgotPasswordFloww();
      }else{
      sucCallback();
      }
  }

  function exit() {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.application.exit();
  }

  function sucCallback() {
    try {
       if(gblPayAppFlow && gblLaunchModeOBJ.lauchMode){  
    navtoForgotPasswordFloww();
      }else{
      kony.application.dismissLoadingScreen();
      fetchApplicationPropertiesWithoutLoading();
      frmLoginKA.show();
      }
    } catch (err) {
       if(gblPayAppFlow && gblLaunchModeOBJ.lauchMode){  
    navtoForgotPasswordFloww();
      }else{
      kony.application.dismissLoadingScreen();
      fetchApplicationPropertiesWithoutLoading();
      frmLoginKA.show();
      kony.print("Catch of Error in logout-->  " + err);
    }
    }
  }

  function errCallback(err) {
    try {
       if(gblPayAppFlow && gblLaunchModeOBJ.lauchMode){  
    navtoForgotPasswordFloww();
      }else{
      kony.print("fail to logout" + JSON.stringify(err));
      kony.application.dismissLoadingScreen();
      fetchApplicationPropertiesWithoutLoading();
      frmLoginKA.show();
      }
    } catch (error) {
       if(gblPayAppFlow && gblLaunchModeOBJ.lauchMode){  
    navtoForgotPasswordFloww();
      }else{
      kony.application.dismissLoadingScreen();
      fetchApplicationPropertiesWithoutLoading();
      frmLoginKA.show();
      kony.print("Error in logout-->  " + error);
      }
    }
  }
};

function clearGlobals() {
  standingIns = "";
  tAndCversion = "0";
  REG_JMP_MOB = "";
  tncVersion = "0";
  isDeviceRegistered = "T"; //it means ask for device registration
  isDevRegDone = "F";
  TnCFlag = "F";
  gblEntryFormForSettings = "";
  custid = "";
  user_id = "";
  gblOtpFromforRemovingOTP = "";
  cmpCustIDRes = "";
  forceChangeUserNameflag = "N";
  forceChangePasswordflag = "N";
  sendUnamecase = "";
  kony.retailBanking.globalData.globals.transferSearch = 0;
  removeSwipe();
  authImgArray = [];
  kony.retailBanking.globalData.session_token = null;
  isAdClosed = false;
  isBannerInfeedAdImageAvailable = false;
  bannerInfeedAdImageUrl = "";
  defaultPage = "frmAccountsLandingKA";
  menuIconForceTouch = false;
  isLoggedIn = false;
  tncAcceDate = "";
  kony.retailBanking.globalData.transfers = {};
  kony.retailBanking.globalData.accountsDashboardData = {};
  kony.retailBanking.globalData.accountsTransactionList = {};
  kony.retailBanking.globalData.accountsTransactionData = {};
  kony.retailBanking.globalData.accountsTransactionDataPreview = {};
  kony.retailBanking.globalData.accountsDashboardDataPreview = {};
  kony.retailBanking.globalData.acccountsUnfilteredData = [];
  kony.retailBanking.globalData.prCardLandinList = [];
  kony.retailBanking.globalData.deposits = {};
  gblLaunchModeOBJ = {"lauchMode" : false , "accno" : "" , "iban" : "", "appFlow" :""};
  enter_count = 0;
  devlistSelectedObj = "";
  pass = "";
  firstPass = "";
  diffFlowFlag = "";
  gblforceflag = false;
  flagClicked = "";
  gblsubacclist = "";
  subMenuFlag = "";
  //gblColor=[];
  gblColorArray = [];
  gblCardNum = "";
  gblCalculateFXLeft = false;
  gblCalculateFXRight = false;
  gblExchangeRatesFlag = false;
  frmMyMoneyListKA.flxLineChart.removeAll();
  kony.retailBanking.globalData.isCreditCardAvailable = false;
  kony.retailBanking.globalData.isDebitCardAvailable = false;
  kony.retailBanking.globalData.isWebchargeCardAvailable = false;
  kony.newCARD.applyCardsOption = false;
  CARDLIST_DETAILS.cardPin = "";
  previous_FORM = null;
  kony.retailBanking.globalData.prCreditCardPayList = [];
  selectedIndex_BULK_PAYMENT = [];
  frmUserSettingsMyProfileKA.lblSMSNumber.text = "";
  hasCasaAccounts = false;
}

/*
----->>Logout Functionality End <<<-----------
*/

function login_FROMLANGUAGE() {

  if (kony.sdk.isNetworkAvailable()) {
    frmLanguageChange.lblClosePass.setVisibility(false);
    frmLoginKA.lblShowPass.setVisibility(false);
    //animateLabel("DOWN", "lblPassword", frmLanguageChange.passwordTextField.text);
    loginKeyboardInactiveLang();
    //frmLanguageChange.passwordTextField.text = frmLanguageChange.passwordTextField.text.trim();
    var username = frmLanguageChange.tbxusernameTextField.text;
    var passsword = frmLanguageChange.passwordTextField.text;
    if (!isEmpty(username))
      username = username.trim();
    //passsword = passsword.trim();
    if (username !== null && username !== "" && passsword !== null && passsword !== "") {
      if (frmLanguageChange.passwordTextField.text !== null && frmLanguageChange.passwordTextField.text !== "") {
        frmLanguageChange.flxbdrpwd.skin = "skntextFieldDividerGreen";
      } else {
        frmLanguageChange.flxbdrpwd.skin = "skntextFieldDivider";
      }
      deviceRegFrom = "login";
      frmSettingsKA.destroy();
      kony.print("username ::" + username + " :: password ::" + passsword);
      kony.sdk.mvvm.LoginAction(username, passsword, "U", "", "", "");
    } else {
      showInvalidCredLabel();
    }
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}



function setRNVersionCall(){

  kony.print("--- Inside setRNVersionCall---");
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options = {
    "access": "online",
    "objectName": "RBObjects"
  };
  var headers = {};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
  var langSelected = kony.store.getItem("langPrefObj");
  var Language = langSelected.toUpperCase();
  var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
  var appVersion1 = appConfig.appVersion.split(".");
  var version = "";
  for(var i in appVersion1){
    if(i == 0)
      version = appVersion1[i]+".";
    else
      version = version+""+appVersion1[i];
  }
  var  currentAppVersion =parseFloat(version);
  dataObject.addField("custId", custid);
  dataObject.addField("Language", Language);
  dataObject.addField("userId", user_id);
  dataObject.addField("RelNoteVersion", currentAppVersion);
  var serviceOptions = {
    "dataObject": dataObject,
    "headers": headers
  };
  objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
  kony.print("Input params1 setRNVersionCall-->" + JSON.stringify(serviceOptions));
  if (kony.sdk.isNetworkAvailable()) {
    objectService.customVerb("setRNVersion", serviceOptions, setRNVersioncallback, setRNVersioncallback);
  } else {
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
  kony.print("--- Exit of setRNVersionCall---");
}

function setRNVersioncallback(){
  kony.print("Send and forget.. !!");
}