function AS_AnimateUsernameDownAndroid(eventobject, changedtext) {
    return AS_TextField_j3d7047f81944175aef75ea8e83400c5(eventobject, changedtext);
}

function AS_TextField_j3d7047f81944175aef75ea8e83400c5(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    frmEnrolluserLandingKA.flxUsernameValidation.isVisible = false;
    animateLabel("DOWN", "lblUsername", frmEnrolluserLandingKA.tbxUsernameKA.text);
}