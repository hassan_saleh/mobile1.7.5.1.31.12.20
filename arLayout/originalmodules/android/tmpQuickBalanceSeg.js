function initializetmpQuickBalanceSeg() {
    flxSegQuickBalance = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxSegQuickBalance",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxSegQuickBalance.setDefaultUnit(kony.flex.DP);
    var lblAccount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccount",
        "isVisible": true,
        "left": "5%",
        "skin": "sknlblTouchIdsmall",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccNumber = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccNumber",
        "isVisible": false,
        "left": "0%",
        "skin": "sknlblTouchIdsmall",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "101dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIncommingTick = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingTick",
        "isVisible": true,
        "right": "3.80%",
        "skin": "sknBOJttfwhiteeSmall",
        "text": "r",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIncommingRing = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingRing",
        "isVisible": true,
        "right": "2%",
        "skin": "sknBOJttfwhitee",
        "text": "s",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountName = new kony.ui.Label({
        "id": "lblAccountName",
        "isVisible": false,
        "left": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSegQuickBalance.add(lblAccount, lblAccNumber, lblIncommingTick, lblIncommingRing, lblAccountName);
}