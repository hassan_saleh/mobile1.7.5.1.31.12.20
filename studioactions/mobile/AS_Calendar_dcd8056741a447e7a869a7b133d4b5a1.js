function AS_Calendar_dcd8056741a447e7a869a7b133d4b5a1(eventobject, isValidDateSelected) {
    var field = frmOpenTermDeposit.calOpenDeposite;
    frmOpenTermDeposit.lblDepositStartDate.text = (parseInt(field.dateComponents[0]) < 10 ? "0" + field.dateComponents[0] : field.dateComponents[0]) + "/" + (parseInt(field.dateComponents[1]) < 10 ? "0" + field.dateComponents[1] : field.dateComponents[1]) + "/" + field.dateComponents[2];
    checkNextOpenDeposite();
    callRateService();
}