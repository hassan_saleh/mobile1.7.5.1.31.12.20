function onClickEditProfile(eventobject) {
    return AS_FlexContainer_i56b4c48809f49498b4f87ca977eed74(eventobject);
}

function AS_FlexContainer_i56b4c48809f49498b4f87ca977eed74(eventobject) {
    if (frmUserSettingsMyProfileKA.lblEditProfile.text == "Save") {
        frmUserSettingsMyProfileKA.flxProfileEdit.isVisible = false;
        frmUserSettingsMyProfileKA.mainContent.isVisible = true;
        frmUserSettingsMyProfileKA.lblEditProfile.text = "Edit";
    } else {
        frmUserSettingsMyProfileKA.mainContent.isVisible = false;
        frmUserSettingsMyProfileKA.flxProfileEdit.isVisible = true;
        frmUserSettingsMyProfileKA.lblEditProfile.text = "Save";
    }
}