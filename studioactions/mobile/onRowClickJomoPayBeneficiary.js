function onRowClickJomoPayBeneficiary(eventobject) {
    return AS_Button_b49a60fcf6df4c69a3ec1c476ab73e33(eventobject);
}

function AS_Button_b49a60fcf6df4c69a3ec1c476ab73e33(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        gblTModule = "jomoBiller";
        getAllAccounts();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}