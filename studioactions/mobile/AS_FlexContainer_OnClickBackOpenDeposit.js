function AS_FlexContainer_OnClickBackOpenDeposit(eventobject) {
    return AS_FlexContainer_a3231f1b6ea942e1a635a05ee9b5ee99(eventobject);
}

function AS_FlexContainer_a3231f1b6ea942e1a635a05ee9b5ee99(eventobject) {
    if (frmOpenTermDeposit.flxConfirmDeposite.isVisible === true) {
        frmOpenTermDeposit.flxDepositSelection.setVisibility(true);
        frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);
        frmOpenTermDeposit.btnNextDeposite.setVisibility(true);
    } else {
        resetForm();
        frmAccountsLandingKA.show();
        frmOpenTermDeposit.destroy();
    }
}