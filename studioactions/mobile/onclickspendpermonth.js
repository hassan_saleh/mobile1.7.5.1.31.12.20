function onclickspendpermonth(eventobject) {
    return AS_Button_acec4d7f9c894f25929f584d5d373058(eventobject);
}

function AS_Button_acec4d7f9c894f25929f584d5d373058(eventobject) {
    frmMyMoneyListKA.btnSpendPerMonth.skin = "slButtonWhiteTab";
    frmMyMoneyListKA.btnSpendPerMonth.focusSkin = "slButtonWhiteTabFocus";
    frmMyMoneyListKA.btnAllAccount.skin = "slButtonWhiteTabDisabled";
    var pie = "";
    frmMyMoneyListKA.lblNoChart.setVisibility(false);
    frmMyMoneyListKA.flxLineChart.removeAll();
    frmMyMoneyListKA.flxPieChart.remove(pie);
    frmMyMoneyListKA.flxPieChart.setVisibility(true);
    frmMyMoneyListKA.flxLineChart.setVisibility(false);
    serv_getStatistics();
}