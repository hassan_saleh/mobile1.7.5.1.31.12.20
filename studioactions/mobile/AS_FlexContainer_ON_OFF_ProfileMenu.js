function AS_FlexContainer_ON_OFF_ProfileMenu(eventobject) {
    return AS_FlexContainer_bc936614b03b4938bebc5d52f232e8fe(eventobject);
}

function AS_FlexContainer_bc936614b03b4938bebc5d52f232e8fe(eventobject) {
    SETTINGS_GLOBALS.PROFILE = !SETTINGS_GLOBALS.PROFILE;
    toggleMenuOptions(PROFILE_CONSTANTS.PROFILE, SETTINGS_GLOBALS.PROFILE, PROFILE_CONSTANTS.MENU[0]);
    toggleMenuOptions(PROFILE_CONSTANTS.ACCOUNTPREFERENCES, (SETTINGS_GLOBALS.ACCOUNT = GENERIC_COSNTATNTS.FALSE), PROFILE_CONSTANTS.MENU[1]);
    toggleMenuOptions(PROFILE_CONSTANTS.CHANGELANGUAGE, (SETTINGS_GLOBALS.LANGUAGE = GENERIC_COSNTATNTS.FALSE), PROFILE_CONSTANTS.MENU[2]);
    toggleMenuOptions(PROFILE_CONSTANTS.ALERTANDNOTIFICATIONS, (SETTINGS_GLOBALS.ALERT = GENERIC_COSNTATNTS.FALSE), PROFILE_CONSTANTS.MENU[4]);
    toggleMenuOptions(PROFILE_CONSTANTS.ALTERNATELOGINS, (SETTINGS_GLOBALS.ALTERNATELOGINS = GENERIC_COSNTATNTS.FALSE), PROFILE_CONSTANTS.MENU[3]);
    toggleMenuOptions(PROFILE_CONSTANTS.CARDPREFERENCES, (SETTINGS_GLOBALS.CARD = GENERIC_COSNTATNTS.FALSE), PROFILE_CONSTANTS.MENU[5]);
}