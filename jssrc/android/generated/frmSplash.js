function addWidgetsfrmSplash() {
    frmSplash.setDefaultUnit(kony.flex.DP);
    var vdoSplash = new kony.ui.Video({
        "height": "100%",
        "id": "vdoSplash",
        "isVisible": true,
        "left": "0dp",
        "onPrepared": AS_Video_h296d8234712437291c488cb7183c55e,
        "onCompletion": AS_Video_f9d8e3f5a48644139d95d9e420528693,
        "skin": "slVideo",
        "source": {
            "mp4": "boj_splash.mp4"
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "controls": false,
        "poster": "tran.png",
        "volume": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    frmSplash.add(vdoSplash);
};

function frmSplashGlobals() {
    frmSplash = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSplash,
        "enabledForIdleTimeout": false,
        "id": "frmSplash",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_i1337e43920841539f5574488c8b73cc,
        "skin": "sknBackground"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_e546c6755b954d789c67f25fdc2ba7e9,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};