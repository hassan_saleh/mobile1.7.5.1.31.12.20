function AS_TextField_NationalIDRegistration_DateOfBirth_onTouchStart(eventobject, x, y) {
    return AS_Calendar_c833479dca1d4fc291188cc271db93f9(eventobject, x, y);
}

function AS_Calendar_c833479dca1d4fc291188cc271db93f9(eventobject, x, y) {
    var caldate = new Date();
    frmRegisterUser.calDateOfBirth.validStartDate = [caldate.getDate(), caldate.getMonth() + 1, caldate.getFullYear() - 118];
    frmRegisterUser.calDateOfBirth.validEndDate = [caldate.getDate(), caldate.getMonth() + 1, caldate.getFullYear() - 18];
}