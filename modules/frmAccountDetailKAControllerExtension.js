/*
 * Controller Extension class for frmAccountDetailKA
 * Developer can edit the existing methods or can add new methods if required
 *
 */

kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
kony.sdk.mvvm.frmAccountDetailKAControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    fetchData: function() {
        try {
			
            var scopeObj = this;
			kony.print("Perf Log: Details page loading service call- start");
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
			exceptionLogCall("frmAccountDetailKAControllerExtension:Fetch","UI ERROR","UI",err);
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
			 kony.print("Perf Log: Details page loading service call- end");
            kony.sdk.mvvm.log.info("success fetching data ", response);
          kony.print("Transction data::::"+JSON.stringify(response));
            //var navigationObject = this.getController() && this.getController().getContextData();
            //navigationObject.setCustomInfo("transactionSegment",response.transactionSegment);
//             populateAccountDetailsinAccountDetails("frmAccountDetailKA");
            var resData = JSON.stringify(response._raw_response_.transactionSegment.records);
            response = {};
            response = {transactionSegment :{}};
            response.transactionSegment = JSON.parse(resData);
            kony.print("transactionSegment response:::::::"+JSON.stringify(response));
            var formattedResponse = scopeObj.getController().processData(response);
            scopeObj.bindData(formattedResponse);
        }

        function error(err) {
            //Error fetching data
        	kony.print("accountTransaction Failed"+JSON.stringify(err));
        	var errorCode = "";
        	if(err.code !== null){
              errorCode = err.code;
            }else if (err.opstatus !== null){
               errorCode = err.opstatus;
            }else if (err.errorCode !== null){
               errorCode = err.errorCode;
            }
            var errorMessage = getServiceErrorMessage(errorCode,"frmAccDetConExtErrFet");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage,popupCommonAlertDimiss,"");
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    processData: function(data) {
        try {
            var scopeObj = this;
            var tempdata=setAccountDetailData(data,"transactionSegment");
        	kony.print("Process Data tempData ::"+JSON.stringify(tempdata));
          	data.transactionSegment=tempdata[0];
          	data.scheduledTransactionSegment=tempdata[1];
          var navigationObject = this.getController() && this.getController().getContextData();
          kony.print("filter options Validation ::"+!validate_FILTEROPTIONS(filter_OPTIONS));
          if((kony.application.getCurrentForm().id !== "frmFilterTransaction" || !validate_FILTEROPTIONS(filter_OPTIONS))){
            navigationObject.setCustomInfo("transactionSegment",tempdata[0]);
          	
          }else if(tempdata[0] !== undefined && tempdata[0] !== null && tempdata[0].length > 0){
            navigationObject.setCustomInfo("transactionSegment",
                         filterAccountTransactions(filter_OPTIONS, kony.application.getPreviousForm().transactionSegment, tempdata[0], kony.application.getPreviousForm()));
//           	navigationObject.setCustomInfo("scheduledTransactionSegment",tempdata[1]);
          }else{
          	navigationObject.setCustomInfo("transactionSegment",tempdata[0]);
          }
            navigationObject.setCustomInfo("scheduledTransactionSegment",tempdata[1]);
            var processedData = this.$class.$superp.processData.call(this, data);
            return processedData;
          //  this.getController().bindData(processedData);
            //return transactionListformatting(processedData,"transactionSegment");
            
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
			exceptionLogCall("frmAccountDetailKAControllerExtension:ProcessData","UI ERROR","UI",err);
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    bindData: function(data) {
        try {
           kony.print("bindData:::"+JSON.stringify(data));
            var formmodel = this.getController().getFormModel();
			var navigationObject = this.getController() && this.getController().getContextData();
			var peekandpop = navigationObject.getCustomInfo("Peekandpop");
			kony.retailBanking.globalData.globals.accountDashboardFlowFlag = false;
            formmodel.clear();
        	var cardData = navigationObject.getCustomInfo("transactionSegment");
        	data["transactionSegment"]["transactionSegment"].setData(cardData);
          /*if(data["transactionSegment"]["transactionSegment"].getData() && data["transactionSegment"]["transactionSegment"].getData().length > 0){		 
              formmodel.setViewAttributeByProperty("transactionSegment","isVisible",true);
    		  formmodel.setViewAttributeByProperty("lblNoResult","isVisible",false);
              formmodel.setViewAttributeByProperty("CopylblFilter0f7770a92cff247","setEnabled",true);
              formmodel.setViewAttributeByProperty("lblExport","setEnabled",true);
         	}
           else{
              formmodel.setViewAttributeByProperty("transactionSegment","isVisible",false);
              formmodel.setViewAttributeByProperty("lblNoResult", "text", "No Results Found"); 
    	      formmodel.setViewAttributeByProperty("lblNoResult","isVisible",true);
              formmodel.setViewAttributeByProperty("CopylblFilter0f7770a92cff247","setEnabled",false);
              formmodel.setViewAttributeByProperty("lblExport","setEnabled",false);
		    } */
          if(data["transactionSegment"]["transactionSegment"].getData() && data["transactionSegment"]["transactionSegment"].getData().length > 0){
            kony.print("leeeee"+JSON.stringify(data["transactionSegment"]["transactionSegment"].getData().length));
            kony.print("data trans:"+JSON.stringify(data["transactionSegment"]["transactionSegment"].getData()));
             var widgetSetData = formmodel.getViewAttributeByProperty("transactionSegment", "setData");
              widgetSetData.setData = cardData;
              formmodel.setViewAttributeByProperty('transactionSegment', "setData", widgetSetData);
             formmodel.setViewAttributeByProperty("transactionSegment","isVisible",true);
    		  formmodel.setViewAttributeByProperty("LabelNoRecordsKA","isVisible",false);
              formmodel.performActionOnView("CopylblFilter0f7770a92cff247","setEnabled",[true]);
              formmodel.performActionOnView("lblExport","setEnabled",[true]);

            if(data["transactionSegment"]["transactionSegment"].getData().length === 1){
               kony.print("LabelNoRecordsKA:n");
              formmodel.performActionOnView("lblExport","setEnabled",[false]);
              	formmodel.performActionOnView("CopylblFilter0f7770a92cff247","setEnabled",[true]);
            }
          	customerAccountDetails.isTransactionEmpty = false;
          }
           else{
             kony.print("LabelNoRecordsKA:");
              formmodel.setViewAttributeByProperty("transactionSegment","isVisible",false);
    		  formmodel.setViewAttributeByProperty("LabelNoRecordsKA","isVisible",true);
              formmodel.setViewAttributeByProperty("LabelNoRecordsKA","text",geti18Value("i18n.accounts.noTransaction"));
              formmodel.performActionOnView("CopylblFilter0f7770a92cff247","setEnabled",[true]);
              formmodel.performActionOnView("lblExport","setEnabled",[false]);
             frmAccountDetailKA.transactionSegment.removeAll();
          	  customerAccountDetails.isTransactionEmpty = true;
           
          }
          kony.print("unClearedTransactionTotal"+kony.retailBanking.globalData.accountsTransactionList.unClearedTransactionTotal);
            if(kony.retailBanking.globalData.accountsTransactionList.unClearedTransactionTotal === 0 || kony.retailBanking.globalData.accountsTransactionList.unClearedTransactionTotal === undefined){
                formmodel.setViewAttributeByProperty("flxUnClearedTransactions","isVisible",false);
            	formmodel.setViewAttributeByProperty("CopyflxLine0bf7c1444b0a84f","isVisible",false);
                formmodel.setViewAttributeByProperty("accountDetailsTransactions","height","82%");
            }else{
                formmodel.setViewAttributeByProperty("flxUnClearedTransactions","isVisible",true);
            	formmodel.setViewAttributeByProperty("CopyflxLine0bf7c1444b0a84f","isVisible",true);
                formmodel.setViewAttributeByProperty("lblUnClearedTransactionTotal","text",kony.retailBanking.globalData.accountsTransactionList.unClearedTransactionTotal+" "+customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].currencyCode);
            	formmodel.setViewAttributeByProperty("accountDetailsTransactions","height","71%");
            }
          
             	formmodel.setViewAttributeByProperty("txtSearch", "text", ""); 
            this.$class.$superp.bindData.call(this, data);
            setDefaultAccountsSetTabView();
			frmAccountDetailKA.accountDetailsTransactions.forceLayout();
            this.getController().getFormModel().formatUI();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
			 if(peekandpop){
               kony.print("peekandpop");
             }
             else{
               formmodel.showView();
               recentTabSelectedAccounts();
			   kony.print("Perf Log: Details page form - end");
             }
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
			exceptionLogCall("frmAccountDetailKAControllerExtension:BindData","UI ERROR","UI",err);
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

    },
    saveData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
			exceptionLogCall("frmAccountDetailKAControllerExtension:SaveData","UI ERROR","UI",err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record
            kony.sdk.mvvm.log.info("success saving record ", res);
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

    },
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
      navigateTo: function(formId, navObject){
    	this.$class.$superp.navigateTo.call(this, formId, navObject);
    },
    navigateToAccountsInfo: function(){
    navigateToAccInfo("frmAccountDetailKA","frmAccountInfoKA");
  }
});