function initializesegGetAllIPSAlias() {
    flxMainSeg = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxMainSeg",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxMainSeg.setDefaultUnit(kony.flex.DP);
    var flxIPSAlias = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxIPSAlias",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIPSAlias.setDefaultUnit(kony.flex.DP);
    var lblAliasTypeTitleSeg = new kony.ui.Label({
        "id": "lblAliasTypeTitleSeg",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.AliasTypeIPS"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAliasTitleSeg = new kony.ui.Label({
        "id": "lblAliasTitleSeg",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAliasTypeSeg = new kony.ui.Label({
        "id": "lblAliasTypeSeg",
        "isVisible": true,
        "left": "30%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAliasSeg = new kony.ui.Label({
        "id": "lblAliasSeg",
        "isVisible": true,
        "left": "30%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIPSAlias.add(lblAliasTypeTitleSeg, lblAliasTitleSeg, lblAliasTypeSeg, lblAliasSeg);
    flxMainSeg.add(flxIPSAlias);
}