var result ={
  "AccountListTransaction": [
      {
       "transactionList": [
        {
          "transamountCurr": "USD",
          "description": "544536770-Direct Energy Power Bill",
          "transamount": "-10.00",
          "transDateEffective": "03/09/2016",
          "type":"cheque"
        },
        {
          "transamountCurr": "USD",
          "description": "Mobile/Email Transfer to John Wu",
          "transamount": "-15.00",
          "transDateEffective": "03/07/2016"
        },
        {
          "transamountCurr": "USD",
          "description": "Acme Bank 4876 Mobile Deposit",
          "transamount": "75.00",
          "transDateEffective": "03/02/2016"
        },
        {
          "transamountCurr": "USD",
          "description": "Bill Pay to Credit Card 4345",
         "transamount": "-39.99",
          "transDateEffective": "03/12/2016"
        },
        {
          "transamountCurr": "USD",
          "description": "Transferred to SAV 2098",
          "transamount": "-60.00",
          "transDateEffective": "02/25/2016"
        }
      ]
    }
  ],        
  "Accounts": [
    {
      "availableBalanceValue": "3,968.24",
      "accountID": "039037292927",
      "availableBalanceCurr": "USD",
      "description": "John's Primary Checking Acct",
      "type": "Checking",
      "acctBalance":"4,153.84",
      "acctBalanceCurr": "USD"
    },
    {
      "availableBalanceValue": "4,152.80",
      "accountID": "039037292927",
      "availableBalanceCurr": "USD",
      "description": "Savings 2453",
      "type": "Savings",
      "acctBalance":"4,287.34",
      "acctBalanceCurr": "USD"
    },
    {
      "availableBalanceValue": "-1,152.80",
      "accountID": "039037292927",
      "availableBalanceCurr": "USD",
      "description": "Travel Rewards Credit Card",
      "type": "CreditCard",
      "acctBalance":"5,000.00",
      "acctBalanceCurr": "USD"
    },
    
    {
      "availableBalanceValue": "22,990",
      "accountID": "039037292927",
      "availableBalanceCurr": "USD",
      "description": "2 Year Deposit 4454",
      "type": "Deposit",
      "maturityDate":"11/20/2017"
    },
    {
      "availableBalanceValue": "50,000",
      "accountID": "039037292927",
      "availableBalanceCurr": "USD",
      "description": "15 Year Mortgage 2388",
      "type": "Mortgage",
      "interestRate": "3.975%"
    }
    ],
    "Locations": [
      {
      "addressLine1": "Kondapur",
      "addressLine2": "Hyderabad",
      "distanceLabel": "8.2 miles",
      "type": "ATM",
      "informationTitle": "HDFC Marketing Bank",
      "latitude": "17.412",
      "longitude": "78.3532",
      "status":"Closed"
    },{
      "addressLine1": "Sector 18",
      "addressLine2": "Noida",
      "distanceLabel": "7.2 miles",
      "type": "BRANCH",
      "informationTitle": "HDFC Personal Banking",
      "latitude": "28.570317",
      "longitude": "77.32181960000003",
      "status":"Open"
    },{
      "addressLine1": "2700 W William Cannon",
      "addressLine2": "Austin, TX 78745",
      "distanceLabel": "1.0 miles",
      "type": "ATM",
      "informationTitle": "Cannon West Ctr ",
      "latitude": "30.215387",
      "longitude": "-97.796012"
    },
  {
      "addressLine1": "2101 W Ben White Blvd",
      "addressLine2": "Austin, TX 78704",
      "distanceLabel": "1.2 miles",
      "type": "ATM",
      "informationTitle": "Ben White Remote",
      "latitude": "30.229812",
      "longitude": "-97.791201"
    },
  {
      "addressLine1": "3601 W William Cannon Dr bldg F",
      "addressLine2": "Austin, TX 78749",
      "distanceLabel": "2.0 miles",
      "type": "BRANCH",
      "informationTitle": "Finacial Center SouthCross Plaza",
      "latitude": "30.214461",
      "longitude": "-97.832546"
    },
  {
      "addressLine1": "3201 Be Caves Rd",
      "addressLine2": "Austin, TX 78701",
      "distanceLabel": "4.7 miles",
      "type": "ATM",
      "informationTitle": "West Woods Shopping Center",
      "latitude": "30.457669",
      "longitude": "-97.789942"
    },
  {
      "addressLine1": "3300 Be Caves Rd STE 795",
      "addressLine2": "Austin, TX 78746",
      "distanceLabel": "4.8 miles",
      "type": "BRANCH",
      "informationTitle": "Finacial Center SouthCross Plaza",
      "latitude": "30.296221",
      "longitude": "-97.829938"
    },
  {
      "addressLine1": "3900 N Interregional Hwy",
      "addressLine2": "Austin, TX 78751",
      "distanceLabel": "7.9 miles",
      "type": "BRANCH",
      "informationTitle": "Finacial Center Hancock",
      "latitude": "30.307333",
      "longitude": "-97.711842"
    },
    {
      "addressLine1": "3600 N Presidental Blvd",
      "addressLine2": "Austin, TX 78719",
      "distanceLabel": "8.2 miles",
      "type": "ATM",
      "informationTitle": "Austin Bergstorm Airport -Office",
      "latitude": "30.1974292",
      "longitude": "-97.6663058"
    }
    ],
   "contact":[
     {
       "typeOfContact" : "Customer Care",
       "phoneNumber" : "284-123-1234" 
     },
     {
       "typeOfContact" : "Stolen Card",
       "phoneNumber" : "284-123-1235" 
     },
     {
       "typeOfContact" : "Technical Support",
       "phoneNumber" : "284-123-1236" 
     },
     {
       "typeOfContact" : "Bill Payment",
       "phoneNumber" : "284-123-1237" 
     }
   ],
  "TransferAndPayments": [
      
        {
          "transamountCurr": "USD",
          "description": "544536770-Direct Energy Power Bill",
          "transamount": "-250.00",
          "transDate": "Mar 13,2016",
          "type": "Recent",
          "acntType":"Checking"
        },
  {
          "transamountCurr": "USD",
          "description": "532676XXXXXX4381 T Mobile POS DEBIT",
          "transamount": "-45.00",
          "transDate": "Mar 10,2016",
          "type": "Recent",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "ATW 532676XXXXXX4381-46-HOUSTON",
          "transamount": "-15.00",
          "transDate": "Mar 08,2016",
          "type": "Recent",
          "acntType":"Checking"
        },
  {
          "transamountCurr": "USD",
          "description": "Bill Pay to Credit Card 4345",
          "transamount": "-75.00",
          "transDate": "Mar 03,2016",
          "type": "Recent",
          "acntType":"CreditCard"
        },{
          "transamountCurr": "USD",
          "description": "Bill Pay to AmericanExpress 6868",
          "transamount": "-50.00",
          "transDate": "Feb 27,2016",
          "type": "Recent",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "Mobile/Email Transfer to John Wu",
          "transamount": "-36.00",
          "transDate": "Feb 13,2016",
          "type": "Recent",
          "acntType":"Checking"
        },
     {
          "transamountCurr": "USD",
          "description": "Recurring Monthly Payment to AT&T",
          "transamount": "-88.27",
          "transDate": "Mar 19,2016",
          "type": "Scheduled",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "Transfer to Saving 2453",
          "transamount": "-250.00",
          "transDate": "Mar 17,2016",
          "type": "Scheduled",
          "acntType":"Checking"
        },
  {
          "transamountCurr": "USD",
          "description": "Recurring Monthly Transfer to saving 2453",
          "transamount": "-72.86",
          "transDate": "Mar 16,2016",
          "type": "Scheduled",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "Recurring Monthly Mobile/Email Transfer to Hasan",
          "transamount": "-24.00",
          "transDate": "Mar 15,2016",
          "type": "Scheduled",
          "acntType":"Checking"
        }
   
],
  "Deposits": [
      
        {
          "transamountCurr": "USD",
          "description": "AmEx 6779 Mobile Deposit",
          "transamount": "49.99",
          "transDate": "Mar 14,2016",
          "type": "Recent",
          "acntType":"Checking"
        },
  {
          "transamountCurr": "USD",
          "description": "Acme Bank 4876 Mobile Deposit",
          "transamount": "15.90",
          "transDate": "Mar 12,2016",
          "type": "Recent",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "BofA 3487 Mobile Deposit",
          "transamount": "250.00",
          "transDate": "Mar 10,2016",
          "type": "Recent",
          "acntType":"Checking"
        },
  {
          "transamountCurr": "USD",
          "description": "Acme Bank 4872 Mobile Deposit",
          "transamount": "10.00",
          "transDate": "Feb 26,2016",
          "type": "Recent",
          "acntType":"CreditCard"
        },{
          "transamountCurr": "USD",
          "description": "AmEx 6772 Mobile Deposit",
          "transamount": "50.00",
          "transDate": "Feb 10,2016",
          "type": "Recent",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "BofA 3481 Mobile Deposit",
          "transamount": "29.99",
          "transDate": "Feb 10,2016",
          "type": "Recent",
          "acntType":"Checking"
        },
     {
          "transamountCurr": "USD",
          "description": "Citibank 1209 Mobile Deposit",
          "transamount": "100.50",
          "transDate": "3/16/2016",
          "type": "Scheduled",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "AmEx 6771 Mobile Deposit",
          "transamount": "15.90",
          "transDate": "3/15/2016",
          "type": "Scheduled",
          "acntType":"Checking"
        },
  {
          "transamountCurr": "USD",
          "description": "Acme Bank 4872 Mobile Deposit",
          "transamount": "35.00",
          "transDate": "3/15/2016",
          "type": "Scheduled",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "BofA 3487 Mobile Deposit",
          "transamount": "10.00",
          "transDate": "3/12/2016",
          "type": "Scheduled",
          "acntType":"Checking"
        },
  {
          "transamountCurr": "USD",
          "description": "Citibank 1201 Mobile Deposit",
          "transamount": "45.34",
          "transDate": "3/11/2016",
          "type": "Scheduled",
          "acntType":"Savings"
        },
  {
          "transamountCurr": "USD",
          "description": "BofA 3481 Mobile Deposit",
          "transamount": "29.99",
          "transDate": "3/10/2016",
          "type": "Scheduled",
          "acntType":"Checking"
        }
   
],
   "registeredPayees" :[
  {
     "transactionName": "John Wu",
     "transactionDate": "E-mail: john.wu@gmail.com"
  },
  {
     "transactionName": "Hasan Shah",
     "transactionDate": "Ph:(407)341-9346"
  },
  {
     "transactionName": "Emily Monroe",
     "transactionDate": "E-mail: emilymon@yahoo.com"
  },
  {
     "transactionName": "Satya Vemuri",
     "transactionDate": "Ph: (207)348-8700"
  }
],
  "myContactList" :[
  {
     "transactionName": "Mrigank Sharma",
     "transactionDate": "Ph:512-792-2900"
  },
  {
     "transactionName": "Andy Barns",
     "transactionDate": "Ph:650-645-5695"
  },
  {
     "transactionName": "Sarah Collins",
     "transactionDate": "Ph:650-645-5696"
  },
  {
     "transactionName": "Kurt Steinle",
     "transactionDate": "Ph:650-645-5697"
  },
  {
     "transactionName": "Bill Reagan",
     "transactionDate": "Ph:650-645-3487"
  },
  {
     "transactionName": "Satish Kamat",
     "transactionDate": "Ph:650-645-0173"
  },
  {
     "transactionName": "William Yee",
     "transactionDate": "Ph:650-645-1840"
  },
  {
     "transactionName": "Melinda Farrell",
     "transactionDate": "Ph:650-621-9289"
  }
],
  "unCategorised" :[
    {
     "transactionName": "Bill Pay to AmericanExpress 6868",
     "transactionDate": "Jan 29, 2016",
     "transactionAmount":"240.00",
      "transamountCurr": "USD"
  },
  {
     "transactionName": "Mobile/Email Transfer to John Wu",
     "transactionDate": "Jan 10, 2016",
     "transactionAmount":"30.00",
      "transamountCurr": "USD"
  },
  {
    "transactionName": "Payment to City Of Austin",
     "transactionDate": "Jan 8, 2016",
     "transactionAmount":"10.00",
      "transamountCurr": "USD"
  },
  {
     "transactionName": "Payment to City Of Austin",
     "transactionDate": "Jan 7, 2016",
     "transactionAmount":"10.00",
      "transamountCurr": "USD"
  },
    {
     "transactionName": "Bill Pay to AmericanExpress 6868",
     "transactionDate": "Jan 7, 2016",
     "transactionAmount":"12.00",
      "transamountCurr": "USD"
  },
  {
     "transactionName": "Mobile/Email Transfer to John ",
     "transactionDate": "Jan 6, 2016",
     "transactionAmount":"22.00",
      "transamountCurr": "USD"
  },
  {
     "transactionName": "Payment to City Of Austin",
     "transactionDate": "Jan 4, 2016",
     "transactionAmount":"15.00",
      "transamountCurr": "USD"
  },
    {
     "transactionName": "Bill Pay to AmericanExpress 6868",
     "transactionDate": "Jan 2, 2016",
     "transactionAmount":"12.00",
      "transamountCurr": "USD"
  } 
  ]
}

