function initializesegmanagecardstmplt() {
    flxCreditCardTemplateKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "flxsegSknblue",
        "height": "90%",
        "id": "flxCreditCardTemplateKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "flexTransparent"
    }, {}, {});
    flxCreditCardTemplateKA.setDefaultUnit(kony.flex.DP);
    var cardType = new kony.ui.Label({
        "centerX": "50%",
        "height": "12%",
        "id": "cardType",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCardDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "height": "81%",
        "id": "flxCardDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "72%",
        "zIndex": 1
    }, {}, {});
    flxCardDetails.setDefaultUnit(kony.flex.DP);
    var cardImage = new kony.ui.Image2({
        "centerX": "50%",
        "height": "100%",
        "id": "cardImage",
        "isVisible": true,
        "skin": "slImage",
        "src": "card_affluent_debit.png",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var cardNumber = new kony.ui.Label({
        "height": "30dp",
        "id": "cardNumber",
        "isVisible": true,
        "left": "26%",
        "skin": "sknLblWhiteTmp",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "24%",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CardHolder = new kony.ui.Label({
        "bottom": "1%",
        "id": "CardHolder",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblWhiteTmp",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "84%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var ValidThru = new kony.ui.Label({
        "id": "ValidThru",
        "isVisible": true,
        "left": "42%",
        "skin": "sknLblWhiteTmp",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "62%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardDetails.add(cardImage, cardNumber, CardHolder, ValidThru);
    flxCreditCardTemplateKA.add(cardType, flxCardDetails);
}