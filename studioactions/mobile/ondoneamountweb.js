function ondoneamountweb(eventobject, changedtext) {
    return AS_TextField_e59de8c02998442899f89a1dc1de84aa(eventobject, changedtext);
}

function AS_TextField_e59de8c02998442899f89a1dc1de84aa(eventobject, changedtext) {
    frmWebCharge.flxBorderAmount.skin = "skntextFieldDividerGreen";
    frmWebCharge.lblHiddenAmount.text = frmWebCharge.txtFieldAmount.text;
    frmWebCharge.txtFieldAmount.maxTextLength = 20;
    frmWebCharge.txtFieldAmount.text = formatAmountwithcomma(frmWebCharge.lblHiddenAmount.text, getDecimalNumForCurr(frmWebCharge.lblCurrencyCode.text));
}