function onTextChangeATMPOCLimit(eventobject, changedtext) {
    return AS_TextField_e12d687073e74031a9afc37b7710bba6(eventobject, changedtext);
}

function AS_TextField_e12d687073e74031a9afc37b7710bba6(eventobject, changedtext) {
    if (eventobject.id === "txtWithdrawalLimit") {
        onTextChange_ATMPOSLIMIT(frmATMPOSLimit.txtWithdrawalLimit, frmATMPOSLimit.sliderATMLimit, "onTextChange", "ATM");
    } else if (eventobject.id === "txtPOSLimit") {
        onTextChange_ATMPOSLIMIT(frmATMPOSLimit.txtPOSLimit, frmATMPOSLimit.sliderPOSLimit, "onTextChange", "POS");
    }
}