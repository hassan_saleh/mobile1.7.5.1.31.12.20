function frmNewTransferKAdeviceback(eventobject) {
    return AS_Form_b83b869f88684d768caf00545b527b5a(eventobject);
}

function AS_Form_b83b869f88684d768caf00545b527b5a(eventobject) {
    kony.print("Device Back1");
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseback, onClickNoBackTransfer, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function onClickNoBackTransfer() {
        popupCommonAlertDimiss();
    }

    function userResponseback() {
        gblLaunchModeOBJ.lauchMode = false;
        if (gblTModule === "own") {
            frmPaymentDashboard.show();;
        } else {
            frmShowAllBeneficiary.show();
        }
        removeDatafrmTransfersform();
        makeInlineErrorsTransfersInvisible();
        popupCommonAlertDimiss();
        frmNewTransferKA.destroy();
    }
}