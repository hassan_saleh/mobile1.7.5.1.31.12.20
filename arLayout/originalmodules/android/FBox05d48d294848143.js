function initializeFBox05d48d294848143() {
    FBox05d48d294848143 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "40dp",
        "id": "FBox05d48d294848143",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    FBox05d48d294848143.setDefaultUnit(kony.flex.DP);
    var lblServiceList = new kony.ui.Label({
        "id": "lblServiceList",
        "isVisible": true,
        "left": "0dp",
        "minHeight": "20dp",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.locateus.frmLocatorBranchDetailsKA.CopyatmServices00ff586c7132d45"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    FBox05d48d294848143.add(lblServiceList);
}