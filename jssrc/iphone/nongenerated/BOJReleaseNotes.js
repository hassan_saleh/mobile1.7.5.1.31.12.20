function releaseNotesPreShow() {
    if (!loginFlowRN) {
        frmReleaseNotesKA.lblClose.setVisibility(false);
        frmReleaseNotesKA.flxBack.setVisibility(true);
    } else {
        frmReleaseNotesKA.flxBack.setVisibility(false);
        frmReleaseNotesKA.lblClose.setVisibility(true);
    }
    if (kony.store.getItem("langPrefObj") == "ar") {
        if (RELNOTES_Heading_AR !== undefined && RELNOTES_Heading_AR !== null && RELNOTES_Heading_AR !== "") {
            frmReleaseNotesKA.lblTitleReleaseNotes.text = RELNOTES_Heading_AR;
        } else {
            frmReleaseNotesKA.lblTitleReleaseNotes.text = "";
        }
        assignReleasenotes(ar_ReleaseNotes);
    } else {
        if (RELNOTES_Heading_EN !== undefined && RELNOTES_Heading_EN !== null && RELNOTES_Heading_EN !== "") {
            frmReleaseNotesKA.lblTitleReleaseNotes.text = RELNOTES_Heading_EN;
        } else {
            frmReleaseNotesKA.lblTitleReleaseNotes.text = "";
        }
        assignReleasenotes(en_ReleaseNotes);
    }
}

function assignReleasenotes(releaseNotes) {
    if (releaseNotes !== undefined && releaseNotes !== null && releaseNotes !== "") {
        releaseNotes = releaseNotes.split(";");
        var data = [];
        var len = releaseNotes.length;
        len--;
        for (var i = 0; i < len; i++) {
            data[i] = {
                "note": releaseNotes[i]
            };
        }
        kony.print("here  :data : " + JSON.stringify(data));
        frmReleaseNotesKA.segReleaseNotes.widgetDataMap = {
            lblReleaseNotes: "note"
        };
        frmReleaseNotesKA.segReleaseNotes.removeAll();
        frmReleaseNotesKA.segReleaseNotes.setData(data);
    } else {
        //nothing to show, should never come here
    }
}

function releaseNotesnextBackBtnonClick() {
    if (loginFlowRN) {
        loginFlowRN = false;
        setRNVersionCall();
        devshwnot();
    } else {
        frmInformationKA.show();
    }
}