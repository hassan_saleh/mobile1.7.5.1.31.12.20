function AS_onEndEditingAccountNumberiOS(eventobject, changedtext) {
    return AS_TextField_j03dd5d1dd524b12afcf9e6136a46fd0(eventobject, changedtext);
}

function AS_TextField_j03dd5d1dd524b12afcf9e6136a46fd0(eventobject, changedtext) {
    if (frmAddExternalAccountKA.externalAccountNumberTextField.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAccountNumber", frmAddExternalAccountKA.externalAccountNumberTextField.text);
}