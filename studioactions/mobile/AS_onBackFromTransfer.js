function AS_onBackFromTransfer(eventobject) {
    return AS_FlexContainer_iecb56689d7a494e8507f0358d52da26(eventobject);
}

function AS_FlexContainer_iecb56689d7a494e8507f0358d52da26(eventobject) {
    if (kony.retailBanking.util.validation.isTransferChanged()) {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponse, onClickNoBack, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    } else {
        userResponse();
    }

    function onClickNoBack() {
        popupCommonAlertDimiss();
    }

    function userResponse() {
        gblLaunchModeOBJ.lauchMode = false;
        kony.print("loadBillerDetails " + loadBillerDetails);
        if (loadBillerDetails === true) {
            gblTModule = "send";
            getAllAccounts();
        } else {
            if (gblTModule === "own") {
                frmPaymentDashboard.show();
            } else {
                frmShowAllBeneficiary.show();
            }
            removeDatafrmTransfersform();
            makeInlineErrorsTransfersInvisible();
            popupCommonAlertDimiss();
            //frmNewTransferKA.destroy();
        }
    }
}