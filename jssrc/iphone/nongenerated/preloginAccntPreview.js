function setAccountPreviewAndroid() {
    /*
    var temppdata=kony.store.getItem("settingsflagsObject");
    if(temppdata.rememberMeFlag === true && temppdata.accountPreviewEnabledFlag === true)
      showAccountPreview();
    else
      showAccountAlert();*/
}

function AccountPreviewiphone(myWidget, gestureInfo) {
    /*
      var temppdata=kony.store.getItem("settingsflagsObject");
      kony.print("temppdata:::"+temppdata);
      // if(temppdata.rememberMeFlag === true && temppdata.accountPreviewEnabledFlag === true)
      // {
      var swipedSide = gestureInfo.swipeDirection;
      if(swipedSide === 1)
        callDeviceRegistrationForCustID();
      /*  }
       else
            showAccountAlert(); */
}

function AccountPreviewiphone1(myWidget, gestureInfo) {
    /*
      var temppdata=kony.store.getItem("settingsflagsObject");
      if(temppdata.rememberMeFlag === true && temppdata.accountPreviewEnabledFlag === true)
      {
        var swipedSide = gestureInfo.swipeDirection;
        if(swipedSide === 1) 
          apUnAnimate();
      }
      else
        showAccountAlert(); */
}

function callDeviceRegistrationForCustID() {
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance()) {
            kony.print("callDeviceRegistrationForCustID::");
            ShowLoadingScreen();
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var serviceName = "RBObjects";
            var headers = {};
            var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
            var langSelected = kony.store.getItem("langPrefObj");
            var Language = langSelected.toUpperCase();
            var devID = deviceid;
            if (kony.boj.imeiPermissionFlag) imei = kony.os.deviceInfo().uid;
            else imei = "";
            kony.print(" - > langSelected  " + langSelected + ":Device ID::" + devID);
            var serviceOptions = {
                "dataObject": dataObject,
                "headers": headers,
                "queryParams": {
                    "deviceId": devID,
                    "Language": Language,
                    "Imei": imei
                }
            };
            modelObj.fetch(serviceOptions, showAccountPreview, callDeviceRegistrationForCustIDError);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function callDeviceRegistrationForCustIDError(err) {
    //Error fetching data
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("callDeviceRegistrationForCustIDError ::" + JSON.stringify(err));
    var errorCode = "";
    if (err.code !== null) {
        errorCode = err.statusCode;
    } else if (err.opstatus !== null) {
        errorCode = err.opstatus;
    } else if (err.errorCode !== null) {
        errorCode = err.errorCode;
    }
    var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
    customAlertPopup(errorHeaderText + ": " + errorCode, errorMessage, popupCommonAlertDimiss, "");
}

function showAccountPreview(response) {
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance()) {
            kony.print("showAccountPreview response::" + JSON.stringify(response));
            ShowLoadingScreen();
            //alert("showAccountPreview::"+JSON.stringify(response));
            if (!isEmpty(response[0].regFlag) && response[0].regFlag === "T") {
                if (!isEmpty(response[0].QBalanceFlag) && response[0].QBalanceFlag === "Y") {
                    gblQbalShowFlag = response[0].QBalanceFlag;
                    gblQtransFlagShowFlag = response[0].QTrasactionFlag;
                    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                    var options = {
                        "access": "online",
                        "objectName": "RBObjects"
                    };
                    var serviceName = "RBObjects";
                    var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
                    var dataObject = new kony.sdk.dto.DataObject("Accounts");
                    custid = response[0].custId;
                    var serviceOptions = {
                        "dataObject": dataObject,
                        "queryParams": {
                            "custId": response[0].custId,
                            "lang": (kony.store.getItem("langPrefObj") === "en") ? "eng" : "ara"
                        }
                    };
                    // alert("custid::"+custid);
                    modelObj.fetch(serviceOptions, preloginAccountsTransactionSuccess, preloginAccountError);
                } else {
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.quickBal.quickDescSettings"), popupCommonAlertDimiss, "");
                }
            } else {
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.deviceNotRegistered.QuickBal"), popupCommonAlertDimiss, "");
            }
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function preloginAccountsTransactionSuccess(response) {
    kony.print("preloginAccountsTransactionSuccess:::" + JSON.stringify(response));
    if (isEmpty(response)) {
        //alert("hiii"+JSON.stringify(response));
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.alerts.NoAccountsMsg"), popupCommonAlertDimiss, "");
    } else {
        var accountsRawData = response;
        var accountsFilteredData = [];
        if (accountsRawData !== undefined && accountsRawData !== null && accountsRawData.length > 0) {
            for (var z in accountsRawData) {
                if ((accountsRawData[z]["accountType"] == "S" || accountsRawData[z]["accountType"] == "C" || accountsRawData[z]["accountType"] == "Y" || accountsRawData[z]["accountType"] == "CC" || accountsRawData[z]["accountType"] == "B") && accountsRawData[z]["productCode"] != "320") {
                    accountsRawData[z]["availableBalance"] = formatamountwithCurrency(accountsRawData[z]["availableBalance"], accountsRawData[z]["currencyCode"]);
                    accountsRawData[z]["currentBalance"] = formatamountwithCurrency(accountsRawData[z]["currentBalance"], accountsRawData[z]["currencyCode"]);
                    accountsRawData[z]["outstandingBalance"] = formatamountwithCurrency(accountsRawData[z]["outstandingBalance"], accountsRawData[z]["currencyCode"]);
                    if (accountsRawData[z].QBHideShowFlag === "T" || accountsRawData[z].QBHideShowFlag === "Y") accountsFilteredData.push(accountsRawData[z]);
                }
            }
            kony.retailBanking.globalData.accountsDashboardDataPreview = accountsFilteredData;
            if (gblQtransFlagShowFlag === "Y") {
                if (accountsFilteredData !== undefined && accountsFilteredData !== null && accountsFilteredData.length > 0) {
                    var requestObj = {
                        "custId": "",
                        "loop_count": parseInt(accountsFilteredData.length),
                        "loop_seperator": ":",
                        "accountNumber": "",
                        "p_Branch": "",
                        "p_no_Of_Txn": "",
                        "lang": ""
                    };
                    for (var j in accountsFilteredData) {
                        requestObj.custId = requestObj.custId + (j === accountsFilteredData.length - 1 ? custid : (custid + ":"));
                        requestObj.accountNumber = requestObj.accountNumber + (j === accountsFilteredData.length - 1 ? accountsFilteredData[j].accountID : (accountsFilteredData[j].accountID + ":"));
                        requestObj.p_Branch = requestObj.p_Branch + (j === accountsFilteredData.length - 1 ? accountsFilteredData[j].branchNumber : (accountsFilteredData[j].branchNumber + ":"));
                        requestObj.lang = requestObj.lang + (j === accountsFilteredData.length - 1 ? "eng" : ("eng" + ":"));
                        requestObj.p_no_Of_Txn = requestObj.p_no_Of_Txn + (j === accountsFilteredData.length - 1 ? "2" : ("2" + ":"));
                    }
                    getAccountTransactionsPreview(requestObj, response);
                } else {
                    kony.print("No data11preview::::");
                    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.alerts.NoAccountsMsg"), popupCommonAlertDimiss, "");
                }
            } else {
                kony.print("No Transaction call:");
                kony.retailBanking.globalData.accountsTransactionDataPreview = [{}];
                preloginAccountsSuccess();
            }
        } else {
            kony.print("No data22::::");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.alerts.NoAccountsMsg"), popupCommonAlertDimiss, "");
        }
    }
}

function preloginAccountError(err) {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
    customAlertPopup(geti18Value("i18n.NUO.Error"), errorMessage, popupCommonAlertDimiss, "");
    //showAccountsError(i18n_fetchingError,i18n_tryAgain);
}

function showAccountsError(errMsg, errType) {
    frmLoginKA.flxAccountsSegment.setVisibility(false);
    frmLoginKA.flxNodata.setVisibility(true);
    frmLoginKA.lbltimeStampKA.setVisibility(false);
    frmLoginKA.lblNodatafetched.text = errMsg;
    frmLoginKA.lblErrorData.text = errType;
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    apAnimate();
}

function preloginAccountsSuccess() {
    var accountsData = kony.retailBanking.globalData.accountsDashboardDataPreview;
    kony.print("PrevieData:" + JSON.stringify(accountsData));
    var sectionAccData = [];
    var jomoImg = "";
    var langSelected1 = kony.store.getItem("langPrefObj");
    if (accountsData && accountsData.length > 0) {
        kony.print("Inside accounts Preview:::");
        for (var i in accountsData) {
            var segformat = [];
            var segRowData = [];
            var aHeader = {};
            var availableBal, currBal, outstandingBal, accType;
            var accountNumber = accountsData[i]["accountID"];
            var ttt = "";
            if (isEmpty(accountsData[i]["AccNickName"])) {
                aHeader["nickName"] = accountsData[i]["accountName"];
                if (aHeader["nickName"] !== null && aHeader["nickName"] !== "") {
                    if (aHeader["nickName"].length > 30) aHeader["nickName"] = aHeader["nickName"].substring(0, 22) + "  . . .";
                    ttt = aHeader["nickName"] + " " + accountNumber;
                }
            } else {
                aHeader["nickName"] = accountsData[i]["AccNickName"];
                if (aHeader["nickName"] !== null && aHeader["nickName"] !== "") {
                    if (aHeader["nickName"].length > 30) aHeader["nickName"] = aHeader["nickName"].substring(0, 27) + "  . . .";
                    ttt = aHeader["nickName"];
                }
            }
            if (accountsData[i]["availableBalance"]) {
                availableBal = accountsData[i]["availableBalance"];
            }
            if (accountsData[i]["currentBalance"]) {
                currBal = accountsData[i]["currentBalance"];
            }
            if (accountsData[i]["outstandingBalance"]) {
                outstandingBal = accountsData[i]["outstandingBalance"];
            }
            if (accountsData[i]["accountType"] == "C") {
                currBal = "-" + currBal;
            }
            aHeader["availableBalance"] = availableBal;
            aHeader["currentBalance"] = currBal;
            aHeader["currencyCode"] = accountsData[i]["currencyCode"];
            aHeader["outstandingBalance"] = outstandingBal;
            if (accountsData[i]["accountType"] == "C") {
                accType = "Current account";
            } else if (accountsData[i]["accountType"] == "S") {
                accType = "Saving account";
            } else if (accountsData[i]["accountType"] == "Y") {
                accType = "Corporate account";
            }
            aHeader["accountType"] = accType;
            aHeader["btnNav"] = {
                "text": "",
                "isVisible": false
            };
            aHeader["btnNav1"] = {
                "isVisible": false
            };
            aHeader["lblIncommingRing"] = {
                "text": "",
                "isVisible": false
            };
            aHeader["CopylblIncommingRing0ac2c9e9c52d147"] = {
                "text": "",
                "isVisible": false
            };
            if (accountsData[i]["isjomopayacc"] == "Y") {
                jomoImg = '<img align = "left" src="mobicon.png" />';
            }
            if (langSelected1 === "ar" || langSelected1 === "ara") {
                var taxtData = jomoImg + "  " + '<label>' + ttt + '</label>';
                aHeader["nickName"] = {
                    "text": taxtData.toString()
                }
                aHeader["availableBalance"] = aHeader["currencyCode"] + " " + availableBal;
            } else {
                var taxtData = '<label>' + ttt + '</label>' + "  " + jomoImg
                aHeader["nickName"] = {
                    "text": taxtData.toString()
                }
                aHeader["availableBalance"] = availableBal + " " + aHeader["currencyCode"];
            }
            segformat.push(aHeader);
            kony.print("gblQtransFlagShowFlag:" + gblQtransFlagShowFlag);
            if (gblQtransFlagShowFlag === "Y") {
                if (kony.retailBanking.globalData.accountsTransactionDataPreview !== null && kony.retailBanking.globalData.accountsTransactionDataPreview[i] !== undefined) {
                    if (kony.retailBanking.globalData.accountsTransactionDataPreview[i].Transactions !== null && kony.retailBanking.globalData.accountsTransactionDataPreview[i].Transactions !== undefined) {
                        var acctransaction = kony.retailBanking.globalData.accountsTransactionDataPreview[i].Transactions;
                        // kony.print("acctransaction::::::"+acctransaction);
                        for (var j = 0; j < acctransaction.length; j++) {
                            var tranSym = (acctransaction[j].category === "C") ? "+ " : "- ";
                            var transDesc = "";
                            if (acctransaction[j].description !== null && acctransaction[j].description !== "") {
                                transDesc = acctransaction[j].description;
                                transDesc = transDesc.substring(0, 30) + "  .  .  .";
                            }
                            var dateFormat = isEmpty(acctransaction[j].transactionDate) ? "" : getShortMonthDateFormat(acctransaction[j].transactionDate);
                            segRowData.push({
                                lblTransId: {
                                    "text": acctransaction[j].transactionId,
                                    "isVisible": false
                                },
                                lblTransDate: {
                                    "text": dateFormat,
                                    "isVisible": true
                                },
                                lblTransName: {
                                    "text": transDesc,
                                    "isVisible": true
                                },
                                lblTransAmount: {
                                    "text": tranSym + formatamountwithCurrency(acctransaction[j].amount, accountsData[i]["currencyCode"]),
                                    "isVisible": true
                                },
                                lblNoData: {
                                    "text": geti18Value("i18n.accounts.noTransaction"),
                                    "isVisible": false
                                },
                                flxTrans: {
                                    "isVisible": true,
                                    "height": "65dp"
                                }
                            });
                        }
                    } else {
                        kony.print("No Transaction -----temp123455");
                        segRowData.push({
                            lblTransId: {
                                "isVisible": false
                            },
                            lblTransDate: {
                                "isVisible": false
                            },
                            lblTransName: {
                                "isVisible": false
                            },
                            lblTransAmount: {
                                "isVisible": false
                            },
                            lblNoData: {
                                "text": geti18Value("i18n.accounts.noTransaction"),
                                "left": "15Dp",
                                "isVisible": true
                            },
                            flxTrans: {
                                "isVisible": true,
                                "height": "65dp"
                            }
                        });
                    }
                } else {
                    kony.print("No Transaction -----temp1234");
                    segRowData.push({
                        lblTransId: {
                            "isVisible": false
                        },
                        lblTransDate: {
                            "isVisible": false
                        },
                        lblTransName: {
                            "isVisible": false
                        },
                        lblTransAmount: {
                            "isVisible": false
                        },
                        lblNoData: {
                            "text": geti18Value("i18n.accounts.noTransaction"),
                            "left": "15Dp",
                            "isVisible": true
                        },
                        flxTrans: {
                            "isVisible": true,
                            "height": "65dp"
                        }
                    });
                }
            } else {
                kony.print("No Transaction -----temp123");
                segRowData.push({
                    lblAccPrecheckNoTrans: {
                        "isVisible": false
                    },
                    flxtmpAccPrecheckNoTransactions: {
                        "isVisible": true,
                        "height": "0dp"
                    },
                    template: flxtmpAccPrecheckNoTransactions
                });
            }
            segformat.push(segRowData);
            sectionAccData.push(segformat);
        }
        kony.print("sectionAccData:::" + JSON.stringify(sectionAccData));
    }
    frmLoginKA.segAccountPreviewKA.widgetDataMap = {
        amountAccount1: "availableBalance",
        nameAccount1: "nickName",
        lblTransDate: "lblTransDate",
        lblTransName: "lblTransName",
        lblTransAmount: "lblTransAmount",
        lblTransId: "lblTransId",
        lblNoData: "lblNoData",
        lblCurrency: "currencyCode",
        btnNav: "btnNav",
        btnNav1: "btnNav1",
        lblIncommingRing: "lblIncommingRing",
        lblAccPrecheckNoTrans: "lblAccPrecheckNoTrans",
        flxtmpAccPrecheckNoTransactions: "flxtmpAccPrecheckNoTransactions",
        CopylblIncommingRing0ac2c9e9c52d147: "CopylblIncommingRing0ac2c9e9c52d147"
    };
    frmLoginKA.segAccountPreviewKA.setData(sectionAccData);
    frmLoginKA.accountPreviewCard.setVisibility(true);
    var date = new Date();
    var timeStampNow = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear() + " " + date.timeNow();
    frmLoginKA.lbltimeStampKA.text = i18n_asOf + " " + timeStampNow;
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    //frmLoginKA.lbltimeStampKA.setVisibility(true);//upendra
    if (!isEmpty(accountsData) && accountsData.length > 0) {
        frmLoginKA.flxAccountsSegment.setVisibility(true);
        frmLoginKA.lbltimeStampKA.setVisibility(true);
    } else {
        frmLoginKA.flxAccountsSegment.setVisibility(false);
        frmLoginKA.LabelNoRecordsKA.setVisibility(true);
    }
    frmLoginKA.lbltimeStampKA.setVisibility(false);
    frmLoginKA.flxNodata.setVisibility(false);
    if (kony.application.getCurrentForm().id === "frmLanguageChange") {
        frmLoginKA.show();
    }
    accountPreviewAnimate();
}

function showAccountAlert() {
    myalert = kony.ui.Alert({
        "message": i18n_swipeMsg,
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": i18n_Info,
        "yesLabel": i18n_OK,
        "noLabel": null,
        "alertHandler": changeopacity
    }, {});
}

function changeopacity() {
    frmLoginKA.loginMainScreen.opacity = 1;
    frmLoginKA.skin = "sknLoginMainPage";
}

function getAccountTransactionsPreview(queryParams, scopeObj, response) {
    kony.print("getAccountTransactions queryParams::::" + JSON.stringify(queryParams));

    function loopingSuccessCallback(resoponse) {
        kony.print("response Looping" + JSON.stringify(resoponse));
        if (resoponse.LoopDataset !== null && resoponse.LoopDataset !== undefined) {
            kony.retailBanking.globalData.accountsTransactionDataPreview = resoponse.LoopDataset;
            kony.print("loopingSuccessCallback-----------------data" + JSON.stringify(kony.retailBanking.globalData.accountsTransactionData));
        } else {
            kony.print("loopingSuccessCallback No data:::");
        }
        preloginAccountsSuccess();
    }

    function loopingErrorCallback(error) {
        kony.print("error looping" + JSON.stringify(error));
        var errorCode = "";
        if (error.code !== null) {
            errorCode = error.code;
        } else if (error.opstatus !== null) {
            errorCode = error.opstatus;
        } else if (error.errorCode !== null) {
            errorCode = error.errorCode;
        }
        var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
        //     var title = geti18nkey("i18n.common.Information");
        customAlertPopup(geti18Value("i18n.NUO.Error"), errorMessage, popupCommonAlertDimiss, "");
        kony.retailBanking.globalData.accountsTransactionDataPreview = [{}];
        preloginAccountsSuccess();
    }
    //    appMFConfiguration.integrationObj = appMFConfiguration.konysdkObject.getIntegrationService("LoopExample");
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopExample");
    kony.print("end point URL ::" + appMFConfiguration.getUrl());
    if (kony.sdk.isNetworkAvailable()) {
        appMFConfiguration.invokeOperation("LoopTransactions", {}, queryParams, loopingSuccessCallback, loopingErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function accountPreviewAnimate() {
    frmLoginKA.apScrollDisable.animate(kony.ui.createAnimation({
        "0": {
            "left": "-100%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        },
        "100": {
            "left": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.20
    }, {
        "animationStart": null,
        "animationEnd": function() {}
    });
    frmLoginKA.skin = "sknLoginMainPageQB";
}

function accountPreviewAnimateOff() {
    kony.print("accountPreviewAnimateOff::");
    frmLoginKA.apScrollDisable.animate(kony.ui.createAnimation({
        "0": {
            "left": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        },
        "100": {
            "left": "-100%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.20
    }, {
        "animationStart": null,
        "animationEnd": function() {}
    });
    frmLoginKA.skin = "sknLoginMainPage";
    kony.boj.maintainenceCHECK();
}

function getAccountDataforQuickPreviewAccounts() {
    kony.print("getAccountDataforQuickPreviewAccounts");
    gblDiffAccountsQuickBalOrder = true;
    if (isDevRegDone === "T") getAccountsServiceCallGeneric();
    else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.deviceNotRegistered.QuickBal"), popupCommonAlertDimiss, "");
    }
}

function getDataforAccountsReOrder() {
    kony.print("getDataforAccountsReOrder");
    gblDiffAccountsQuickBalOrder = false;
    getAccountsServiceCallGeneric();
}

function getAccountsServiceCallGeneric() {
    kony.print("getAccountsServiceCallGeneric");
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance()) {
            ShowLoadingScreen();
            kony.print("isDeviceRegistered:::" + isDeviceRegistered);
            //if(!isEmpty(isDeviceRegistered) && (isDeviceRegistered === "F" || isDeviceRegistered === F)){
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("Accounts");
            var serviceOptions = {
                "dataObject": dataObject,
                "queryParams": {
                    "custId": custid,
                    "lang": kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara"
                }
            };
            modelObj.fetch(serviceOptions, getAccountDataforQuickPreviewAccountsSuccess, getAccountDataforQuickPreviewAccountsError);
            /*}else{
              accountDashboardDataCall();
            }*/
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function getAccountDataforQuickPreviewAccountsSuccess(response) {
    kony.print("preloginAccountsTransactionSuccess:::" + JSON.stringify(response));
    var accountsFilteredData = [];
    var accountsMasterdata = [];
    if (!isEmpty(response)) {
        var accountsRawData = response;
        kony.print("accountsRawData length:::" + JSON.stringify(accountsRawData.length));
        if (accountsRawData !== undefined && accountsRawData !== null && accountsRawData.length > 0) {
            if (gblDiffAccountsQuickBalOrder) frmAuthorizationAlternatives.segAccounts.widgetDataMap = {
                lblAccount: "lblAccount",
                lblAccNumber: "lblAccNumber",
                lblIncommingTick: "lblIncommingTick",
                lblIncommingRing: "lblIncommingRing",
                lblAccountName: "lblAccountName"
            };
            else frmAccountsReorderKA.segReOrderAccounts.widgetDataMap = {
                lblAccount: "lblAccount",
                lblAccNumber: "lblAccNumber",
                lblIncommingTick: "lblIncommingTick",
                lblIncommingRing: "lblIncommingRing",
                lblAccountName: "lblAccountName"
            };
            //kony.print(quick);
            for (var z in accountsRawData) {
                if ((accountsRawData[z]["accountType"] == "S" || accountsRawData[z]["accountType"] == "C" || accountsRawData[z]["accountType"] == "Y" || accountsRawData[z]["accountType"] == "CC" || accountsRawData[z]["accountType"] == "B") && accountsRawData[z]["productCode"] != "320") {
                    accountsFilteredData.push(accountsRawData[z]);
                }
            }
            kony.print("accountsFilteredData filterdd:::" + JSON.stringify(accountsFilteredData));
            kony.print("accountsFilteredData lengthsafsadf:::" + accountsFilteredData.length);
            kony.retailBanking.globalData.accountsDashboardDataPreview = accountsFilteredData;
            if (accountsFilteredData !== undefined && accountsFilteredData !== null && accountsFilteredData.length > 0) {
                kony.print("accountsFilteredData lengthsafsadf:::");
                for (var k in accountsFilteredData) {
                    kony.print("k :::" + k);
                    var segRowData1 = {};
                    var accountNumber = accountsFilteredData[k]["accountID"];
                    // var accountName = (accountsFilteredData[k]["AccNickName"] !== undefined && accountsFilteredData[k]["AccNickName"] !== "")?accountsFilteredData[k]["AccNickName"]:accountsFilteredData[k]["accountName"];
                    var accountName = "";
                    if (isEmpty(accountsFilteredData[k]["AccNickName"])) {
                        accountName = accountsFilteredData[k]["accountName"];
                        if (accountName !== null && accountName !== "") {
                            if (accountName.length > 30) accountName = accountName.substring(0, 22) + "  ...";
                            accountName = accountName + " " + accountNumber;
                        }
                    } else {
                        accountName = accountsFilteredData[k]["AccNickName"];
                        if (accountName !== null && accountName !== "") {
                            if (accountName.length > 40) accountName = accountName.substring(0, 40) + "  ...";
                        }
                    }
                    segRowData1["lblAccount"] = accountName ? accountName : "";
                    segRowData1["accountNumber"] = accountNumber;
                    segRowData1["lblAccountName"] = accountsFilteredData[k]["accountName"];
                    if (gblDiffAccountsQuickBalOrder) {
                        kony.print("Qbal:");
                        segRowData1["lblIncommingRing"] = {
                            "text": "s",
                            "isVisible": true
                        };
                        if (!isEmpty(accountsFilteredData[k]["QBHideShowFlag"]) && accountsFilteredData[k]["QBHideShowFlag"] === "T") { //&& gblQbalShowFlag === "Y"){
                            segRowData1["lblIncommingTick"] = {
                                "isVisible": true
                            };
                        } else {
                            segRowData1["lblIncommingTick"] = {
                                "isVisible": false
                            };
                        }
                    } else {
                        if (kony.os.deviceInfo().name === "Android" || kony.os.deviceInfo().name === "android") {
                            segRowData1["lblIncommingRing"] = {
                                "text": "W",
                                "isVisible": true,
                                "right": "2%",
                                "left": null
                            };
                        } else {
                            segRowData1["lblIncommingRing"] = {
                                "isVisible": false
                            };
                        }
                        var accountName = accountName ? accountName : "";
                        if (kony.store.getItem("langPrefObj") === "ar") {
                            segRowData1["lblAccount"] = {
                                "text": accountName,
                                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                                "right": "20%",
                                "left": null
                            };
                        } else {
                            segRowData1["lblAccount"] = {
                                "text": accountName,
                                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                                "right": null,
                                "left": "5%"
                            };
                        }
                        segRowData1["lblIncommingTick"] = {
                            "isVisible": false
                        };
                        segRowData1["template"] = flxSegQuickBalance;
                    }
                    kony.print("accountsFilteredData segRowData:::" + JSON.stringify(segRowData1));
                    accountsMasterdata.push(segRowData1);
                }
            }
            /*else{
                      kony.print("No data11preview::::");
                      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                    customAlertPopup("", geti18nkey("i18n.alerts.NoAccountsMsg"),popupCommonAlertDimiss,"");
                    }*/
        }
        /*else{
                kony.print("No data22::::");
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                customAlertPopup("", geti18nkey("i18n.alerts.NoAccountsMsg"),popupCommonAlertDimiss,"");
              }*/
    }
    kony.print("accountsMasterdata" + JSON.stringify(accountsMasterdata));
    if (gblDiffAccountsQuickBalOrder) frmAuthorizationAlternatives.segAccounts.setData(accountsMasterdata);
    else frmAccountsReorderKA.segReOrderAccounts.setData(accountsMasterdata);
    if (gblDiffAccountsQuickBalOrder) {
        kony.print("seg length" + frmAuthorizationAlternatives.segAccounts.data.length);
        if (gblQbalShowFlag === "Y") {
            frmAuthorizationAlternatives.flxSwitchOff.isVisible = false;
            frmAuthorizationAlternatives.flxSwitchOn.isVisible = true;
        } else {
            frmAuthorizationAlternatives.flxSwitchOn.isVisible = false;
            frmAuthorizationAlternatives.flxSwitchOff.isVisible = true;
        }
        if (gblQtransFlagShowFlag === "Y") {
            frmAuthorizationAlternatives.flxSwitchTransactionOff.isVisible = false;
            frmAuthorizationAlternatives.flxSwitchTransactionsOn.isVisible = true;
        } else {
            frmAuthorizationAlternatives.flxSwitchTransactionsOn.isVisible = false;
            frmAuthorizationAlternatives.flxSwitchTransactionOff.isVisible = true;
        }
        if (!isEmpty(frmAuthorizationAlternatives.segAccounts.data) && frmAuthorizationAlternatives.segAccounts.data.length > 0) {
            kony.print("Inside seg");
            frmAuthorizationAlternatives.segAccounts.isVisible = true;
            frmAuthorizationAlternatives.LabelNoRecordsKA.isVisible = false;
        } else {
            kony.print("no data");
            frmAuthorizationAlternatives.segAccounts.isVisible = false;
            frmAuthorizationAlternatives.LabelNoRecordsKA.isVisible = true;
        }
        frmAuthorizationAlternatives.show();
    } else {
        if (frmAccountsReorderKA.segReOrderAccounts.data.length > 0) {
            kony.print("Inside segfrmAccountsReorderKA");
            frmAccountsReorderKA.segReOrderAccounts.isVisible = true;
            frmAccountsReorderKA.LabelNoRecordsKA.isVisible = false;
        } else {
            kony.print("no datafrmAccountsReorderKA");
            frmAccountsReorderKA.segReOrderAccounts.isVisible = false;
            frmAccountsReorderKA.LabelNoRecordsKA.text = geti18nkey("i18n.alerts.NoAccountsMsg");
            frmAccountsReorderKA.LabelNoRecordsKA.isVisible = true;
        }
        frmAccountsReorderKA.lblSettingsTitle.text = geti18nkey("i18n.appsettings.ReOrderMyAccountList");
        frmAccountsReorderKA.lblReorderDesc.text = geti18nkey("i18n.reorderDesc");
        frmAccountsReorderKA.forceLayout();
        frmAccountsReorderKA.show();
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function getAccountDataforQuickPreviewAccountsError(err) {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("getAccountDataforQuickPreviewAccountsError ::" + JSON.stringify(err));
    var errorCode = "";
    if (err.code !== null) {
        errorCode = err.code;
    } else if (err.opstatus !== null) {
        errorCode = err.opstatus;
    } else if (err.errorCode !== null) {
        errorCode = err.errorCode;
    }
    var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
    customAlertPopup(geti18Value("i18n.NUO.Error"), errorMessage, popupCommonAlertDimiss, "");
}

function onrowClickAccountsSwitchOptionChange() {
    kony.print("onrowClickAccountsSwitchOptionChange" + frmAuthorizationAlternatives.segAccounts.selectedRowIndex)
    var rowIndex = frmAuthorizationAlternatives.segAccounts.selectedRowIndex[1];
    var segData = frmAuthorizationAlternatives.segAccounts.data[parseInt(rowIndex)];
    kony.print("segData:::" + JSON.stringify(segData) + "--visibility check::" + segData.lblIncommingTick.isVisible)
    segData.lblIncommingTick.isVisible = !segData.lblIncommingTick.isVisible;
    frmAuthorizationAlternatives.segAccounts.setDataAt(segData, parseInt(rowIndex), 0)
    var QuickSegData = frmAuthorizationAlternatives.segAccounts.data;
    kony.print("seg length:" + frmAuthorizationAlternatives.segAccounts.data.length);
    var segLength = 0;
    for (var i in QuickSegData) {
        if (!QuickSegData[i].lblIncommingTick.isVisible) {
            segLength++;
        }
    }
    kony.print("segLength--:" + segLength)
    if (segLength === frmAuthorizationAlternatives.segAccounts.data.length - 1) {
        if (!frmAuthorizationAlternatives.flxSwitchOn.isVisible) {
            customAlertPopup(geti18Value("i18n.NUO.Error"), kony.i18n.getLocalizedString("i18n.showSwipeQuick.desc"), popupCommonAlertDimiss, "");
        }
    }
    if (segLength === frmAuthorizationAlternatives.segAccounts.data.length) {
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = false;
        frmAuthorizationAlternatives.flxSwitchTransactionOff.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchTransactionsOn.isVisible = false;
    } else {
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = false;
    }
}