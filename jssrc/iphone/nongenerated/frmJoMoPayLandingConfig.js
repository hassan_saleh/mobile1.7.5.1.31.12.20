var frmJoMoPayLandingConfig = {
    "formid": "frmJoMoPayLanding",
    "frmJoMoPayLanding": {
        "entity": "p2pregistration",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        }
    },
    "segJomopayDetails": {
        "fieldprops": {
            "widgettype": "Segment",
            "entity": "p2pregistration",
            "additionalFields": ["jomopaytypeval", "paymentamt", "description", "frmAccountCurr", "paymentdate"],
            "field": {
                "lblAmount": {
                    "widgettype": "Label",
                    "field": "paymentamt"
                },
                "lblPayeeName": {
                    "widgettype": "Label",
                    "field": "jomopaytypeval"
                },
                "lblDescription": {
                    "widgettype": "Label",
                    "field": "description"
                },
                //               "lblCurrency":{
                //                 	"widgettype": "Label",
                //                     "field": "frmAccountCurr"
                //               },
                "lblTransactiondate": {
                    "widgettype": "Label",
                    "field": "paymentdate"
                }
            }
        }
    }
};