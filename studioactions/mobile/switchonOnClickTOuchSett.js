function switchonOnClickTOuchSett(eventobject) {
    return AS_FlexContainer_d856355e89594d518c952fbacaeae3e6(eventobject);
}

function AS_FlexContainer_d856355e89594d518c952fbacaeae3e6(eventobject) {
    var model = kony.os.deviceInfo().model;
    if (checkFaceIdIphone()) {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), kony.i18n.getLocalizedString("i18n.FaceID.SureDisableText"), EnableDisableFingerPrintLogin, popupCommonAlertDimiss, kony.i18n.getLocalizedString("i18n.common.YES"), kony.i18n.getLocalizedString("i18n.common.NO"));
    } else {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), kony.i18n.getLocalizedString("i18n.Touch.SureDisableText"), EnableDisableFingerPrintLogin, popupCommonAlertDimiss, kony.i18n.getLocalizedString("i18n.common.YES"), kony.i18n.getLocalizedString("i18n.common.NO"));
    }
}