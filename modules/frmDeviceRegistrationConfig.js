var frmDeviceRegistrationConfig = {
    "formid": "frmDeviceRegistration",
    "frmDeviceRegistration": {
        "entity": "DeviceRegistration",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        },
    },
    "lblLanguage": {
        "fieldprops": {
            "entity": "DeviceRegistration",
            "widgettype": "Label",
            "field": "lang"
        }
    },
    "lblDeviceInfo": {
        "fieldprops": {
            "entity": "DeviceRegistration",
            "widgettype": "Label",
            "field": "devInfo"
        }
    },
  "lblCustId": {
        "fieldprops": {
            "entity": "DeviceRegistration",
            "widgettype": "Label",
            "field": "custId"
        }
    },
    "lblDeviceId": {
        "fieldprops": {
            "entity": "DeviceRegistration",
            "widgettype": "Label",
            "field": "deviceId"
        }
    },
    "lblRegisterFlag": {
        "fieldprops": {
            "entity": "DeviceRegistration",
            "widgettype": "Label",
            "field": "RegisterFlag"
        }
    },
    "lblKSID": {
        "fieldprops": {
            "entity": "DeviceRegistration",
            "widgettype": "Label",
            "field": "ksid"
        }
    },
    "lblAppID": {
        "fieldprops": {
            "entity": "DeviceRegistration",
            "widgettype": "Label",
            "field": "appid"
        }
    },
    "lblUserId": {
        "fieldprops": {
            "entity": "DeviceRegistration",
            "widgettype": "Label",
            "field": "user_id"
        }
    }
  
  
};


