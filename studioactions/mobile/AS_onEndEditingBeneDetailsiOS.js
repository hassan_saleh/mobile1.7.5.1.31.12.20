function AS_onEndEditingBeneDetailsiOS(eventobject, changedtext) {
    return AS_TextField_ed59c473c6c4401ebedace3d7f85476a(eventobject, changedtext);
}

function AS_TextField_ed59c473c6c4401ebedace3d7f85476a(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxBeneRelation.text !== "") {
        frmAddExternalAccountKA.flxUnderlineBeneRelation.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineBeneRelation.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblBeneRelation", frmAddExternalAccountKA.tbxBeneRelation.text);
}