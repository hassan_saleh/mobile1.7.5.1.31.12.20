function AS_onClickDeletePayee(eventobject) {
    return AS_Button_i77a9d5add9c46438b031fbd1d7046f9(eventobject);
}

function AS_Button_i77a9d5add9c46438b031fbd1d7046f9(eventobject) {
    customAlertPopup(geti18nkey("i18n.common.DeleteBill"), geti18nkey("i18n.common.deleteBillText"), deletePayeeFromInside, popupCommonAlertDimiss, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
}