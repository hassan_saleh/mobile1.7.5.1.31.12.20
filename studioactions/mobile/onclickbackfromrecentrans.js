function onclickbackfromrecentrans(eventobject) {
    return AS_Button_e3c6eee298c44f14a44cd8fc1fce382c(eventobject);
}

function AS_Button_e3c6eee298c44f14a44cd8fc1fce382c(eventobject) {
    var previousForm = kony.application.getPreviousForm().id;
    if (previousForm == "frmAccountDetailKA") {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen();
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmAccountDetailKA");
        controller.getFormModel().showView();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } else {
        backToTransferPayLandingPage("frmRecentTransactionDetailsKA");
    }
}