function onSelectionDOBCalenderJoMoPayRegistration(eventobject, isValidDateSelected) {
    return AS_Calendar_e7dffb8d961e42bf8f4cf3cb6f458de7(eventobject, isValidDateSelected);
}

function AS_Calendar_e7dffb8d961e42bf8f4cf3cb6f458de7(eventobject, isValidDateSelected) {
    animate_NEWCARDSOPTIOS("UP", "lblDateOfBirthstatic", frmJoMoPayRegistration.lblDateOfBirth.text);
    var field = frmJoMoPayRegistration.calDateDOB;
    var date = field.dateComponents[0] < 10 ? "0" + field.dateComponents[0] : field.dateComponents[0];
    var month = field.dateComponents[1] < 10 ? "0" + field.dateComponents[1] : field.dateComponents[1];
    frmJoMoPayRegistration.lblDateOfBirth.text = date + "/" + month + "/" + field.dateComponents[2];
}