pieChart = "";
ColumnChart = "";
var userAgent = kony.os.userAgent();
var size;
var legendFont;
if (userAgent == "iPhone") {
  size = [8];
  sz = [10];
} else {
  size = [22];
  sz = [24];
}
if (userAgent == "iPhone") {
  legendFont = [9];
} else {
  legendFont = [23];
}

kony = kony || {};
kony.retailBanking = kony.retailBanking || {};
kony.retailBanking = {
  AreaChartGenereted: false,
  columnChartGenereted: false,
  pieChartGenerated: false,
  columnChartGeneretedios: false,
  DatesOfMonth: [],
  closingValuesOfDate1: [],
  closingValuesOfDate2: [],
  barGraphRecordsList: [],
  barGraphMonthNamesList: [],
  barGraphFullMonthNamesList: [],
  barGraphtotalCashMonth: [],
  barGraphMonthIdList: [],
  MonthDetailsCashFlowPieChart: [],
  CategoryNameList: [],
  LAST_MONTH: "",
  THIS_MONTH: "",
  CategoryIdList: [],
  TransactionList: [],
  columnMajorIntervals: null,
  minimumValue: null,
  maxValue: null,
  selectedMonthId: null,
  fromAppMenu: false,
  onClickMonthOverview: function(args) {
    var columnNumber = args["columnNumber"];
    var rowNumber = args["rowNumber"];
    var MonthId = kony.retailBanking.barGraphMonthIdList[rowNumber];
    kony.retailBanking.selectedMonthId = MonthId;
    fetchPieChartData(MonthId, success, error);

    function success() {
      populateSegLegend();
      pieChart = Pie_createChartWidget();
      frmMyMoneyListKA.flxMonthlySpendingKAinner.add(pieChart);
      kony.retailBanking.settingSpecificMonth(kony.retailBanking.barGraphFullMonthNamesList[rowNumber]);
    }

    function error() {
      //Handle error case
      kony.sdk.mvvm.log.error("In fetching Pie Chart data");
    }
  },
  settingSpecificMonth: function(Month) {
    frmMyMoneyListKA.resourcesLabel.skin = sknMonthlySpending3278E6KA;
    frmMyMoneyListKA.flxJanMnthKA.isVisible = true;
    frmMyMoneyListKA.lblMonthlySpendingKA.text = "> " + Month;
    frmMyMoneyListKA.flxMonthlySpendingKA.isVisible = true;
    frmMyMoneyListKA.flxSpendingOverviewKA.isVisible = false;
    frmMyMoneyListKA.flxExpenditureKA.isVisible = false;
    frmMyMoneyListKA.flxsegMonthlyDataKA.isVisible = false;
  },
  settingSpecificTransactionList: function(categoryName, categoryID) {
    fetchMyModuleTransactionList(kony.retailBanking.selectedMonthId, categoryID, success, error);

    function success() {
      frmMyMoneyListKA.resourcesLabel.skin = sknMonthlySpending3278E6KA;
      frmMyMoneyListKA.lblMonthlySpendingKA.skin = sknMonthlySpending3278E6KA;
      frmMyMoneyListKA.lblExpenditureKA.text = "> " + categoryName;
      frmMyMoneyListKA.flxJanMnthKA.isVisible = true;
      frmMyMoneyListKA.flxExpenditureKA.isVisible = true;
      frmMyMoneyListKA.flxMonthlySpendingKA.isVisible = false;
      frmMyMoneyListKA.flxSpendingOverviewKA.isVisible = false;
      frmMyMoneyListKA.flxsegMonthlyDataKA.isVisible = true;
    }

    function error() {
      //Handle error case
      kony.sdk.mvvm.log.error("In fetching Pie Chart data");
    }
  }
};
//creating chart widget...
function createChartWidget() {
  kony.retailBanking.AreaChartGenereted = true;
  var chartObj = createChartJSObject();
  var chartWidget = new kony.ui.Chart2D3D({
    "id": "chartid",
    "isVisible": true,
    "height": "80%",
    "width": "100%",
    "chartheight": "80%",
    "chartwidth": "100%"
  }, {
    "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
    "containerWeight": 100
  }, chartObj);
  return chartWidget;
}
//creating chart object with chart properties and chart data...
function createChartJSObject() {
  var chartInfo = {
    "chartProperties": {
      "chartHeight": "80%",
      "position": [0, 0, 100, 100],
      "title": {
        "visible": false,
        "text": "April Cash Flow",
        "font": {
          "size": size,
          "family": ["Verdana"],
          "style": ["normal"],
          "color": ["0xaaaaaaff"],
          "transparency": [0]
        },
        "position": "top",
        "alignment": "center",
        "direction": "up",
        "containerWt": 10,
        "margin": [0, 0, 0, 0],
        "background": {
          "fillType": "gradient",
          "transparency": 100,
          "gradientType": "linearTopToBottom",
          "gradientRatios": [0, 100]
          //"color": ["0x0812A4FF","0x78a0c8ff"]
        }
      },
      "layerArea": {
        "background": {
          "fillType": "gradient",
          "transparency": 100,
          "gradientType": "linearTopToBottom",
          "gradientRatios": []
          //"color": ["0x0812A4FF","0x78a0c8ff"]
        }
      },
      "dataSetMapping": {
        "setId": "dataset1",
        "eventsSetId": "eventsMap1"
      },
      "axis": {
        "type": ["xAxis", "secondaryYAxis"],
        "xAxis": {
          "scale": {
            "type": "auto"
          },
          "axisLine": {
            "line": {
              "color": ["0xffffffff"]
            }
          },
          "labels": {
            "margin": [0, 0, 0, 0],
            "orientationAngle": 0,
            "skipLabelInterval": 5,
            "font": {
              "size": size,
              "family": ["Verdana"],
              "style": ["normal"],
              "color": ["0xffffffff"],
              "transparency": [0]
            }
          }
        },
        "secondaryYAxis": {
          "scale": {
            "offset": {
              "value": [0, 0],
              "type": "pixels"
            }
          },
          "axisLine": {
            "line": {
              "color": ["0xffffffff"]
            }
          },
          "labels": {
            "margin": [0, 0, 0, 0],
            "font": {
              "size": size,
              "family": ["Verdana"],
              "style": ["normal"],
              "color": ["0xffffffff"],
              "transparency": [0]
            }
          }
        }
      },
      "grid": {
        "type": ["secondaryYAxisMajorGrid"],

        "secondaryYAxisMajorGrid": {
          "line": {
            "transparency": [80],
            "color": ["0xffffffff"]
          }
        }
      },
      "drawEntities": ["axis","grid","areaChart"],
      "drawArea": {
        "margin": [0, 10, 0, 0]
      },
      "areaChart": {
        "columnId": [0],
        "animations": {
          "onInitAnimation": true
        },
        "graphType": "normal",
        "lineType": "normal",
        "dataAlignToAxis": ["secondaryYAxis"],
        "plotMissingValues": "assumeZero",
        "area": {
          "fillType": ["gradient"],
          "transparency": [90],
          "colorAboveXAxis": [["0x5a96aaff", "0x2878aaff"]]
        },
        "line": {
          "visible": true,
          "color": ["0x8cc3e6ff"],
          "width": [1],
          "transparency": [0]
        },
        "plotPoints": {
          "visible": false,
          "colorIndicator": "columns",
          "marker": {
            "type": ["circle"],
            "fillType": "color"
          },
          "color": ["0x2a99ceff"],
          "transparency": [0],
          "size": size
        },
        "dataLabels": null
      }
    },
    "chartData": {
      "rowNames": {
        "values": kony.retailBanking.DatesOfMonth
      },
      "columnNames": {
        "values": ["Money"]
      },
      "data": {
        "Money": kony.retailBanking.closingValuesOfDate1
      }
    },
    "chartEvents": {
      "events": ["eventsMap1"],
      "eventsMap1": {
        "onPinchZoom": {
          "minimumZoomScale": 1,
          "maximumZoomScale": 2
        },
        "onTouch": {
          "crossHair": {
            "line": {
              "color": ["0xffffffff"],
              "width": [1],
              "transparency": [0]
            }
          },
          "dataLabels": {
            "visible": true,
            "indicators": ["numberValue"],
            "separator": "space",
            "font": {
              "size": size,
              "family": ["Verdana"],
              "style": ["normal"],
              "color": ["0xffffffff"],
              "transparency": [0]
            }
          },
          "border": {
            "visible": false,
            "line": {
              "color": ["0xffffffff"],
              "width": [0],
              "transparency": [0]
            }
          },
          "background": {
            "fillType": "gradient",
            "transparency": 100,
            "gradientType": "linearTopToBottom",
            "gradientRatios": []
            //"color": ["0x0812A4FF","0x78a0c8ff"]
          }
        }
      }
    }
  };
  return chartInfo;
}
//creating chart widget...
function line_createChartWidget(chartData,accountType) {
  try
    {
      var len=accountType.length;
      for(var i=0;i<len;i++)
        {
          gblColorArray[i]=gblColor[i];
        }
  var chartObj = line_createChartJSObject(accountType,gblColorArray);
      kony.print("Chart Object :: "+JSON.stringify(chartObj));
  var chartWidget = new kony.ui.Chart2D3D({
    "id": "linechartid",
    "isVisible": true,
    "height": "72%",
    "width": "90%",
    "left":"5%",
    //"chartheight": "60%",
    "chartwidth": "90%"
  }, {
    "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
    "containerWeight": 100
  }, chartObj);
      chartWidget.setData(chartData);
      kony.print("Chart Widget:: "+JSON.stringify(chartWidget));
      
  return chartWidget;
    }
  catch(e)
    {
      kony.print("Exception in creating line chart:: "+e);
    }
}
//creating chart object with chart properties and chart data...
function line_createChartJSObject(accountType,gblColorArray) {
  var chartInfo = {
        "chartProperties": {
                "chartHeight": 72,
                "enableScrolling": false,
                "position": [0, 0, 100, 100],
//                 "title": {
//                     "visible": true,
//                     "text": "Production Output indicator: Multi Chart, PinchToZoom, Crosshair",
//                     "font": {
//                         "size": ["20"],
//                         "family": ["Verdana"],
//                         "style": ["Bold"],
//                         "color": ["0x000000ff"],
//                         "transparency": [30]
//                     },
//                     "position": "top",
//                     "alignment": "center",
//                     "direction": "up",
//                     "containerWt": 10,
//                     "margin": [0, 0, 0, 0],
//                     "background": {
//                         "fillType": "gradient",
//                         "transparency": 0,
//                         "gradientType": "linearTopToBottom",
//                         "gradientRatios": [0, 20, 50, 100],
//                         "color": ["0xffffffff"]
//                     }
//                 },
                 "legend": {
                    "visible": true,
                    "indicators": ["marker", "textLabel"],
                    "separator": "space",
                    "marker": {
                        "type": "colorCircle",
                        "color": gblColorArray
                    },
//                     "rowName": {
//                         "color": ["0x169EECFF", "0xBE0056ff", "0xFCC40EFF"],
//                         "margin": [5, 5, 0, 0]
//                     },
//                     "numberValue": {
//                         "color": ["0x169EECFF", "0xBE0056ff", "0xFCC40EFF"],
//                         "margin": [5, 5, 0, 0]
//                     },
//                     "percentValue": {
//                         "color": ["0x169EECFF", "0xBE0056ff", "0xFCC40EFF"],
//                         "margin": [5, 5, 0, 0]
//                     },
//                     "columnName": {
//                         "color": ["0x169EECFF", "0xBE0056ff", "0xFCC40EFF"],
//                         "margin": [5, 5, 0, 0]
//                     },
                    "textLabel": {
                        "text": accountType,
                        "color": gblColorArray,
                        "margin": [3, 5, 1, 1]
                    },
//                     "textValue": {
//                         "text": ["$909", "$309", "$609"],
//                         "color": ["0x169EECFF", "0xBE0056ff", "0xFCC40EFF"],
//                         "margin": [5, 5, 0, 0]
//                     },
                    "font": {
                        "size": [15],
                        "family": ["Verdana"],
                        "style": ["normal"],
                        "color": ["0xaaaaaaff"],
                        "transparency": [0]
                    },
                    "position": "bottom",
                    "alignment": "right",
                    "layout": "horizantal",
                    "containerWt": 8,
                    "margin": [10, 10, 10, 10],
                    "padding": [1, 1, 1, 1],
                    "background": {
                        "fillType": "gradient",
                        "transparency": 100,
                        "gradientType": "linearTopToBottom",
                        "gradientRatios": [0, 20, 50, 100],
                        "color": ["0xff1234ff", "0xffffffff", "0x12121212", "0x23456789"]
                    }
                },
                "layerArea": {
                    "background": {
                        "fillType": "gradient",
                        "transparency": 100,
                        "gradientType": "linearTopToBottom",
                        "gradientRatios": [0, 20, 50, 100],
                        "color": ["0xffffffff"],
                      
                    }
                },
//                 "dataSetMapping": {
//                     "setId": "dataset1",
//                     "eventsSetId": null
//                 },
                "axis": {
                    "type": ["xAxis", "yAxis"],
                    "xAxis": {
                        "scale": {
                            "type": "fixedGapMajorInterval",
                            "gap": 7
                        },
                        "axisLine": {
                            "visible": false,
                            "crossOtherAxisAt": "value"
                        },
                        "labels": {
                            "orientationAngle": 0,
                            "font": {
                                "size": [15],
                                "family": ["Verdana"],
                                "style": ["normal"],
                                "color": ["0x00A49Aff"],
                                "transparency": [0]
                            }
                        }
                    },
                    "yAxis": {
                        "axisLine": {
                            "visible": false,
                            "crossOtherAxisAt": "start"
                        },
//                       "scale": {
//                             "type": "fixedGapMajorInterval",
//                             "gap": 100
//                         },
                                "scale": {
            "minValue": 0,
            "maxValue": 1000,
            "majorInterval": 100
          },
                        "labels": {
                            "margin": [30, 0, 0, 0],
                            "font": {
                                "size": [15],
                                "family": ["Verdana"],
                                "style": ["normal"],
                                "color": ["0x00A49Aff"],
                                "transparency": [10]
                            }
                        }
                    }
                },
                "grid": {
                    "type": ["yAxisMajorGrid"],
                    "yAxisMajorGrid": {
                        "line": {
                            "color": ["0x0966A6ff"]
                        }
                    }
                },
                "lineChart": {
                    "columnId": [0, 1, 2, 3, 4],
                    "animations": {
                        "onInitAnimation": true
                    },
                    "graphType": "normal",
                    "lineType": "normal",
                    "dataAlignToAxis": ["primaryYAxis"],
                    "plotMissingValues": "assumeZero",
                    "line": {
                        "color": gblColorArray,
                        "width": [3],
                        "transparency": [0]
                    },
                    "plotPoints": {
                        "visible": false,
                        "colorIndicator": "columns",
                        "marker": {
                            "type": ["bubble", "bubble", "bubble"],
                            "fillType": "color"
                        },
                        "color": ["0xa9e200ff", "0x22b8dbff", "0xf7d700ff"],
                        "transparency": [0],
                        "size": [12]
                    },
                    "dataLabels": null
                },
                "drawEntities": ["axis", "grid", "areaChart", "lineChart"],
                "areaChart": {
                    "columnId": [3],
                    "animations": {
                        "onInitAnimation": true
                    },
                    "graphType": "normal",
                    "lineType": "normal",
                    "dataAlignToAxis": ["primaryYAxis"],
                    "plotMissingValues": "assumeZero",
                    "area": {
                        "fillType": ["color"],
                        "transparency": [100],
//                         "color": ["0x00ff00ff"],
//                         "colorAboveXAxis": ["0xB4B4B4FF"],
//                         "colorBelowXAxis": ["0xff0000ff"],
                        "visible": false
                    },
//                     "line": {
//                         "visible": false,
//                         "color": ["0xff0000ff"],
//                         "width": [1],
//                         "transparency": [0]
//                     },
                    "plotPoints": {
                        "visible": false
                    }
                }
            },
//             "chartData": {
//                 "rowNames": {
//                     "values": ["01", "05", "10", "15", "20", "25", "30"]
//                 },
//                 "columnNames": {
//                     "values": ["Deposit", "Cash", "Credit", "Target", "Achieved"]
//                 },
//                 "data": {
//                     "Deposit": [234, 236, 224, 244, 240, 218, 256],
//                     "Cash": ["", 512, 514, 526, 534, 536, 522],
//                     "Credit": [344, 335, 336, 334, 332, 324, 322],
//                     "Achieved": [78, 79, 67, 87, 76, 54, 34],
//                     "Target": [120, 120, 120, 120, 120, 120, 120]
//                 }
//             },
            "chartEvents": {
                "onPinchZoom": {
                    "minimumZoomScale": 1,
                    "maximumZoomScale": 2
                },
                "onTouch": {
                    "crossHair": {
                        "line": {
                            "color": ["0x724C9FFF"],
                            "width": [1],
                            "transparency": [0]
                        }
                    },
                    "dataLabels": {
                        "visible": true,
                        "indicators": ["numberValue"],
                        "separator": "space",
                        "font": {
                            "size": [14],
                            "family": ["Verdana"],
                            "style": ["Bold"],
                            "color": ["0xAAAAAAFF"],
                            "transparency": [0]
                        }
                    },
                    "border": {
                        "visible": true,
                        "line": {
                            "color": ["0x9fd500ff"],
                            "width": [1],
                            "transparency": [0]
                        }
                    },
                    "background": {
                        "fillType": "color",
                        "transparency": 0,
                        "gradientType": "linearTopToBottom",
                        "gradientRatios": [],
                        "color": ["0xffffffff"]
                    }
                }
            }
        };
  return chartInfo;
}
//creating chart widget...
function column_createChartWidget() {
  var chartObj = column_createChartJSObject();
  var chartWidget = new kony.ui.Chart2D3D({
    "id": "chartColumnid",
    "height": "100%",
    "width": "100%",
    "chartheight": "80%",
    "chartwidth": "100%",
    "isVisible": true
  }, {
    "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
    "containerWeight": 100
  }, chartObj);
  return chartWidget;
}
//creating chart object with chart properties and	chart data...
//creating chart object with chart properties and	chart data...
function column_createChartJSObject() {
  var chartInfo = {
    "chartProperties": {
      "chartHeight": "80%",
      "title": {
        "text": "",
        "font": {
          "size": size,
          "family": ["Verdana"],
          "style": ["normal"],
          "color": ["0xffffffff"]
        },
        "containerWt": 12,
        "background": {
          "transparency": 100,
          "fillType": "gradient",
          "color": ["0x070707ff", "0x323232FF"]
        }
      },
      "layerArea": {
        "background": {
          "transparency": 100,
          "fillType": "gradient",
          "color": ["0x070707ff", "0x323232FF"]
        }
      },
      "axis": {
        "xAxis": {
          "title": {
            "text": "",
            "font": {
              "size": size,
              "family": ["Verdana"],
              "style": ["Bold"],
              "color": ["0xaaaaaaff"]
            }
          },
          "axisLine": {
            "visible": false
          },
          "labels": {
            "font": {
              "size": size,
              "family": ["Verdana"],
              "style": ["normal"],
              "color": ["0x000000ff"]
            }
          }
        },
        "yAxis": {
          "scale": {
            "minValue": kony.retailBanking.minimumValue,
            "maxValue": kony.retailBanking.maxValue,
            "majorInterval": kony.retailBanking.columnMajorIntervals
          },
          "axisLine": {
            "visible": false
          }
        }
      },
      "grid": {
        "type": ["yAxisMajorGrid"],
        "yAxisMajorGrid": {
          "visible": false
        },
        "background": {
          "fillType": "alternateColor",
          "alternateColorPattern": "yMajorMajor",
          "transparency": 80,
          "color": ["0x323232FF", "0x070707FF"]
        }
      },
      "drawEntities": ["axis", "columnChart"],
      "columnChart": {
        "animations": {
          "onInitAnimation": true
        },
        "bar": {
          "fillType": ["color"],
          "color": [
            ["0x64A1D6ff"]
          ]
        },
        "border": {
          "line": {
            "color": ["0xaaaaaaff"]
          }
        },
        "dataLabels": {
          "placement": "above",
          "font": {
            "size": size,
            "family": ["Verdana"],
            "style": ["normal"],
            "color": ["0x000000ff"]
          },
          "format": {
            "prefix": ["$"],
            "suffix": [""]
          }
        }
      }
    },
    "chartData": {
      "rowNames": {
        "values": kony.retailBanking.barGraphMonthNamesList
      },
      "columnNames": {
        "values": ["c1"]
      },
      "data": {
        "c1": kony.retailBanking.barGraphtotalCashMonth
      }
    },
    "chartEvents": {
      "events": ["eventsMap1"],
      "eventsMap1": {
        "onTap": onTap,
        "onPinchZoom": {
          "minimumZoomScale": 1,
          "maximumZoomScale": 2
        },
        "onTouch": {
          "crossHair": {
            "line": {
              "color": ["0xAAAAAAFF"],
              "transparency": [100]
            }
          },
          "dataLabels": {
            "placement": "above",
            "indicators": ["rowName", "numberValue"],
            "font": {
              "size": size,
              "family": ["Verdana"],
              "style": ["normal"],
              "color": ["0x000000ff"]
            }
          },
          "border": {
            "line": {
              "color": ["0x000000ff"]
            }
          },
          "background": {
            "fillType": "color",
            "color": ["0xFFFFFFFF"],
          }
        }
      }
    }
  };
  return chartInfo;
}

function onTap(args) {
  kony.retailBanking.onClickMonthOverview(args);
}
//creating chart widget...
function Pie_createChartWidget(chartObj) {
//   var chartObj = Pie_createChartJSObject();
  var chartWidget = new kony.ui.Chart2D3D({
    "id": "chartPieid",
    "isVisible": true,
    "height": "100%",
    "width": "100%",
    "centerX": "50%",
    "centerY": "40%",
    "chartheight": "100%",
    "chartwidth": "100%"
  }, {
    "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE,
    "padding": [5, 5, 5, 5],
    "margin": [5, 5, 5, 5],
    "hExpand": true,
    "vExpand": true,
    "containerWeight": 100
  }, chartObj);
//   var chartData = {
//     "rowNames": {
//       "values": kony.retailBanking.CategoryNameList
//     },
//     "columnNames": {
//       "values": ["Amount1"]
//     },
//     "data": {
//       "Amount1": kony.retailBanking.MonthDetailsCashFlowPieChart
//     }
//   }
 // chartWidget.setData(chartData);
  return chartWidget;
}
//creating chart object with chart properties and chart data...
function Pie_createChartJSObject() {//rowNames,columnNames,data
  var chartInfo = {
	"chartProperties": {
			"position": [0, 0, 100, 100],
            "drawEntities": ["donutChart"],
            "chartHeight": 60,

            "layerArea": {
                "background": {
                  "fillType": "gradient",
                  "transparency": 100,
                  "gradientType": "linearTopToBottom",
                  "gradientRatios": [0, 30, 70, 100],
                  "color": ["0xecedf0FF", "0xbabec8FF", "0xadb1bcFF", "0xecedf0FF"]
                }
            },
            "donutChart": {
            	"holeRadius": 60,
                "border": {
                	"visible": false,
                    "line": {
                        "color": ["0xffffffff"]
                    }
                },

                "pieSlice": {
                    "color": ["0x3165cbff", "0xdc3812ff", "0xff9700ff", "0x11951bff", "0x990098ff", "0x0099c5ff"]
                },

                "dataLabels": {
                	"placement": "outside",
                	"indicators": ["rowName"],
                    "font": {
                        "size": [15],
                        "color": ["0xffffffff"]
                    }
                }
            }
        },

        "chartData": {
            "columnNames": {
                "values": ["2013"]
            },

            "data": {
                "2013": [25, 20, 14, 18, 17, 13]
            },

            "rowNames": {
                "values": ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
            }
        }

    };
    return chartInfo;
}

function onTapPieChart(args) {
  var columnNumber = args["columnNumber"];
  var rowNumber = args["rowNumber"];
  var categoryName = kony.retailBanking.CategoryNameList[rowNumber];
  var categoryID = kony.retailBanking.CategoryIdList[rowNumber];
  kony.retailBanking.settingSpecificTransactionList(categoryName, categoryID);
}

function serv_getStatistics(){
	var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
	kony.print("from Accounts :: "+JSON.stringify(fromAccounts[0]));
    var date = new Date();
  

            var day = date.getDate();
            var langselected = (kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara");
            if (day < 10) {
                day = "0" + day;
            }

            var mon = date.getMonth() + 1;
            if (mon < 10) {
                mon = "0" + mon;
            }

            var year = date.getFullYear();
            var currDate = year + "-" + mon + "-" + day;
  
    var queryParams = 
    {
        "custid":custid,
        "loop_count": "1",
        "loop_seperator":":",
        "p_Account":kony.retailBanking.globalData.accountsDashboardData.accountsData[customerAccountDetails.currentIndex].accountID,
        "p_Branch":kony.retailBanking.globalData.accountsDashboardData.accountsData[customerAccountDetails.currentIndex].branchNumber,     
        "p_toDate":currDate,
        "lang":kony.store.getItem("langPrefObj") === "en"?"eng":"ara"
    };

    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopStatistics");
    if (kony.sdk.isNetworkAvailable()) {
    	kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    	kony.print("queryParams ::"+JSON.stringify(queryParams));
        appMFConfiguration.invokeOperation("LoopStatsService", {},queryParams,serv_getStatisticsSuccess,function(err){kony.print("error in service ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
	}
}

function serv_getStatisticsSuccess(){//response
	try{
    	//response = setDuplicatePieData(response);
//       kony.print("response ::"+JSON.stringify(response));
//     	kony.print("response ::"+JSON.stringify(response.LoopDataset[0].StatsCollection));
//     	if(response.LoopDataset[0].StatsCollection !== undefined && response.LoopDataset[0].StatsCollection !== null){
//           var pie = [];
//           var rowNames = [];
//           var columnNames = [];
//           var data = [];
//           var flag = false;
//           for(var i in response.LoopDataset[0].StatsCollection){
//           	var value = get_pieChartValues(response.LoopDataset[0].StatsCollection[i]);
//             kony.print("pie.length1: "+pie.length);
//           	  if(pie.length > 0){
//               	for(var j in pie){
//                 	flag = false;
//                 	kony.print("J ::"+j+" txnmodule :: "+value.txnmodule+" :: "+pie[j].txnmodule+"pie.length: "+pie.length+" (j === (pie.length-1)): "+((pie.length-1)));
//                 	if(value.txnmodule === pie[j].txnmodule){
//                       	kony.print("existing. add amount");
//                     	pie[j].amount += (parseInt(response.LoopDataset[0].StatsCollection[i].transactionamount));
//                     	flag = true;
//                       	break;
//                     }
//                   	if(!flag && (pie.length-1 == j)){//j === (pie.length-1)){
//                     	pie.push({"txnmodule":value.txnmodule,"rowNames":value.rowNames,"amount":value.amount});
//                     }
//                 }
//               }else{
//               	pie.push({"txnmodule":value.txnmodule,"rowNames":value.rowNames,"amount":value.amount});
//               }
//           }
//           for(var i in pie){
//           	rowNames.push(pie[i].rowNames);
//             data.push(pie[i].amount);
//           }
//           kony.print("data for pie chart ::"+JSON.stringify(pie));
          var chartData = Pie_createChartJSObject();//rowNames,["amount"],{"amount":data});
          var pieC = "";
          frmMyMoneyListKA.flxPieChart.remove(pieC);
          pieC = Pie_createChartWidget(chartData);
          frmMyMoneyListKA.lblNoChart.isVisible = false;
          var accountTitle = kony.retailBanking.globalData.accountsDashboardData.accountsData[customerAccountDetails.currentIndex].AccNickName !== ""?
            	kony.retailBanking.globalData.accountsDashboardData.accountsData[customerAccountDetails.currentIndex].AccNickName:kony.retailBanking.globalData.accountsDashboardData.accountsData[customerAccountDetails.currentIndex].accountName;
          accountTitle = kony.retailBanking.globalData.accountsDashboardData.accountsData[customerAccountDetails.currentIndex].accountID;
          frmMyMoneyListKA.lblAccountTitile.text = accountTitle;
//           frmMyMoneyListKA.lblAccountTitile.isVisible = true;
          frmMyMoneyListKA.flxPieChart.add(pieC);
//         }else{
//         	//frmMyMoneyListKA.lblNoChart.text = "No transactions to show.";//Swipe left to see other accounts.
//         	frmMyMoneyListKA.lblNoChart.isVisible = true;
//         	frmMyMoneyListKA.lblAccountTitile.isVisible = false;
//         }
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	frmMyMoneyListKA.show();
    }catch(e){
    	kony.print("Exception_serv_getStatistics ::"+e);
    }
}

function get_pieChartValues(pieChartData){
  var rowName = "";
  if(pieChartData.txnmodule === "1"){
    rowName = "ATM";
  }else if(pieChartData.txnmodule === "2"){
    rowName = "EFwatercoom";
  }else if(pieChartData.txnmodule === "3"){
    rowName = "Transfers";
  }else if(pieChartData.txnmodule === "4"){
    rowName = "Teller in & out";
  }else if(pieChartData.txnmodule === "5"){
    rowName = "Credit card payment";
  }else{
    pieChartData.txnmodule = "6";
    rowName = "Others";
  }
  return {"txnmodule":pieChartData.txnmodule,"rowNames":rowName,"amount":parseInt(pieChartData.transactionamount)};
}
/***********************
function setDuplicatePieData(response){
  var data = [];
  var testData = [{
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-04",
        "creditdebitflag": "D",
        "userrefnumber": "1",
        "txnrefnumber": "683539558-1",
        "txnuser": "",
        "transactionamount": "150",
        "currencycode": "JOD",
        "txnmodule": "1"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-04",
        "creditdebitflag": "D",
        "userrefnumber": "2",
        "txnrefnumber": "683539558-3",
        "txnuser": "",
        "transactionamount": "0.2",
        "currencycode": "JOD",
        "txnmodule": "1"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-04",
        "creditdebitflag": "D",
        "userrefnumber": "3",
        "txnrefnumber": "683539578-1",
        "txnuser": "",
        "transactionamount": "150",
        "currencycode": "JOD",
        "txnmodule": "3"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-04",
        "creditdebitflag": "D",
        "userrefnumber": "4",
        "txnrefnumber": "683539578-3",
        "txnuser": "",
        "transactionamount": "0",
        "currencycode": "JOD",
        "txnmodule": "3"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-10",
        "creditdebitflag": "D",
        "userrefnumber": "5",
        "txnrefnumber": "683539598-1",
        "txnuser": "",
        "transactionamount": "76",
        "currencycode": "JOD",
        "txnmodule": "4"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-10",
        "creditdebitflag": "D",
        "userrefnumber": "6",
        "txnrefnumber": "683539598-3",
        "txnuser": "",
        "transactionamount": "0.2",
        "currencycode": "JOD",
        "txnmodule": "5"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-10",
        "creditdebitflag": "D",
        "userrefnumber": "7",
        "txnrefnumber": "683539600-1",
        "txnuser": "",
        "transactionamount": "120",
        "currencycode": "JOD",
        "txnmodule": "5"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-10",
        "creditdebitflag": "D",
        "userrefnumber": "8",
        "txnrefnumber": "683539600-3",
        "txnuser": "",
        "transactionamount": "0",
        "currencycode": "JOD",
        "txnmodule": "5"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "9",
        "txnrefnumber": "683539673-1",
        "txnuser": "",
        "transactionamount": "76",
        "currencycode": "JOD",
        "txnmodule": "10"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "10",
        "txnrefnumber": "683539673-3",
        "txnuser": "",
        "transactionamount": "0.2",
        "currencycode": "JOD",
        "txnmodule": "10"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "11",
        "txnrefnumber": "683539676-1",
        "txnuser": "",
        "transactionamount": "1500",
        "currencycode": "JOD",
        "txnmodule": "12"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "12",
        "txnrefnumber": "683539676-3",
        "txnuser": "",
        "transactionamount": "0.2",
        "currencycode": "JOD",
        "txnmodule": "4"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "13",
        "txnrefnumber": "683539678-3",
        "txnuser": "",
        "transactionamount": "0.2",
        "currencycode": "JOD",
        "txnmodule": "3"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "14",
        "txnrefnumber": "683539678-1",
        "txnuser": "",
        "transactionamount": "43",
        "currencycode": "JOD",
        "txnmodule": "2"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "15",
        "txnrefnumber": "683539679-3",
        "txnuser": "",
        "transactionamount": "0.2",
        "currencycode": "JOD",
        "txnmodule": "5"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "16",
        "txnrefnumber": "683539679-1",
        "txnuser": "",
        "transactionamount": "76",
        "currencycode": "JOD",
        "txnmodule": "5"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "17",
        "txnrefnumber": "683539680-1",
        "txnuser": "",
        "transactionamount": "21",
        "currencycode": "JOD",
        "txnmodule": "2"
    }, {
        "currencydesc": "Jordanian Dinar",
        "transactiondate": "2017-01-11",
        "creditdebitflag": "D",
        "userrefnumber": "18",
        "txnrefnumber": "683539680-3",
        "txnuser": "",
        "transactionamount": "0.2",
        "currencycode": "JOD",
        "txnmodule": "13"
    }];
	for(var i in response.LoopDataset[0].StatsCollection){
    	data.push(response.LoopDataset[0].StatsCollection[i]);    
    }
	for(var i in testData){
    	data.push(testData[i]);
    }
	response.LoopDataset[0].StatsCollection = data;
	return response;
}
******************/