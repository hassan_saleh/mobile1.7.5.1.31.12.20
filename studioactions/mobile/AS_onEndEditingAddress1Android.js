function AS_onEndEditingAddress1Android(eventobject, changedtext) {
    return AS_TextField_d8e411ce7a7c4bfaa9743912bc753167(eventobject, changedtext);
}

function AS_TextField_d8e411ce7a7c4bfaa9743912bc753167(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxAddress1.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAddress1.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAddress1.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAddress1", frmAddExternalAccountKA.tbxAddress1.text);
}