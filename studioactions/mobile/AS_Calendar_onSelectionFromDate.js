function AS_Calendar_onSelectionFromDate(eventobject, isValidDateSelected) {
    return AS_Calendar_d780c47911ec49808afed14fede81fdf(eventobject, isValidDateSelected);
}

function AS_Calendar_d780c47911ec49808afed14fede81fdf(eventobject, isValidDateSelected) {
    var currDate = frmNewTransferKA.calFrom.day;
    var currMonth = frmNewTransferKA.calFrom.month;
    var currYear = frmNewTransferKA.calFrom.year;
    if (validateSelectedDay()) {
        frmNewTransferKA.calTo.date = ([currDate + 1, currMonth, currYear]);
        frmNewTransferKA.calTo.validStartDate = ([currDate + 1, currMonth, currYear]);
        onTextChangeRecurrence();
        recurrence_DATE_SELECTION();
    }
}