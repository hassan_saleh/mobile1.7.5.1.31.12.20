function AS_Button_OnClickBtnBulkPaymentManagaPayee(eventobject) {
    return AS_Button_i07262e1aa3d42789040e40654c4e108(eventobject);
}

function AS_Button_i07262e1aa3d42789040e40654c4e108(eventobject) {
    frmBulkPaymentKA.flxMainContainer.isVisible = true;
    frmBulkPaymentKA.flxConfirmPayment.isVisible = false;
    frmBulkPaymentKA.lblNext.skin = "sknLblNextDisabled";
    frmBulkPaymentKA.flxUnderlinePaymentModeBulk.skin = "sknFlxGreyLine";
    frmBulkPaymentKA.lblPaymentMode.text = geti18Value("i18n.billsPay.Accounts");
    frmBulkPaymentKA.flxConversionAmt.setVisibility(false);
    serv_getBulkBillNumberDetails();
}