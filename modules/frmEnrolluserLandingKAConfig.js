var frmEnrolluserLandingKAConfig= {
  "formid": "frmEnrolluserLandingKA",
  "frmEnrolluserLandingKA": {
    "entity": "User",
    "objectServiceName": "RBObjects",
    "objectServiceOptions" : {"access":"online"}
  },

  "lblUname":{
    "fieldprops":{
      "entity": "User",
      "widgettype": "Label",
      "field": "uname"
    }
  },
  "lblPwd":{
    "fieldprops":{
      "entity": "User",
      "widgettype": "Label",
      "field": "pwd"
    }
  },

  "lblCPwd":{
    "fieldprops":{
      "entity": "User",
      "widgettype": "Label",
      "field": "CPwd"
    }
  },

  "lblLanguage":{
    "fieldprops":{
      "entity": "User",
      "widgettype":"Label",
      "field":"lang"
    }
  },

  "lblFlag":{
    "fieldprops":{
      "entity": "User",
      "widgettype":"Label",
      "field":"Updateflag"
    }
  },
    "lblIEMI":{
    "fieldprops":{
      "entity": "User",
      "widgettype": "Label",
      "field": "Imei"
    }
  },
    "lblDevId":{
    "fieldprops":{
      "entity": "User",
      "widgettype": "Label",
      "field": "DevId"
    }
  },
    "lblIpAdd":{
    "fieldprops":{
      "entity": "User",
      "widgettype": "Label",
      "field": "Ipaddress"
    }
  },
  
  "UserFlag":{//Used in case of change username. If "C",checks if username exists, else updates thes username.
    "fieldprops":{
      "entity": "User",
      "widgettype":"Label",
      "field":"UserFlag"
    }
  }


  /*   "lstboxaccountype":{
          "fieldprops": {
            "entity":"User",
            "widgettype": "ListBox",
            "field":"accountType",
            "selector": "Account Type",
            "picklistInfo": {
              "entity": "AccountType",
              "key": "TypeID",
              "value": "TypeDescription"
          } 

        }
      },
       "lstboxquestn1":{
          "fieldprops": {
            "entity":"User",
            "widgettype": "ListBox",
            "field":"",
            "selector": "a question here",
            "picklistInfo": {
              "entity": "SecurityQuestions",
              "key": "SecurityID",
              "value": "question"
          } 

        }
      },
       "lstboxquestn2":{
          "fieldprops": {
            "entity":"User",
            "widgettype": "ListBox",
            "field":"",
            "selector": "a question here",
            "picklistInfo": {
              "entity": "SecurityQuestions",
              "key": "SecurityID",
              "value": "question"
          } 

        }
      },
       "lstboxquestn3":{
          "fieldprops": {
            "entity":"User",
            "widgettype": "ListBox",
            "field":"",
            "selector": "a question here",
            "picklistInfo": {
              "entity": "SecurityQuestions",
              "key": "SecurityID",
              "value": "question"
          } 

        }
      },
       "lstboxquestn4":{
          "fieldprops": {
            "entity":"User",
            "widgettype": "ListBox",
            "field":"",
            "selector": "a question here",
            "picklistInfo": {
              "entity": "SecurityQuestions",
              "key": "SecurityID",
              "value": "question"
          } 

        }
      },
       "lstboxquestn5":{
          "fieldprops": {
            "entity":"User",
            "widgettype": "ListBox",
            "field":"",
            "selector": "a question here",
            "picklistInfo": {
              "entity": "SecurityQuestions",
              "key": "SecurityID",
              "value": "question"
          } 

        }
      },
        "tbxaccntnumber":{
         "fieldprops": {
            "entity":"User",
            "widgettype": "TextField",
            "field":"accountNumber"
         }
      },
      "tbxssnnumber":{
          "fieldprops":{
          "entity":"User",
             "widgettype": "TextField",
              "field": "ssn"
          }
        },
      "lblDobKA": {
            "fieldprops": {
            "entity": "User",
            "field": "dateOfBirth",
            "widgettype": "Label"
          }
       },
         "tbxEmailKA":{
            "fieldprops":{
            "entity": "User",
            "widgettype": "TextField",
            "field": "email"
            }
         },
       "tbxPhoneNumberKA":{
         "fieldprops":{
            "entity": "User",
             "widgettype": "TextField",
             "field": "phone"
            }
       },
      "tbxAnswer1":{
          "fieldprops":{
          "entity": "User",
              "widgettype": "TextField",
              "field": ""
            }

    },
     "tbxAnswer2":{
       "fieldprops":{
            "entity": "User",
              "widgettype": "TextField",
              "field": ""
            }

    },
     "tbxAnswer3":{
       "fieldprops":{
            "entity": "User",
              "widgettype": "TextField",
              "field": ""
            }

    },
     "tbxAnswer4":{
       "fieldprops":{
            "entity": "User",
              "widgettype": "TextField",
              "field": ""
            }

    },
     "tbxAnswer5":{
       "fieldprops":{
            "entity": "User",
              "widgettype": "TextField",
              "field": ""
            }

    }*/
};

