function AS_Button_f446556b4f9c4b368b249319efc9edcd(eventobject) {
    function SHOW_ALERT_ide_onClick_i1f929e9ce6041e988cc786e4a923f19_True() {
        frmAccountsLandingKA.flxAccntLandingNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_i1f929e9ce6041e988cc786e4a923f19_False() {}

    function SHOW_ALERT_ide_onClick_i1f929e9ce6041e988cc786e4a923f19_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_i1f929e9ce6041e988cc786e4a923f19_True();
        } else {
            SHOW_ALERT_ide_onClick_i1f929e9ce6041e988cc786e4a923f19_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_i1f929e9ce6041e988cc786e4a923f19_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}