function preshowJoMoPay(eventobject) {
    return AS_Form_de6f9f37b9ef4530bd86d912d87730ff(eventobject);
}

function AS_Form_de6f9f37b9ef4530bd86d912d87730ff(eventobject) {
    inputObj1 = ["Person to Person"];
    inputObj2 = ["Current Account ***2202", "Saving Accounts ***7786", "Salary Account ***2480"];
    inputObj3 = ["Alias", "Mobile Number", "National ID"];
    var len1 = inputObj1.length;
    var len2 = inputObj2.length;
    var len3 = inputObj3.length;
    if (len1 < 2) {
        frmJoMoPay.btnDropDown.setVisibility(false);
    }
    if (len2 < 2) {
        frmJoMoPay.btnDropDownAccount.setVisibility(false);
    }
    if (len3 < 2) {
        frmJoMoPay.btnAliasDropDown.setVisibility(false);
    }
}