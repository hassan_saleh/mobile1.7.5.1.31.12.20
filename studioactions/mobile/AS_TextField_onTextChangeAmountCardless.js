function AS_TextField_onTextChangeAmountCardless(eventobject, changedtext) {
    return AS_TextField_a13aceb3120a4f7e8074fff5bf7ee866(eventobject, changedtext);
}

function AS_TextField_a13aceb3120a4f7e8074fff5bf7ee866(eventobject, changedtext) {
    /*if(checkNextCardless())
      frmCardlessTransaction.btnSubmit.setEnabled(true);
    else
      frmCardlessTransaction.btnSubmit.setEnabled(false);*/
    frmCardlessTransaction.txtFieldAmount.text = frmCardlessTransaction.txtFieldAmount.text.toString().replace(/\./g, "");
    if (parseInt(frmCardlessTransaction.txtFieldAmount.text) > 500) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.cardless.limitAmount"), popupCommonAlertDimiss, "");
    } else if (parseInt(frmCardlessTransaction.txtFieldAmount.text) === 500) {
        frmCardlessTransaction.flxPlusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
        frmCardlessTransaction.flxPlusContainer.setEnabled(false);
        frmCardlessTransaction.flxMinusContainer.skin = "plusMinusSkin";
        frmCardlessTransaction.flxMinusContainer.setEnabled(true);
    } else if (parseInt(frmCardlessTransaction.txtFieldAmount.text) === 0 || frmCardlessTransaction.txtFieldAmount.text === "") {
        frmCardlessTransaction.flxMinusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
        frmCardlessTransaction.flxMinusContainer.setEnabled(false);
        frmCardlessTransaction.flxPlusContainer.skin = "plusMinusSkin";
        frmCardlessTransaction.flxPlusContainer.setEnabled(true);
    } else {
        checkNextCardless();
        frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
        frmCardlessTransaction.flxPlusContainer.skin = "plusMinusSkin";
        frmCardlessTransaction.flxPlusContainer.setEnabled(true);
        frmCardlessTransaction.flxMinusContainer.skin = "plusMinusSkin";
        frmCardlessTransaction.flxMinusContainer.setEnabled(true);
    }
    checkNextCardless();
    check_AMOUNT_PREDEFINED(frmCardlessTransaction.txtFieldAmount.text);
}