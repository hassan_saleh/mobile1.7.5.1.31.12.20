function AS_Label_i239b1551a444d51bb873749a0442bc7(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.common.sureExit"), onClickYesCloseChange, onClickNoCloseChange, geti18Value("i18n.login.continue"), geti18Value("i18n.common.cancel"));

    function onClickYesCloseChange() {
        popupCommonAlertDimiss();
        frmSettingsKA.show();
    }

    function onClickNoCloseChange() {
        popupCommonAlertDimiss();
    }
}