function AS_onClickPayNowPayeeDetail(eventobject) {
    return AS_Button_c086d93cdede428a96b66c6dcd18fba4(eventobject);
}

function AS_Button_c086d93cdede428a96b66c6dcd18fba4(eventobject) {
    kony.boj.selectedBillerType = "PostPaid";
    if (kony.boj.selectedBillerType === "PostPaid") gblQuickFlow = "post";
    else gblQuickFlow = "pre";
    getBillDetailsPostpaidBills();
    frmBills.flxConversionAmt.setVisibility(false);
    frmBills.lblPaymentMode.skin = "sknLblNextDisabled";
    clearfrmBills();
}