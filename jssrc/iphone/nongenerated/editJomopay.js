function setUpdateJomoPayBillerScreen(data) {
    try {
        v_benef_ID = data.benef_id; // edit beneficiary
        kony.print("Jomopay data ::" + JSON.stringify(data));
        glb_jomopayBene = data.benAcctNo; // edit beneficiary
        glb_jomopayBeneName = data.beneficiaryName; // edit beneficiary
        if (data.bene_address1 == "M") {
            frmJOMOPayAddBiller.txtBeneficiaryMobile.text = data.benAcctNo;
            frmJOMOPayAddBiller.lblType.text = geti18Value("i18n.jomopay.mobiletype");
            frmJOMOPayAddBiller.flxBeneficiaryAlias.isVisible = false; /* hassan */
            frmJOMOPayAddBiller.lblMobileIDHint.setVisibility(true);
        } else {
            frmJOMOPayAddBiller.txtBeneficiaryAlias.text = data.benAcctNo;
            frmJOMOPayAddBiller.lblType.text = geti18Value("i18n.jomopay.aliastype");
            frmJOMOPayAddBiller.flxBeneficiaryMobile.isVisible = false; /* hassan */
            frmJOMOPayAddBiller.lblMobileIDHint.setVisibility(false);
            frmJOMOPayAddBiller.flxBeneficiaryAlias.setVisibility(true);
        }
        frmJOMOPayAddBiller.btnAddBilller.setVisibility(false);
        frmJOMOPayAddBiller.btnEditBeneficiary.setVisibility(true);
        frmJOMOPayAddBiller.lblJoMoPay.text = geti18Value("i18n.jomopay.editJomoBene");
        frmJOMOPayAddBiller.txtName.text = data.beneficiaryName;
        frmJOMOPayAddBiller.txtNickName.text = data.nickName;
        if (data.isFav === "N") frmJOMOPayAddBiller.btnFavCheckBox.text = "q";
        else frmJOMOPayAddBiller.btnFavCheckBox.text = "p";
        frmJOMOPayAddBiller.flxBorderName.skin = "skntextFieldDividerJomoPay";
        frmJOMOPayAddBiller.flxBorderNickName.skin = "skntextFieldDividerJomoPay";
        frmJOMOPayAddBiller.flxBorderBenificiary.skin = "skntextFieldDividerJomoPay";
        frmJOMOPayAddBiller.flxBorderAlias.skin = "skntextFieldDividerJomoPay"; // hassan
        frmJOMOPayAddBiller.lblNameTitle.top = "40%";
        frmJOMOPayAddBiller.lblNameTitle.width = "50%";
        frmJOMOPayAddBiller.lblNameTitle.skin = "lblAccountStaticText";
        frmJOMOPayAddBiller.lblNickNameTitle.top = "40%";
        frmJOMOPayAddBiller.lblNickNameTitle.width = "50%";
        frmJOMOPayAddBiller.lblNickNameTitle.skin = "lblAccountStaticText";
        frmJOMOPayAddBiller.lblBeneficiaryMobileTitle.top = "40%";
        frmJOMOPayAddBiller.lblBeneficiaryMobileTitle.width = "50%";
        frmJOMOPayAddBiller.lblBeneficiaryMobileTitle.skin = "lblAccountStaticText";
        frmJOMOPayAddBiller.lblBeneficiaryAlias.top = "40%"; /* hassan */
        frmJOMOPayAddBiller.lblBeneficiaryAlias.width = "50%"; /* hassan */
        frmJOMOPayAddBiller.lblBeneficiaryAlias.skin = "lblAccountStaticText"; /* hassan */
    } catch (e) {
        kony.print("Exception_fillUpdateJomoPayBillerScreen ::" + e);
    }
}

function navigate_JOMOPAYUPDATEBILLER(data) {
    try {
        jomopayFlag = "editBene";
        setUpdateJomoPayBillerScreen(data);
        frmJOMOPayAddBiller.show();
    } catch (e) {
        kony.print("Exception_navigate_JOMOPAYUPDATEBILLER ::" + e);
    }
}

function goToConfirmScreen() {
    var jomopayBenType = "";
    var jomopayBene = "";
    var btnFav = "";
    //if (frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== "" && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== null && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== undefined){
    if (frmJOMOPayAddBiller.lblType.text === geti18Value("i18n.jomopay.mobiletype")) {
        jomopayBene = frmJOMOPayAddBiller.txtBeneficiaryMobile.text;
        jomopayBenType = "M";
    } else {
        jomopayBene = frmJOMOPayAddBiller.txtBeneficiaryAlias.text;
        jomopayBenType = "A";
    }
    frmJOMOPayAddBiller.lblNameConfirm.text = frmJOMOPayAddBiller.txtName.text;
    frmJOMOPayAddBiller.lblNickNameConfirm.text = frmJOMOPayAddBiller.txtNickName.text;
    if (jomopayBenType === "A") frmJOMOPayAddBiller.lblJoMoPayTypeConfirm.text = geti18Value("i18n.jomopay.aliastype");
    else frmJOMOPayAddBiller.lblJoMoPayTypeConfirm.text = geti18Value("i18n.jomopay.mobiletype");
    frmJOMOPayAddBiller.lblAccountConfirm.text = jomopayBene;
    frmJOMOPayAddBiller.flxConfirmJomopay.isVisible = "true";
    frmJOMOPayAddBiller.flxFirstJomopay.isVisible = "false";
    frmJOMOPayAddBiller.btnFavConfirm.text = frmJOMOPayAddBiller.btnFavCheckBox.text;
}

function serv_UPDATE_JOMOPAY_BENEFICIARY_DETAILS() {
    try {
        var jomopayBenType = "";
        var jomopayBene = "";
        var btnFav = "";
        //if (frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== "" && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== null && frmJOMOPayAddBiller.txtBeneficiaryMobile.text !== undefined){
        if (frmJOMOPayAddBiller.lblType.text === geti18Value("i18n.jomopay.mobiletype")) {
            jomopayBene = frmJOMOPayAddBiller.txtBeneficiaryMobile.text;
            jomopayBenType = "M";
        } else {
            jomopayBene = frmJOMOPayAddBiller.txtBeneficiaryAlias.text;
            jomopayBenType = "A";
        }
        if (frmJOMOPayAddBiller.btnFavConfirm.text === "q") btnFav = "N";
        else btnFav = "Y";
        var requestObj = {
            "custId": custid,
            "beneficiaryName": frmJOMOPayAddBiller.lblNameConfirm.text,
            "nickName": frmJOMOPayAddBiller.lblNickNameConfirm.text,
            "bene_address1": jomopayBenType,
            "bene_city": "",
            "benAcctNo": frmJOMOPayAddBiller.lblAccountConfirm.text,
            "BENE_BANK_SWIFT": "",
            "BENE_BANK_COUNTRY": "",
            "bankName": "",
            "benBranchNo": "",
            "bene_bank_city": "",
            "bene_address2": "",
            "P_BENE_TYPE": "JMP",
            "bene_email": "",
            "language": "en",
            "benAcctCurrency": "",
            "Fav_flag": btnFav,
            "P_BENEF_ID": v_benef_ID,
            "P_BENEF_NAME": glb_jomopayBeneName,
            "P_BENE_ACCOUNT": glb_jomopayBene
        };
        kony.print("Input Parameters ::" + JSON.stringify(requestObj));
        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("updateBeneficiary");
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        if (kony.sdk.isNetworkAvailable()) {
            appMFConfiguration.invokeOperation("updateBeneficiaryTrans", {}, requestObj, success_serv_UPDATE_JOMOPAY_BENEFICIARY_DETAILS, function(err) {
                kony.print("Error ::" + JSON.stringify(err));
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                gblFromModule = "";
                kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"));
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            });
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception serv_UPDATE_JOMOPAY_BENEFICIARY_DETAILS ::" + e);
    }
}

function success_serv_UPDATE_JOMOPAY_BENEFICIARY_DETAILS(res) {
    try {
        var msg = "";
        kony.print("success response ::" + JSON.stringify(res));
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        if (!isEmpty(res)) {
            gblFromModule = "";
            if (res.ErrorCode === "00000" && res.result === "00000") {
                var sucs_msg = geti18Value("i18n.updatebeneficiary.successmsg");
                sucs_msg = sucs_msg.replace("name", frmJOMOPayAddBiller.txtName.text);
                //   gblSelectedBene = kony.boj.getNewAddedBeneDetails(res.ReferenceNumber);
                loadBillerDetails = true;
                kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), sucs_msg, geti18Value("i18n.Bene.Sendmoneyto"), "sendMoneyJomo", geti18Value("i18n.common.gotojomopay"), "JoMo");
            } else if (res.opstatus_prAddBeneficiary != "0") {
                //trying to add one more time	
                serv_ADD_JOMOPAYBILLER();
            }
            if (res.result !== "00000") {
                msg = geti18Value("i18n.updatebene.failedmsg");
            } else if (res.ErrorCode !== "00000") {
                msg = "Please add the biller again, for some reason biller is deleted.";
            }
            if (msg !== "") {
                kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.common.gotojomopay"), "JoMo");
            }
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_success_serv_UPDATE_JOMOPAY_BENEFICIARY_DETAILS ::" + e);
    }
}