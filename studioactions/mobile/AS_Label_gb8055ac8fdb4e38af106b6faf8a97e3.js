function AS_Label_gb8055ac8fdb4e38af106b6faf8a97e3(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if ((currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") && currentForm.passwordTextField.text !== "") {
        if (frmLoginKA.lblShowPass.text === "B") {
            currentForm.passwordTextField.secureTextEntry = true;
            frmLoginKA.lblShowPass.text = "A";
        } else {
            currentForm.passwordTextField.secureTextEntry = false;
            frmLoginKA.lblShowPass.text = "B";
        }
    }
}