function onRowClickSISegment(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_efe741d62b2d40ba9bab8c4a6bbaf03a(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_efe741d62b2d40ba9bab8c4a6bbaf03a(eventobject, sectionNumber, rowNumber) {
    // alert(JSON.stringify());
    // frmConfirmStandingInstructions.show();
    var x = frmStandingInstructions.segSIList.selectedItems[0].instruction_number;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmConfirmStandingInstructions");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("segSIDetails", {
        "headers": {},
        "queryParams": {
            "InstructionNum": x
        }
    });
    controller.loadDataAndShowForm(navObject);
}