function onRowClickCardsListOption(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_c243f75cc0c349de9071a6cc8bcce56e(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_c243f75cc0c349de9071a6cc8bcce56e(eventobject, sectionNumber, rowNumber) {
    kony.print("Segment Data ::" + JSON.stringify(frmCardStatementKA.segCardsOptions.selectedRowItems));
    kony.print("Segment Data Length ::" + frmCardStatementKA.segCardsOptions.selectedRowItems[0].lblValue.length);
    if (frmCardStatementKA.segCardsOptions.selectedRowItems[0].lblTitle === "Year") {
        frmCardStatementKA.lblCardYear.text = frmCardStatementKA.segCardsOptions.selectedRowItems[0].lblValue;
    } else if (gblDownloadPDFFlow && frmCardStatementKA.segCardsOptions.selectedRowItems[0].lblTitle === "Type") {
        frmCardStatementKA.lblDownloadType.text = frmCardStatementKA.segCardsOptions.selectedRowItems[0].lblValue;
    } else {
        frmCardStatementKA.lblCardDate.text = frmCardStatementKA.segCardsOptions.selectedRowItems[0].lblValue;
    }
    animate_cardsFilterOption();
}