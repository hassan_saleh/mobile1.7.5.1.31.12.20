function onClickBackCardLinkedAccountsConfirm(eventobject) {
    return AS_FlexContainer_c06d5d941d5d41d586d6310445a80163(eventobject);
}

function AS_FlexContainer_c06d5d941d5d41d586d6310445a80163(eventobject) {
    if (frmCardLinkedAccountsConfirm.btnConfirm.text === geti18Value("i18n.transfers.CONFIRM")) {
        frmCardLinkedAccounts.show();
        frmCardLinkedAccountsConfirm.destroy();
    }
}