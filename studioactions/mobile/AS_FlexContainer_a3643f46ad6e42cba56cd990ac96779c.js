function AS_FlexContainer_a3643f46ad6e42cba56cd990ac96779c(eventobject) {
    if (frmEPS.lblNext.skin === "sknLblNextEnabled") {
        if (validate_SufficientBalance()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            SetupConfirmationScreen();
            //   GetAliasInfo();
            frmEPS.flxMain.setVisibility(false);
            frmEPS.flxConfirmEPS.setVisibility(true);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
}