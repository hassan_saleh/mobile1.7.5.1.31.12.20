function AS_onBackPayeeDetails(eventobject) {
    return AS_FlexContainer_hd92cc663fcd4501be12b764ee78b818(eventobject);
}

function AS_FlexContainer_hd92cc663fcd4501be12b764ee78b818(eventobject) {
    // if(loadBillerDetails){
    if (kony.sdk.isNetworkAvailable()) {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmManagePayeeKA");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("managepayeesegment", {
            "headers": {},
            "queryParams": {
                "custId": custid,
                "P_BENE_TYPE": "PRE"
            }
        });
        controller.loadDataAndShowForm(navObject);
    } else {
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
    // }else{
    //   frmManagePayeeKA.show();
    // }
}