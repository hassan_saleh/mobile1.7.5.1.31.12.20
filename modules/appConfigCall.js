function appConfigManualFetch() {
  if (kony.sdk.isNetworkAvailable()) {
    ShowLoadingScreen();
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
    var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");

    dataObject.addField("Language", "EN");

    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };

    objectService.customVerb("getAppSettingsBOJ", serviceOptions, appConfigManualFetchSuccess, appConfigManualFetchFailure);

  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function appConfigManualFetchSuccess(response) {
  getAppConfig = false;
  try {
    if(!isEmpty(response) && !isEmpty(response.AppConfig)){
      kony.print("response appConfigManualFetchSuccess: " + JSON.stringify(response));
      kony.boj.retailBanking.globals.APP_USAGE_TIMEOUT = parseInt(response.AppConfig[0].AppusageTimeout) * 60;
      kony.boj.retailBanking.globals.IDLE_TIMEOUT = parseInt(response.AppConfig[0].IdleTimeout);
      if(!isEmpty(response.AppConfig[0].EN_OTP_MSG)){
        engOTPmsg = response.AppConfig[0].EN_OTP_MSG ;
      }else{
        engOTPmsg = "Dear %s, please enter the one time password (OTP ): %s; to complete  BOJ Mobile transaction" ;
      }
      if(!isEmpty(response.AppConfig[0].AR_OTP_MSG)){
        araOTPmsg = response.AppConfig[0].AR_OTP_MSG ;
      }else{
        araOTPmsg = "عميلنا الكريم %s\nيرجى ادخال رمز الدخول المؤقت%s(OTP) لاستكمال حركتكم على الخدمة المصرفية عبر الهاتف النقال BOJ Mobile";
      }
      engSUNMmsg = response.AppConfig[0].En_Forgot_Usrnm_Msg ;
      araSUNMmsg = response.AppConfig[0].Ar_Forgot_Usrnm_Msg ;

      gblEN_CNGSMS_MSG_BFR = response.AppConfig[0].EN_CNGSMS_MSG_BFR ;
      gblAR_CNGSMS_MSG_BFR = response.AppConfig[0].AR_CNGSMS_MSG_BFR ;
      gblEN_CNGSMS_MSG_AFTR = response.AppConfig[0].EN_CNGSMS_MSG_AFTR ;
      gblAR_CNGSMS_MSG_AFTR = response.AppConfig[0].AR_CNGSMS_MSG_AFTR ;
      RELNOTES_Heading_EN = response.AppConfig[0].RELNOTES_HEADING_EN ;
      RELNOTES_Heading_AR = response.AppConfig[0].RELNOTES_HEADING_AR ;

      if (!isEmpty(response.AppConfig[0].SCANCARDLICENSEIOS))
        SCANCARDLICENSEIOS = response.AppConfig[0].SCANCARDLICENSEIOS;
     if (!isEmpty(response.AppConfig[0].SCANCARDLICENSEANDROID))
         SCANCARDLICENSEANDROID = response.AppConfig[0].SCANCARDLICENSEANDROID;

      //upgrade cards limit
      MIN_CARDLIMIT_CLASSIC = response.AppConfig[0].MIN_CARDLIMIT_CLASSIC;
      MAX_CARDLIMIT_CLASSIC = response.AppConfig[0].MAX_CARDLIMIT_CLASSIC;
      MIN_CARDLIMIT_GOLD = response.AppConfig[0].MIN_CARDLIMIT_GOLD;
      MAX_CARDLIMIT_GOLD = response.AppConfig[0].MAX_CARDLIMIT_GOLD;
      MIN_CARDLIMIT_PLATINUM = response.AppConfig[0].MIN_CARDLIMIT_PLATINUM;
      MAX_CARDLIMIT_PLATINUM = response.AppConfig[0].MAX_CARDLIMIT_PLATINUM;
      MIN_CARDLIMIT_WORLD = response.AppConfig[0].MIN_CARDLIMIT_WORLD;
      MAX_CARDLIMIT_WORLD = response.AppConfig[0].MAX_CARDLIMIT_WORLD;
      ISSIRIENABLEDAppLevel = response.AppConfig[0].ISSIRIENABLED;
      //
      kony.print(" engOTPmsg:: "+engOTPmsg  + " :: araOTPmsg ::  "+ araOTPmsg);
      //Username validation
      kony.boj.util.usernameValidation.minUsernameLength=parseInt(response.Username[0].MINLENGTH);
      kony.boj.util.usernameValidation.maxUsernameLength=parseInt(response.Username[0].MAXLENGTH);
      var validarray = [];      
      for(i=0;i<response.Username[0].VALIDCHAR.length;i++){
        validarray.push(response.Username[0].VALIDCHAR.charAt(i));
      }
      kony.boj.util.usernameValidation.validCharacters=validarray;
      kony.boj.util.usernameValidation.minSpecial = parseInt(response.Username[0].MINSPECIAL);
      kony.boj.util.usernameValidation.minLowerCase = parseInt(response.Username[0].MINLCASE);

      //password Validation
      kony.boj.util.passwordValidation.minPasswordLength=parseInt(response.Password[0].MINLENGTH);
      kony.boj.util.passwordValidation.maxPasswordLength=parseInt(response.Password[0].MAXLENGTH);

      kony.boj.util.passwordValidation.minLowerCase=parseInt(response.Password[0].MINLCASE);
      kony.boj.util.passwordValidation.minUpperCase=parseInt(response.Password[0].MAXUCASE);

      kony.boj.util.passwordValidation.minDigit=parseInt(response.Password[0].MINNUM);
      kony.boj.util.passwordValidation.minSpecial=parseInt(response.Password[0].MINSPECIAL);
      kony.boj.retailBanking.globals.Maintenance_Flag = response.AppConfig[0].MaintenanceFlag; 

      validarray = [];      
      for(i=0;i<response.Password[0].VALIDCHAR.length;i++){
        validarray.push(response.Password[0].VALIDCHAR.charAt(i));
      }
      kony.boj.util.passwordValidation.validCharacters=validarray;

      serverAppVersion = parseFloat(response.AppConfig[0].AppVersion);

      if (!isEmpty(response.AppConfig[0].CURRENCY))
        CURRENCY = response.AppConfig[0].CURRENCY;
      else
        CURRENCY = "";

      if (!isEmpty(response.AppConfig[0].LINKEDIN))
        LINKEDIN = response.AppConfig[0].LINKEDIN;
      else
        LINKEDIN = "";

      if (!isEmpty(response.AppConfig[0].TWITTER))
        TWITTER = response.AppConfig[0].TWITTER;
      else
        TWITTER = "";

      if (!isEmpty(response.AppConfig[0].FACEBOOK))
        FACEBOOK = response.AppConfig[0].FACEBOOK;
      else
        FACEBOOK = "";

      if (!isEmpty(response.AppConfig[0].INSTAGRAM))
        INSTAGRAM = response.AppConfig[0].INSTAGRAM;
      else
        INSTAGRAM = "";


      if (!isEmpty(response.AppConfig[0].YOUTUBE))
        YOUTUBE = response.AppConfig[0].YOUTUBE;
      else
        YOUTUBE = "";
      
      if (!isEmpty(response.AppConfig[0].BITLYPARAMS))
        bitly_parameters = response.AppConfig[0].BITLYPARAMS;
      if (!isEmpty(response.AppConfig[0].BITLYURL))
        bitly_requestURL = response.AppConfig[0].BITLYURL;
      if (!isEmpty(response.AppConfig[0].SHARELINKURL))
        SHARELINKURL = response.AppConfig[0].SHARELINKURL;
      
      
      if (!isEmpty(response.AppConfig[0].Siri_Limit))
        Siri_Limit = response.AppConfig[0].Siri_Limit;
      
      
        

      if (!isEmpty(response.AppConfig[0].ContactNum))
        bojcontactnumber = response.AppConfig[0].ContactNum;
      if (!isEmpty(response.AppConfig[0].Email))
        bojemailID = response.AppConfig[0].Email;
      if (!isEmpty(response.AppConfig[0].WEBSITE))
        bojWEBSITE = response.AppConfig[0].WEBSITE;
      else
        bojWEBSITE = "www.bankofjordan.com";
      kony.print(" :: bojJMP_Limit :: "+ response.AppConfig[0].JMP_LIMIT);
      if (!isEmpty(response.AppConfig[0].JMP_LIMIT))
        bojJMP_Limit = parseFloat(response.AppConfig[0].JMP_LIMIT);
      else
        bojJMP_Limit = 0.0;

      //#ifdef android
      	if(!isEmpty(response.AppConfig[0].ENABLEBOJPAYAPP))
          ENABLEBOJPAYAPP = response.AppConfig[0].ENABLEBOJPAYAPP;
      //#endif
      
      kony.print(" :: bojJMP_Limit :: "+ bojJMP_Limit +" :: bojcontactnumber :: "+ bojcontactnumber + ":: bojemailID :: " + bojemailID + " :: bojWEBSITE :: "+ bojWEBSITE );
      if(gblPayAppFlow && !isEmpty(gblLaunchModeOBJ.appFlow)){
        frmEnrolluserLandingKA.lblContactCenterNumber.text = bojcontactnumber;
        frmEnrolluserLandingKA.forceLayout();
      }
      var appVersion1 = appConfig.appVersion.split(".");
	  var version = "";
      for(var i in appVersion1){
      	if(i == 0)
        	version = appVersion1[i]+".";
        else
        	version = version+""+appVersion1[i];
      }
      var  currentAppVersion =parseFloat(version);
      kony.print("currentAppVersion:: "+ currentAppVersion + " ::serverAppVersion:: "+ serverAppVersion);

      var relNotes = "";
      var langSelected = kony.store.getItem("langPrefObj");

      if(response.AppConfig[0].RELNOTES_EN !== undefined && response.AppConfig[0].RELNOTES_EN !== null && response.AppConfig[0].RELNOTES_EN !== ""){
        en_ReleaseNotes = response.AppConfig[0].RELNOTES_EN;
      }
      if(response.AppConfig[0].RELNOTES_AR !== undefined && response.AppConfig[0].RELNOTES_AR !== null && response.AppConfig[0].RELNOTES_AR !== ""){       
        ar_ReleaseNotes = response.AppConfig[0].RELNOTES_AR;
      }

      var CurrForm  = kony.application.getCurrentForm();
      if(isEmpty(CurrForm)){
     	if(!isLoggedIn){
          if (kony.store.getItem("langFlagSettingObj")) {
            CurrForm =  frmLoginKA;
          } else {
            CurrForm =  frmLanguageChange;
          }
        }
      }
      kony.application.dismissLoadingScreen();
      if(CurrForm.id === "frmLoginKA"){
        kony.print("currentAppVersion:: "+ currentAppVersion + " ::serverAppVersion:: "+ serverAppVersion);
        var stored_APP_VERSION = null;
        if(kony.store.getItem("isAppUpdated") !== undefined && kony.store.getItem("isAppUpdated") !== null){
            stored_APP_VERSION = kony.store.getItem("isAppUpdated").split(".");
        }else{
            stored_APP_VERSION = appConfig.appVersion;
        }
        if(!isEmpty(stored_APP_VERSION)){
            for(var i in stored_APP_VERSION){
            if(i == 0)
              version = stored_APP_VERSION[i]+".";
            else
              version = version+""+stored_APP_VERSION[i];
          }
            if(parseFloat(version) < serverAppVersion)
        		kony.store.setItem("isDeviceRegisteredMandatory", true);
        }
        if(currentAppVersion < serverAppVersion ){
          kony.store.setItem("isDeviceRegisteredMandatory", true);
          customAlertPopup(geti18nkey("i18n.appUpgrade.upgradeOfAnApp"), kony.i18n.getLocalizedString("i18n.appUgrademsg.text"), navtoPlayStore, exitAppOk, kony.i18n.getLocalizedString("i18n.common.Text"), kony.i18n.getLocalizedString("i18n.common.cancelC"));
          //kony.application.dismissLoadingScreen();
          return; 
        }

      }
      if(checkme){
        kony.boj.maintainenceCHECK();
        checkme = false;
      }else{
        kony.application.dismissLoadingScreen();
      }
    }else{
      kony.boj.retailBanking.globals.APP_USAGE_TIMEOUT = 1200;
      kony.boj.retailBanking.globals.IDLE_TIMEOUT = 300;
    }
  }catch (e) {
    kony.print("Exception in appconfigmanualFetchSuccess : "+ e);
    kony.boj.retailBanking.globals.APP_USAGE_TIMEOUT = 1200;
    kony.boj.retailBanking.globals.IDLE_TIMEOUT = 300;
    exceptionLogCall("appConfigManualFetchSuccess","UI ERROR","UI",e);
    kony.application.dismissLoadingScreen();
  }

}

function exitAppOk() {
  kony.application.exit();
}


function navtoPlayStore(){
  try{
    if (kony.retailBanking.globalData.deviceInfo.isIphone()) {
      kony.application.openURL("https://itunes.apple.com/jo/app/boj-mobile/id1403342029?mt=8");
    }else{
      kony.application.openURL("https://play.google.com/store/apps/details?id=com.bankofjordan.mobileapp");
    }                  
  }catch(e){
    kony.print("exception in navtoPlayStore"+ e);
  }

}

function appConfigManualFetchFailure(response) {
  kony.print("Exception in appconfigmanualFetchSuccess : "+ JSON.stringify(response));
  kony.application.dismissLoadingScreen();
}
