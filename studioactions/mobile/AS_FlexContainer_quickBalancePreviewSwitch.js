function AS_FlexContainer_quickBalancePreviewSwitch(eventobject) {
    return AS_FlexContainer_a1f6c6f666db4f85a7469341d8131fab(eventobject);
}

function AS_FlexContainer_a1f6c6f666db4f85a7469341d8131fab(eventobject) {
    if (frmAuthorizationAlternatives.flxSwitchOff.isVisible === true) {
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = false;
        //frmAuthorizationAlternatives.flxTransactionPreview.isVisible = true;
    } else {
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = false;
        var segLength = 0;
        if (frmAuthorizationAlternatives.segAccounts.isVisible) var segLength = frmAuthorizationAlternatives.segAccounts.data.length;
        var segArray = [];
        kony.print("da---:" + JSON.stringify(frmAuthorizationAlternatives.segAccounts.data));
        if (!isEmpty(segLength)) {
            for (var i = 0; i < segLength; i++) {
                var segData = frmAuthorizationAlternatives.segAccounts.data[i];
                segData.lblIncommingTick.isVisible = false;
                segArray.push(segData);
            }
        }
        frmAuthorizationAlternatives.segAccounts.setData(segArray);
        frmAuthorizationAlternatives.flxSwitchTransactionOff.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchTransactionsOn.isVisible = false;
    }
}