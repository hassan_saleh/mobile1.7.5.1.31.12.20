var cardlessData = [];
var selectedCardlessR = [];
var glbAmountFlag = "";

function checkNextCardless() {
    if ((isEmpty(frmCardlessTransaction.txtFieldAmount.text) || parseFloat(frmCardlessTransaction.txtFieldAmount.text) === 0.000) || isEmpty(frmCardlessTransaction.tbxPassword.text) || frmCardlessTransaction.lblPaymentMode.text === geti18Value("i18n.billsPay.Accounts") || frmCardlessTransaction.lblTermsandConditionsCheckBox.text === "q" || parseFloat(frmCardlessTransaction.txtFieldAmount.text) > 500) {
        frmCardlessTransaction.btnNextCardless.skin = "jomopaynextDisabled";
        frmCardlessTransaction.btnNextCardless.focusSkin = "jomopaynextDisabled";
        frmCardlessTransaction.btnNextCardless.setEnabled(false);
        return false;
    } else if (frmCardlessTransaction.tbxPassword.text.length < 6) {
        frmCardlessTransaction.btnNextCardless.skin = "jomopaynextDisabled";
        frmCardlessTransaction.btnNextCardless.focusSkin = "jomopaynextDisabled";
        frmCardlessTransaction.btnNextCardless.setEnabled(false);
        return false;
    } else if (frmCardlessTransaction.txtFieldAmount.text.trim() === "") {
        frmCardlessTransaction.btnNextCardless.skin = "jomopaynextDisabled";
        frmCardlessTransaction.btnNextCardless.focusSkin = "jomopaynextDisabled";
        frmCardlessTransaction.btnNextCardless.setEnabled(false);
        return false;
    } else {
        frmCardlessTransaction.btnNextCardless.skin = "jomopaynextEnabled";
        frmCardlessTransaction.btnNextCardless.focusSkin = "jomopaynextEnabled";
        frmCardlessTransaction.btnNextCardless.setEnabled(true);
        return true;
    }
}

function onendeditingCaredless() {
    frmCardlessTransaction.flxBorderAmount.skin = "skntextFieldDividerGreen";
    if (frmCardlessTransaction.txtFieldAmount.text !== "" && frmCardlessTransaction.txtFieldAmount.text !== "0" && frmCardlessTransaction.txtFieldAmount.text !== 0) {
        var amount = Number.parseInt(frmCardlessTransaction.txtFieldAmount.text);
        frmCardlessTransaction.txtFieldAmount.text = amount + "";
    }
}
kony.boj.onClickYesBackAddCardless = function() {
    popupCommonAlertDimiss();
    frmManageCardLess.show();
    frmCardlessTransaction.destroy();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
};

function onSelection_TNC() {
    try {
        var tncMsg = "";
        if (frmCardlessTransaction.lblTermsandConditionsCheckBox.text === "p") {
            frmCardlessTransaction.lblTermsandConditionsCheckBox.text = "q";
        } else {
            frmCardlessTransaction.lblTermsandConditionsCheckBox.text = "p";
        }
    } catch (e) {
        kony.print("Exception_onSelection_TNC_CARDLESS::" + e);
    }
}

function toConfirmScreenCardless() {
    // frmCardlessTransaction.lblAccountNumber.text = frmCardlessTransaction.lblPaymentMode.text;
    frmCardlessTransaction.lblConfirmAmount.text = frmCardlessTransaction.txtFieldAmount.text;
    frmCardlessTransaction.lblConfirmPass.text = frmCardlessTransaction.tbxPassword.text;
    frmCardlessTransaction.flxConfirm.isVisible = true;
    frmCardlessTransaction.flxBody.isVisible = false;
    frmCardlessTransaction.btnNextCardless.isVisible = false;
}
/* cardless Transactions */
function serv_cardlessTransactions() {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Accounts");
    dataObject.addField("custId", custid);
    dataObject.addField("p_channel", "MOBILE");
    dataObject.addField("p_user_id", "BOJMOB");
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    kony.print("service input paramerters ::" + JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("cardlessList", serviceOptions, function(res) {
            kony.print("Success ::" + JSON.stringify(res));
            cardlessTransactions(res);
        }, function(err) {
            kony.print("failed ::" + JSON.stringify(err));
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }
}

function cardlessTransactions(res) {
    kony.print("Success11 ::" + JSON.stringify(res));
    frmManageCardLess.segCardlessTransactions.setData([]);
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    frmManageCardLess.segCardlessTransactions.widgetDataMap = {
        lbltmpTitle: "lbltmpTitle",
        transactionDate: "requestDate",
        amount: "Amount",
        accountNumber: "accountNumber",
        btnDelete: "btnSetting",
        lblStatus: "lblStatus"
    };
    cardlessData = []; // hassan 04/08/2019
    for (var i in res.cardlessList) {
        // res[i].initial = res[i].code_desc.substring(0, 2).toUpperCase();
        /*  res.cardlessList[i].initial = res.cardlessList[i].p_nickname.substring(0,2).toUpperCase();
          res.cardlessList[i].icon = {
            backgroundColor: kony.boj.getBackGroundColour(res.cardlessList[i].initial)
          };*/
        res.cardlessList[i].AmountDecimal = setDecimal(res.cardlessList[i].Amount, 3);
        res.cardlessList[i].Amount = setDecimal(res.cardlessList[i].Amount, 3);
        res.cardlessList[i].Amount = res.cardlessList[i].Amount + " JOD";
        res.cardlessList[i].btnSetting = {
            "isVisible": false
        };
        if (res.cardlessList[i].status === "I") {
            res.cardlessList[i].lblStatus = {
                "text": geti18Value("i18n.Cardless.pending"),
                "skin": "lblsknNumberOrange"
            };
            res.cardlessList[i].category = "Pending";
            res.cardlessList[i].btnSetting = {
                "isVisible": true
            };
        } else if (res.cardlessList[i].status === "D") {
            res.cardlessList[i].lblStatus = {
                "text": geti18Value("i18n.Cardless.accepted"),
                "skin": "lblsknNumberGreen"
            };
            res.cardlessList[i].category = "Accept";
        } else {
            res.cardlessList[i].lblStatus = {
                "text": geti18Value("i18n.Cardless.Reject"),
                "skin": "lblsknNumberRed"
            };
            res.cardlessList[i].category = "Reject";
        }
        cardlessData.push(res.cardlessList[i]);
    }
    cardlessData = sort_BYDATE(cardlessData, "requestDate");
    kony.print("hassan cardlessData ::" + JSON.stringify(cardlessData));
    frmManageCardLess.segCardlessTransactions.setData(cardlessData);
    frmManageCardLess.btnAll.skin = "slButtonWhiteTab";
    frmManageCardLess.btnPending.skin = "slButtonWhiteTabDisabled";
    frmManageCardLess.btnAccepted.skin = "slButtonWhiteTabDisabled";
    frmManageCardLess.btnCancel.skin = "slButtonWhiteTabDisabled";
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmManageCardLess.show();
    frmCardlessTransaction.destroy();
}

function show_Cardless_List(flow) {
    try {
        var temp = [];
        kony.print("show_Cardless_List cardlessData ::" + JSON.stringify(cardlessData));
        kony.print("hassan flow ::" + flow);
        if (flow === "all") {
            //temp = kony.boj.AllPayeeList;
            for (var j in cardlessData) {
                cardlessData[j].Amount = setDecimal(cardlessData[j].Amount, 3)
                cardlessData[j].Amount = cardlessData[j].Amount + " JOD";
                if (cardlessData[j].category === "Pending") {
                    cardlessData[j].lblStatus = {
                        "text": geti18Value("i18n.Cardless.pending"),
                        "skin": "lblsknNumberOrange"
                    };
                    cardlessData[j].btnSetting = {
                        "isVisible": true
                    };
                } else {
                    cardlessData[j].btnSetting = {
                        "isVisible": false
                    };
                }
                if (cardlessData[j].category === "Accept") {
                    cardlessData[j].lblStatus = {
                        "text": geti18Value("i18n.Cardless.accepted"),
                        "skin": "lblsknNumberGreen"
                    };
                } else if (cardlessData[j].category === "Reject") {
                    cardlessData[j].lblStatus = {
                        "text": geti18Value("i18n.Cardless.Reject"),
                        "skin": "lblsknNumberRed"
                    };
                }
                temp.push(cardlessData[j]);
            }
        } else {
            for (var i in cardlessData) {
                if (cardlessData[i].category === flow) {
                    //	cardlessData[i].initial = cardlessData[i].p_nickname.substring(0,2).toUpperCase();
                    //	cardlessData[i].lblTick = "";
                    if (cardlessData[i].category === "Pending") {
                        cardlessData[i].btnSetting = {
                            "isVisible": true
                        };
                    } else {
                        cardlessData[i].btnSetting = {
                            "isVisible": false
                        };
                    }
                    temp.push(cardlessData[i]);
                }
            }
        }
        kony.print("hassan flow1 ::" + flow);
        frmManageCardLess.segCardlessTransactions.removeAll();
        frmManageCardLess.segCardlessTransactions.widgetDataMap = {
            lbltmpTitle: "lbltmpTitle",
            transactionDate: "requestDate",
            amount: "Amount",
            accountNumber: "accountNumber",
            btnDelete: "btnSetting",
            lblStatus: "lblStatus"
        };
        frmManageCardLess.segCardlessTransactions.setData(temp);
        kony.print("hassan flow3 ::" + flow);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } catch (e) {
        kony.print("Exception_show_Cardless_List ::" + e);
    }
}
/* cardless Transactions */
/*CardLess Authorization */
function serv_cardlessAuth() {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Accounts");
    dataObject.addField("custId", custid);
    dataObject.addField("branchNumber", frmCardlessTransaction.lblBranchCode.text);
    dataObject.addField("accNumber", frmCardlessTransaction.lblAccountNumber.text);
    dataObject.addField("Amount", frmCardlessTransaction.lblConfirmAmount.text);
    dataObject.addField("password", frmCardlessTransaction.lblConfirmPass.text);
    dataObject.addField("lang", kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara");
    dataObject.addField("p_user_id", "BOJMOB");
    dataObject.addField("p_channel", "MOBILE");
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    kony.print("service input paramerters ::" + JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("cardlessAuth", serviceOptions, function(res) {
            kony.print("Success ::" + JSON.stringify(res));
            cardlessAuthSuccess(res);
        }, function(err) {
            kony.print("failed ::" + JSON.stringify(err));
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }
}

function cardlessAuthSuccess(response) {
    kony.print("Success cardlessAuthSuccess-->" + JSON.stringify(response));
    if (response.ErrorCode === "00000") {
        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.Carless.successMsg"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.Cardless.goToCardless"), geti18nkey("i18n.Cardless.ManageCardless"), "", "", "");
    } else
    if (response.ErrorCode === "4004") {
        customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.cardless.DuplicateAmount"), popupCommonAlertDimiss, "");
    } else customAlertPopup(geti18Value("i18n.Bene.Failed"), getErrorMessage(response.ErrorCode), popupCommonAlertDimiss, "");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}
/*CardLess Authorization */
/* Delete Cardless Transaction*/
kony.boj.deleteCardlessTransFromSegment = function(eventobject, context) {
    customAlertPopup(geti18nkey("i18n.Cardless.DeleteCardless"), geti18nkey("i18n.cardless.deleteMessage"), function() {
        selectedCardlessR = context.widgetInfo.selectedRowItems[0];
        bojCardlessTransFromSegment();
    }, function() {
        selectedCardlessR = {};
        popupCommonAlertDimiss()
    }, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
};

function bojCardlessTransFromSegment() {
    popupCommonAlert.dismiss();
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Accounts");
    kony.print("selectedCardlessR ::" + JSON.stringify(selectedCardlessR));
    dataObject.addField("custId", custid);
    dataObject.addField("branchNumber", selectedCardlessR.BranchNo);
    dataObject.addField("accNumber", selectedCardlessR.accountNumber);
    dataObject.addField("Amount", selectedCardlessR.AmountDecimal);
    dataObject.addField("password", selectedCardlessR.password);
    dataObject.addField("authNumber", selectedCardlessR.ReferenceNum);
    dataObject.addField("lang", kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara");
    dataObject.addField("p_user_id", "BOJMOB");
    dataObject.addField("p_channel", "MOBILE");
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("cardlessCancel", serviceOptions, function(res) {
            kony.print("Success ::" + JSON.stringify(res));
            cardlessCancelSuccess(res);
        }, function(err) {
            kony.print("failed ::" + JSON.stringify(err));
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        });
    }
}

function cardlessCancelSuccess(response) {
    kony.print("Success cardlessCancelSuccess-->" + JSON.stringify(response));
    if (response.ErrorCode === "00000") {
        frmManageCardLess.btnAll.skin = "slButtonWhiteTab";
        frmManageCardLess.btnPending.skin = "slButtonWhiteTabDisabled";
        frmManageCardLess.btnAccepted.skin = "slButtonWhiteTabDisabled";
        frmManageCardLess.btnCancel.skin = "slButtonWhiteTabDisabled";
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        //show_Cardless_List("all");
        serv_cardlessTransactions();
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}
/* Delete Cardless Transaction*/
/*Search Cardlessa Transactions*/
kony.boj.searchCardlessTransactions = function(searchTerm) {
    var data = [],
        isChecked = false,
        initial = "";
    //selectedIndex_BULK_PAYMENT = [];
    searchTerm = searchTerm.toLowerCase();
    var flow = "all";
    if (frmManageCardLess.btnAll.skin === "slButtonWhiteTab") {
        //all payee
        flow = "all";
    } else if (frmManageCardLess.btnPending.skin === "slButtonWhiteTab") {
        //Pendein flow payee
        flow = "Pending";
    } else if (frmManageCardLess.btnAccepted.skin === "slButtonWhiteTab") {
        //Accepted flow payee
        flow = "Accept";
    } else {
        //Canceled flow 
        flow = "Reject";
    }
    kony.print(frmManageCardLess.btnAll.skin + " :: " + frmManageCardLess.btnPending.skin + " :: " + frmManageCardLess.btnAccepted.skin);
    kony.print(":flow is  : " + flow);
    var temp = [];
    if (flow === "all") {
        temp = cardlessData;
    } else {
        for (var i in cardlessData) {
            if (cardlessData[i].category === flow) {
                if (cardlessData[i].category === "Pending") {
                    cardlessData[i].btnSetting = {
                        "isVisible": true
                    };
                    cardlessData[i].lblStatus = {
                        "text": geti18Value("i18n.Cardless.pending"),
                        "skin": "lblsknNumberOrange"
                    };
                } else {
                    cardlessData[i].btnSetting = {
                        "isVisible": false
                    };
                }
                if (cardlessData[i].category === "Accept") {
                    cardlessData[i].lblStatus = {
                        "text": geti18Value("i18n.Cardless.accepted"),
                        "skin": "lblsknNumberGreen"
                    };
                } else if (cardlessData[i].category === "Reject") {
                    cardlessData[i].lblStatus = {
                        "text": geti18Value("i18n.Cardless.Reject"),
                        "skin": "lblsknNumberRed"
                    };
                }
                temp.push(cardlessData[i]);
            }
        }
    }
    kony.print("tmo Cardless : " + JSON.stringify(temp));
    for (var i in temp) {
        if (temp[i].Amount.toLowerCase().indexOf(searchTerm) != -1 || temp[i].accountNumber.toLowerCase().indexOf(searchTerm) != -1) {
            /* if(cardlessData[j].category === "Pending"){
               cardlessData[j].btnSetting ={"isVisible": true};
             }else{
               cardlessData[j].btnSetting ={"isVisible": false};  
             }*/
            data.push(temp[i]);
        }
    }
    frmManageCardLess.segCardlessTransactions.setData([]);
    kony.print("search results ::" + JSON.stringify(data));
    frmManageCardLess.segCardlessTransactions.widgetDataMap = {
        lbltmpTitle: "lbltmpTitle",
        transactionDate: "requestDate",
        amount: "Amount",
        accountNumber: "accountNumber",
        btnDelete: "btnSetting",
        lblStatus: "lblStatus"
    };
    if (data.length === 0) {
        frmManageCardLess.segCardlessTransactions.isVisible = false;
        frmManageCardLess.lblSearchResult.isVisible = true;
    } else {
        frmManageCardLess.segCardlessTransactions.isVisible = true;
        frmManageCardLess.lblSearchResult.isVisible = false;
        frmManageCardLess.segCardlessTransactions.setData(data);
    }
};
/*Search Cardlessa Transactions*/
function getAmountFunction(AmountFlag) {
    var value1 = 0;
    resetButtons();
    if (frmCardlessTransaction.flxPlusContainer.skin === "CopyslButtonPaymentDashFocus0fc27e0876b3b4d") {
        frmCardlessTransaction.flxPlusContainer.skin = "plusMinusSkin";
        frmCardlessTransaction.flxPlusContainer.setEnabled(true);
    }
    if (frmCardlessTransaction.flxMinusContainer.skin === "CopyslButtonPaymentDashFocus0fc27e0876b3b4d") {
        frmCardlessTransaction.flxMinusContainer.skin = "plusMinusSkin";
        frmCardlessTransaction.flxMinusContainer.setEnabled(true);
    }
    if (AmountFlag === "ten") {
        frmCardlessTransaction.btnTenJOD.skin = "sknOrangeBGRNDBOJ";
        value1 = 10;
    } else if (AmountFlag === "fifteen") {
        frmCardlessTransaction.btnFiJOD.skin = "sknOrangeBGRNDBOJ";
        value1 = 50;
    } else if (AmountFlag === "hundred") {
        frmCardlessTransaction.btnHuJOD.skin = "sknOrangeBGRNDBOJ";
        value1 = 100;
    } else if (AmountFlag === "hundredFif") {
        frmCardlessTransaction.btnHuFiJOD.skin = "sknOrangeBGRNDBOJ";
        value1 = 150;
    } else if (AmountFlag === "tHundred") {
        frmCardlessTransaction.btnTwoHJOD.skin = "sknOrangeBGRNDBOJ";
        value1 = 200;
    } else if (AmountFlag === "twoHundredFif") {
        frmCardlessTransaction.flxPlusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
        frmCardlessTransaction.flxPlusContainer.setEnabled(false);
        frmCardlessTransaction.btnTwoFiJOD.skin = "sknOrangeBGRNDBOJ";
        value1 = 500;
    }
    checkNextCardless();
    frmCardlessTransaction.txtFieldAmount.text = value1 + "";
    frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
}

function changeAmountFunction(sign) {
    var value = frmCardlessTransaction.txtFieldAmount.text;
    //value = value.replace(/\,/g,"");
    if (value === "" || isEmpty(value) || value === null) value = 0;
    if (sign === "plus") {
        frmCardlessTransaction.txtFieldAmount.text = (parseInt(value) + 5) + "";
        if (parseInt(frmCardlessTransaction.txtFieldAmount.text) === 500) {
            frmCardlessTransaction.flxPlusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
            frmCardlessTransaction.flxPlusContainer.setEnabled(false);
        } else {
            frmCardlessTransaction.flxPlusContainer.skin = "plusMinusSkin";
            frmCardlessTransaction.flxPlusContainer.setEnabled(true);
            frmCardlessTransaction.flxMinusContainer.skin = "plusMinusSkin";
            frmCardlessTransaction.flxMinusContainer.setEnabled(true);
            // frmCardlessTransaction.txtFieldAmount.text = fixDecimal(frmCardlessTransaction.txtFieldAmount.text,getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
        }
        frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
    } else {
        if (parseFloat(value) > 5) frmCardlessTransaction.txtFieldAmount.text = (parseInt(value) - 5) + "";
        else frmCardlessTransaction.txtFieldAmount.text = 0 + "";
        if (parseInt(frmCardlessTransaction.txtFieldAmount.text) === 0) {
            frmCardlessTransaction.flxMinusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
            frmCardlessTransaction.flxMinusContainer.setEnabled(false);
        } else {
            frmCardlessTransaction.flxMinusContainer.skin = "plusMinusSkin";
            frmCardlessTransaction.flxMinusContainer.setEnabled(true);
            frmCardlessTransaction.flxPlusContainer.skin = "plusMinusSkin";
            frmCardlessTransaction.flxPlusContainer.setEnabled(true);
            // frmCardlessTransaction.txtFieldAmount.text = fixDecimal(frmCardlessTransaction.txtFieldAmount.text,getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
        }
        frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
    }
    check_AMOUNT_PREDEFINED(frmCardlessTransaction.txtFieldAmount.text.replace(/,/g, ""));
    checkNextCardless();
}

function resetButtons() {
    frmCardlessTransaction.btnTenJOD.skin = "slButtonBlueFocus";
    frmCardlessTransaction.btnFiJOD.skin = "slButtonBlueFocus";
    frmCardlessTransaction.btnHuJOD.skin = "slButtonBlueFocus";
    frmCardlessTransaction.btnHuFiJOD.skin = "slButtonBlueFocus";
    frmCardlessTransaction.btnTwoHJOD.skin = "slButtonBlueFocus";
    frmCardlessTransaction.btnTwoFiJOD.skin = "slButtonBlueFocus";
}

function sort_BYDATE(object, field) {
    try {
        object.sort(SortByName);

        function SortByName(x, y) {
            return ((new Date(x[field]) == new Date(y[field])) ? 0 : ((new Date(x[field]) < new Date(y[field])) ? 1 : -1));
        };
        return object;
    } catch (e) {
        kony.print("Exception_sort_BYDATE ::" + e);
    }
}

function check_AMOUNT_PREDEFINED(value) {
    try {
        resetButtons();
        value = value + "";
        switch (value) {
            case "10":
                frmCardlessTransaction.btnTenJOD.skin = "sknOrangeBGRNDBOJ";
                break;
            case "50":
                frmCardlessTransaction.btnFiJOD.skin = "sknOrangeBGRNDBOJ";
                break;
            case "100":
                frmCardlessTransaction.btnHuJOD.skin = "sknOrangeBGRNDBOJ";
                break;
            case "150":
                frmCardlessTransaction.btnHuFiJOD.skin = "sknOrangeBGRNDBOJ";
                break;
            case "200":
                frmCardlessTransaction.btnTwoHJOD.skin = "sknOrangeBGRNDBOJ";
                break;
            case "500":
                frmCardlessTransaction.btnTwoFiJOD.skin = "sknOrangeBGRNDBOJ";
                break;
        }
        if (value < 5) {
            frmCardlessTransaction.flxMinusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
            frmCardlessTransaction.flxMinusContainer.setEnabled(false);
        } else if (value >= 496) {
            frmCardlessTransaction.flxPlusContainer.skin = "CopyslButtonPaymentDashFocus0fc27e0876b3b4d";
            frmCardlessTransaction.flxPlusContainer.setEnabled(false);
        } else if ((value >= 5) && (value <= 495)) {
            frmCardlessTransaction.flxMinusContainer.skin = "plusMinusSkin";
            frmCardlessTransaction.flxMinusContainer.setEnabled(true);
            frmCardlessTransaction.flxPlusContainer.skin = "plusMinusSkin";
            frmCardlessTransaction.flxPlusContainer.setEnabled(true);
        }
    } catch (e) {
        kony.print("Exception_check_AMOUNT_PREDEFINED ::" + e);
    }
}

function navigate_CARDLESS_TRANSACTION(from) {
    try {
        frmCardlessTransaction.btnNextCardless.isVisible = true;
        frmCardlessTransaction.btnNextCardless.skin = "jomopaynextDisabled";
        frmCardlessTransaction.btnNextCardless.focusSkin = "jomopaynextDisabled";
        frmCardlessTransaction.btnNextCardless.setEnabled(false);
        if (from === "accountsLanding") {
            gblTModule = "CardLessTransaction";
            set_formCardlessTransaction(kony.store.getItem("BillPayfromAcc"));
        }
        frmCardlessTransaction.show();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } catch (e) {
        kony.print("Exception_navigate_CARDLESS_TRANSACTION ::" + e);
    }
}

function back_CARDLESS_TRANSACTION() {
    try {
        customAlertPopup(geti18Value("i18n.common.AreYouSure"), geti18Value("i18n.Bene.backFromBeneError"), function() {
            serv_cardlessTransactions();
            popupCommonAlertDimiss();
        }, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    } catch (e) {
        kony.print("Exception_back_CARDLESS_TRANSACTION ::" + e);
    }
}