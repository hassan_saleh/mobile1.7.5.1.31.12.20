kony = kony || {};
kony.retailbanking = kony.retailbanking || {};

kony.retailbanking.AdvancedAtmFilter = function(){

};


kony.retailbanking.AdvancedAtmFilter.servicesMasterData = 
  [
  "All",
  "Make an Appointment",
  "On-site Relationship Manager",
  "Home Loan Specialists",
  "Financial Advisors",
  "Investment Services",
  "Foreign Currency Exchange",
  "Retail Branch",
  "Wealth Branch",
  "Business Banking",
  "International Banking Center",
  "Commercial Deposits",
  "Night Deposits",
  "Safe Deposit Box",
  "Handicap Access",
  "ATM - Full Service",
  "ATM - Cash withdrawal Only",
  "ATM - Check Deposits",
  "ATM - Cash Deposits",
  "ATM - Cardless Cash Withdrawal",
  "ATM - Drive Up"
];

kony.retailbanking.AdvancedAtmFilter.mapper = {
  "All" : "All",
  "Make an Appointment" : "",
  "On-site Relationship Manager" : "",
  "Home Loan Specialists" : "finance",
  "Financial Advisors" : "finance",
  "Investment Services" : "",
  "Foreign Currency Exchange" : "finance",
  "Retail Branch" : "bank",
  "Wealth Branch" : "",
  "Business Banking" : "bank",
  "International Banking Center" : "bank",
  "Commercial Deposits" : "bank",
  "Night Deposits" : "bank",
  "Safe Deposit Box" : "bank",
  "Handicap Access" : "",
  "ATM - Full Service" : "atm",
  "ATM - Cash withdrawal Only" : "atm",
  "ATM - Check Deposits" : "atm",
  "ATM - Cash Deposits" : "atm",
  "ATM - Cardless Cash Withdrawal" : "atm",
  "ATM - Drive Up" : "atm"
};

//kony.retailbanking.AdvancedAtmFilter.servicesMasterData = ["All","ATM","BANK","Finance"];
kony.retailbanking.AdvancedAtmFilter.rangeList = ["5 Miles","10 Miles","20 Miles","30 Miles","40 Miles"];
kony.retailbanking.AdvancedAtmFilter.isFilterApplied = false;
kony.retailbanking.AdvancedAtmFilter.ServicesSelected = null;
kony.retailbanking.AdvancedAtmFilter.rangeSelected = null;

kony.retailbanking.AdvancedAtmFilter.prototype.convertArrayToSegData = function(services){
  try{
    var servicesSegData = [];
    if(services !== null && services !== undefined){
      for(var i=0;i<services.length;i++){
        var jsonFormat = {"serviceType":services[i],
                          "imgCheckVal" :"checkboxunselected.png"
                         };
        servicesSegData.push(jsonFormat);
      }
    }
    return servicesSegData;
  }catch(err){
    customErrorCallback(err);
  }
};
kony.retailbanking.AdvancedAtmFilter.prototype.setDataToServices = function(servicesData){
  try{
    if(servicesData !== null && servicesData !== undefined){
      var servicesSegWidgetDataMap = {
        "lblServiceName" : "serviceType",
        "imgCheck" : "imgCheckVal"
      };
      frmAtmAdvancedFilterKA.segServices.widgetDataMap = servicesSegWidgetDataMap;
      frmAtmAdvancedFilterKA.segServices.setData(servicesData);
    }
  }catch(err){
    customErrorCallback(err);
  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.setAtmSearchRangeList = function(list){
  try{
    var masterData = [];
    var selectedKey = null;
    if(list !== null && list !== undefined){
      for(var i=0;i<list.length;i++){
        var temp = [list[i],list[i]];
        masterData.push(temp);
      }
      selectedKey = list[0];
    }
    frmAtmAdvancedFilterKA.listBoxRange.masterData = masterData;
    if(selectedKey !== null)
      frmAtmAdvancedFilterKA.listBoxRange.selectedKey = selectedKey;
  }catch(err){
    customErrorCallback(err);
  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.onClickOfApply = function(){
  try{
    frmLocatorKA.locatorSearchTextField.text = "";
    kony.application.showLoadingScreen(null,"", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    var segServicesData = frmAtmAdvancedFilterKA.segServices.selectedRowIndices;
    var selectedRange  = frmAtmAdvancedFilterKA.listBoxRange.selectedKey;
    if(segServicesData === null){
      if(selectedRange === "5 Miles"){
        if(gblFormName === "frmLoginKA"){
          onClickLocateUS(false,gblFormName);
          return;
        }
        else{
          onClickLocateUS(true,gblFormName);
          return;
        }
      }
    }
    var selectedIndexesForServices = [];
    if(segServicesData&&segServicesData[0]&&segServicesData[0][1]&&segServicesData[0][1]){
      selectedIndexesForServices = segServicesData[0][1];
    }
    var selectedServices = [];
    var masterData = kony.retailbanking.AdvancedAtmFilter.servicesMasterData;
    for(var i=0;i<selectedIndexesForServices.length;i++){
      var temp = masterData[selectedIndexesForServices[i]];
      var mapperValue = kony.retailbanking.AdvancedAtmFilter.mapper[temp];
      if(mapperValue !== undefined)
      selectedServices.push(mapperValue);
    }
    var selectedRangeSplit = selectedRange.split(" ");
    var selectedRangeValue = selectedRangeSplit[0];
    kony.retailbanking.AdvancedAtmFilter.ServicesSelected = segServicesData;
    kony.retailbanking.AdvancedAtmFilter.rangeSelected = selectedRange;
    this.filterTheAtms(selectedServices,selectedRangeValue);
  }catch(err){
    customErrorCallback(err);
    kony.application.dismissLoadingScreen();
  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.filterTheAtms = function(services,range){
  try{
    kony.application.showLoadingScreen(null,"", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    var self = this;
    kony.location.getCurrentPosition(function(response){
      gblIsCurrentLocation = true;
      if(response && response.coords && response.coords.latitude && response.coords.longitude){
        gblLatitude =response.coords.latitude;
        gbLlongitude =response.coords.longitude; 
      }
      var options = {"access":"online"};
      var objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
      var dataObject = new kony.sdk.dto.DataObject("Locations");
      var radiusInMeters = 1609* parseInt(range);
      dataObject.addField("currLatitude",gblLatitude);
      dataObject.addField("currLongitude",gbLlongitude);
      dataObject.addField("radius",radiusInMeters);
      var queryParams = {"currLatitude":gblLatitude?gblLatitude:"","currLongitude":gbLlongitude?gbLlongitude:"","radius":radiusInMeters};
      var serviceOptions = {"dataObject":dataObject};//,"queryParams":queryParams};
      kony.print("Perf log for map loading -- Start");
      objectService.customVerb('getLocationRange',serviceOptions,self.successFetchAtms.bind(self,services,range),self.errorFetchAtms.bind(self));
    },function(err){
      customErrorCallback(err);
      kony.application.dismissLoadingScreen();
    });
  }catch(err){
    customErrorCallback(err);
    kony.application.dismissLoadingScreen();
  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.successFetchAtms = function(services,range,response){
  try{
    kony.print(JSON.stringify(response)+"-----------------"+services+"---------------"+range);
    kony.application.showLoadingScreen(null,"", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    globalZoomLevel = this.getZoomLevelBasedOnRange(range);
    //frmLocatorKA.imgFilter.src = "filter.png";
    kony.retailbanking.AdvancedAtmFilter.isFilterApplied = true;
    var atmData = response.Locations;
    var result = [];
    if(atmData === undefined || atmData === null || services === null || services.length === 0){
      setDataMapCallBack(response);
      return;
    }
    if(services[0].toLowerCase() === "all"){
      setDataMapCallBack(response);
      return;
    }
    for(var i=0;i<atmData.length;i++){
      var atmServices = atmData[i].services.toLowerCase().split("||");
      for(var j=0;j<services.length;j++){
        if(atmServices.indexOf(services[j].toLowerCase()) > -1){
          result.push(atmData[i]);
          break;
        }
      }
    }
    var res = {};
    res.Locations = result;
    setDataMapCallBack(res);
  }catch(err){
    customErrorCallback(err);
    kony.application.dismissLoadingScreen();
  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.getZoomLevelBasedOnRange = function(range){
  try{
    range = parseInt(range);
    if(range >= 0 && range <=5){
      return 15;
    }
    if(range > 5 && range <= 10){
      return 13;
    }
    if(range > 10 && range <= 20){
      return 11;
    }
    if(range >20 && range <= 30){
      return 10;
    }
    if(range >30){
      return 8;
    }
  }catch(err){

  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.errorFetchAtms = function(err){
  try{
    customErrorCallback(err);
  }catch(error){
    customErrorCallback(error);
  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.showForm = function(){
  try{
    if(kony.retailbanking.AdvancedAtmFilter.isFilterApplied === false){
      var servicesData = this.convertArrayToSegData(kony.retailbanking.AdvancedAtmFilter.servicesMasterData);
      this.setDataToServices(servicesData);
      this.setAtmSearchRangeList(kony.retailbanking.AdvancedAtmFilter.rangeList);
    }
    else{
      frmAtmAdvancedFilterKA.segServices.selectedRowIndices = kony.retailbanking.AdvancedAtmFilter.ServicesSelected;
      frmAtmAdvancedFilterKA.listBoxRange.selectedKey = kony.retailbanking.AdvancedAtmFilter.rangeSelected;
    }
    frmAtmAdvancedFilterKA.show();
  }catch(err){
    customErrorCallback(err);
  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.onClickOfBackButton = function(){
  try{
    if(kony.retailbanking.AdvancedAtmFilter.isFilterApplied === false){
      if(gblFormName === "frmLoginKA"){
        onClickLocateUS(false,gblFormName);
      }
      else{
        onClickLocateUS(true,gblFormName);
      }
    }
    else{
      frmLocatorKA.show();
    }

  }catch(err){
    customErrorCallback(err);
  }
};


kony.retailbanking.AdvancedAtmFilter.prototype.clearFilter = function(){
  try{
    kony.application.showLoadingScreen(null,"", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    var globalList = kony.retailbanking.AdvancedAtmFilter.rangeList;
    frmAtmAdvancedFilterKA.listBoxRange.selectedKey = globalList[0];
    frmAtmAdvancedFilterKA.segServices.selectedRowIndices = null;
    kony.retailbanking.AdvancedAtmFilter.isFilterApplied = false;
    //frmLocatorKA.imgFilter.src = "filterunselected.png";
    if(gblFormName === "frmLoginKA"){
      onClickLocateUS(false,gblFormName);
    }
    else{
      onClickLocateUS(true,gblFormName);
    }

  }catch(err){
    customErrorCallback(err);
  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.onSearchClearFilter = function(){
  try{
    var globalList = kony.retailbanking.AdvancedAtmFilter.rangeList;
    frmAtmAdvancedFilterKA.listBoxRange.selectedKey = globalList[0];
    frmAtmAdvancedFilterKA.segServices.selectedRowIndices = null;
    kony.retailbanking.AdvancedAtmFilter.isFilterApplied = false;
    //frmLocatorKA.imgFilter.src = "filterunselected.png";
  }catch(err){

  }
};

kony.retailbanking.AdvancedAtmFilter.prototype.onRowClickOfSeg = function(rowNumber){
  try{
    if(frmAtmAdvancedFilterKA.segServices.selectedRowIndices === null){
      return;
    }
    if(rowNumber === 0){
      var keys = frmAtmAdvancedFilterKA.segServices.selectedRowIndices;
      var indexes = keys[0][1];
      if(indexes[0] === 0){
        var length = kony.retailbanking.AdvancedAtmFilter.servicesMasterData.length;
        var res = [];
        for(var i=0;i<length;i++){
          res.push(i);
        }
        frmAtmAdvancedFilterKA.segServices.selectedRowIndices = [[0,res]];
      }
      return;
    }
    if(rowNumber > 0){
      var selectedIndexes = frmAtmAdvancedFilterKA.segServices.selectedRowIndices;
      var values = selectedIndexes[0][1];
      if(values.indexOf(rowNumber) === -1){
        var resultVal = [];
        var j = 0;
        if(values[0] === 0 ){
          j = 1;
        }
        for(;j<values.length;j++){
          resultVal.push(values[j]);
        }
        frmAtmAdvancedFilterKA.segServices.selectedRowIndices = [[0,resultVal]];
      }
    }
  }catch(err){
    alert(err+"    "+JSON.stringify(err));
  }
};