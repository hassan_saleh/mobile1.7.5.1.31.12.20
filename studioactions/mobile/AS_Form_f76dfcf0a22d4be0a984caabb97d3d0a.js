function AS_Form_f76dfcf0a22d4be0a984caabb97d3d0a(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        frmAccountsLandingKA.flxDeals.setVisibility(false);
        frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
        frmAccountsLandingKA.flxAccountsMain.left = "0%"
        frmAccountsLandingKA.flxLoans.setVisibility(false);
        frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
        frmAccountsLandingKA.flxDeals.zIndex = 1;
        frmAccountsLandingKA.flxLoans.zIndex = 1;
        frmAccountsLandingKA.forceLayout();
        accountDashboardDataCall();
        kony.print("Nav accounts Dadhboard:::");
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}