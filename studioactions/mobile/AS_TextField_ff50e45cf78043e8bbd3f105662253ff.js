function AS_TextField_ff50e45cf78043e8bbd3f105662253ff(eventobject, changedtext) {
    animateLabel("DOWN", "lblNickName", frmAddNewPrePayeeKA.tbxNickName.text);
    animateLabel("UP", "lblBillerNumber", frmAddNewPrePayeeKA.tbxBillerNumber.text);
    frmAddNewPrePayeeKA.tbxNickName.text = frmAddNewPrePayeeKA.tbxNickName.text.trim();
    if (frmAddNewPrePayeeKA.tbxNickName.text !== null && frmAddNewPrePayeeKA.tbxNickName.text !== "") {
        frmAddNewPrePayeeKA.flxUnderlineNickName.skin = "sknFlxGreenLine";
    } else {
        frmAddNewPrePayeeKA.flxUnderlineNickName.skin = "sknFlxOrangeLine";
    }
}