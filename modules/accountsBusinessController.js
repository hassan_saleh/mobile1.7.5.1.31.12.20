kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.accountsBusinessController = function(mfObjSvc){
  this.mfObjectServiceHandler = mfObjSvc;
};
kony.rb.accountsBusinessController.prototype.fetchAccountsSupportCardlessWithdraw = function(presentationSuccessCallback,presentationErrorCallback){
     var headers={"session_token":kony.retailBanking.globalData.session_token};
     this.mfObjectServiceHandler.fetch("Accounts",{},headers, success, error);
   var utlHandler = applicationManager.getUtilityHandler();
     function success(res)	
	{
	  var acntArray = res.records;
      for(var i=0;i<acntArray.length;i++){
        acntArray[i] = utlHandler.convertJSonToAccountsObject(acntArray[i]);
		
      }
      presentationSuccessCallback(acntArray);
	}
	function error(err)
	{
	presentationErrorCallback(err);
	alert("inside error of business controller");
	}
   
};
  
  