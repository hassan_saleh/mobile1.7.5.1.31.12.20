function AS_Button_e7f9754edb484273a7a66e136841659e(eventobject) {
    //frmManageCardsKA.btnDeactivateKA.setEnabled(true);
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmManageCardsKA");
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
    navigationObject.setRequestOptions("segCardsKA", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    controller.performAction("navigateTo", ["frmManageCardsKA", navigationObject]);
}