function AS_Form_ca304034e0a24dab9d1c3470ae721674(eventobject) {
    if (kony.boj.detailsForBene.lblToUpdate === "lblBranchName") frmSelectBankName.lblTitle.text = geti18Value("i18n.Map.Branchname");
    else if (kony.boj.detailsForBene.lblToUpdate === "lblCountryNameKA") frmSelectBankName.lblTitle.text = geti18Value("i18n.NUO.Country");
    else if (kony.boj.detailsForBene.lblToUpdate === "lblCityName" || kony.boj.detailsForBene.lblToUpdate === "lblCityBank") frmSelectBankName.lblTitle.text = geti18Value("i18n.manage_payee.cityPlh");
    else if (kony.boj.detailsForBene.lblToUpdate === "lblBankBranch") frmSelectBankName.lblTitle.text = geti18Value("i18n.Bene.BankBranch");
    else if (kony.boj.detailsForBene.lblToUpdate === "lblBankName") frmSelectBankName.lblTitle.text = geti18Value("i18n.Bene.Bankname");
    frmSelectBankName.tbxSearch.text = "";
}