//Type your code here
//Save Quick access settings
function callSetQucikAccessList() {
    kony.print("callSetQucikAccessList:::::");
    var AccountList = [];
    var QbalFlag = "N";
    var QTraFlag = "N";
    var AccData = [];
    var AorQFlag = "Q";
    kony.print("AccData:::::" + JSON.stringify(AccData));
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        ShowLoadingScreen();
        if (kony.application.getCurrentForm().id === "frmAuthorizationAlternatives") {
            AccData = frmAuthorizationAlternatives.segAccounts.data;
            AorQFlag = "Q";
            if (frmAuthorizationAlternatives.flxSwitchOn.isVisible) {
                QbalFlag = "Y";
                gblQuickAccessFlowFlag = "E";
            } else {
                QbalFlag = "N";
                gblQuickAccessFlowFlag = "D";
            }
            gblQbalShowFlag = QbalFlag;
            if (frmAuthorizationAlternatives.flxSwitchTransactionsOn.isVisible) {
                QTraFlag = "Y";
            } else {
                QTraFlag = "N";
            }
            gblQtransFlagShowFlag = QTraFlag;
            if (!(isEmpty(AccData)) && AccData.length > 0) {
                kony.print("length" + AccData.length);
                AccountList = [];
                for (var i in AccData) {
                    var HideShowFlagt = "";
                    if (AccData[i].lblIncommingTick.isVisible) HideShowFlagt = "T";
                    else HideShowFlagt = "F";
                    var accObj = {
                        AccountNumber: AccData[i].accountNumber,
                        AccName: AccData[i].lblAccountName,
                        PreferenceNumber: i,
                        HideShowFlag: HideShowFlagt,
                        NickName: ""
                    };
                    AccountList.push(accObj);
                }
            } else {
                AccountList = [];
            }
        } else if (kony.application.getCurrentForm().id === "frmAccountsReorderKA") {
            AccData = frmAccountsReorderKA.segReOrderAccounts.data;
            AorQFlag = "A";
            QbalFlag = "";
            QTraFlag = "";
            kony.print("length" + AccData.length);
            if (!(isEmpty(AccData)) && AccData.length > 0) {
                AccountList = [];
                for (var z in AccData) {
                    var accObj1 = {
                        AccountNumber: AccData[z].accountNumber,
                        AccName: AccData[z].lblAccountName,
                        PreferenceNumber: z,
                        HideShowFlag: "",
                        NickName: ""
                    };
                    AccountList.push(accObj1);
                }
            } else {
                AccountList = [];
            }
        }
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        kony.print("AccountList::" + JSON.stringify(AccountList));
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
        dataObject.addField("custId", custid);
        dataObject.addField("Language", Language);
        dataObject.addField("user_id", user_id);
        dataObject.addField("FlowFlag", gblQuickAccessFlowFlag);
        dataObject.addField("QbalFlag", QbalFlag);
        dataObject.addField("QTraFlag", QTraFlag);
        dataObject.addField("AccountList", (JSON.stringify(AccountList)).replace(/"/g, "'"));
        dataObject.addField("AorQFlag", AorQFlag);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params1-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("SetQuickAccessList", serviceOptions, callSetQucikAccessListSuccess, callSetQucikAccessListError);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function callSetQucikAccessListSuccess(response) {
    kony.print("callSetQucikAccessListSuccess:::" + JSON.stringify(response));
    if (kony.application.getCurrentForm().id === "frmAuthorizationAlternatives") {
        if (frmAuthorizationAlternatives.btnQuickCancel.isVisible) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            frmSettingsKA.show();
        } else {
            frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            accountDashboardDataCall();
        }
    } else if (kony.application.getCurrentForm().id === "frmAccountsReorderKA") {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        frmSettingsKA.show();
    }
}

function callSetQucikAccessListError(err) {
    kony.print("callSetQucikAccessListError:::" + JSON.stringify(err));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
    customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
    if (kony.application.getCurrentForm().id === "frmAuthorizationAlternatives") {
        if (frmAuthorizationAlternatives.btnQuickCancel.isVisible) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            frmSettingsKA.show();
        } else {
            frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            accountDashboardDataCall();
        }
    } else if (kony.application.getCurrentForm().id === "frmAccountsReorderKA") {
        frmSettingsKA.show();
    }
}
//SetData to Account settings account data for nickname change
function getAccountsforNicknameSettings() {
    kony.print("getAccountsforNicknameSettings");
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance()) {
            ShowLoadingScreen();
            /* var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var options ={    "access": "online",
                    "objectName": "RBObjects"
                   };
      var serviceName = "RBObjects";
      var langSelected = kony.store.getItem("langPrefObj");
      var Language = langSelected.toUpperCase();
      var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
      var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
      dataObject.addField("custId", custid);
      dataObject.addField("Language", Language);
      var serviceOptions = {
        "dataObject": dataObject,
        "headers": {}
      };
      kony.print("Input params1----->" + JSON.stringify(serviceOptions));
      modelObj.customVerb("GetQuickAccessList", serviceOptions, getAccountsforNicknameSettingsSuccess, getAccountDataforQuickPreviewAccountsError);
    */
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var serviceName = "RBObjects";
            var langSelected = kony.store.getItem("langPrefObj");
            var Language = langSelected.toUpperCase();
            var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("Accounts");
            var serviceOptions = {
                "dataObject": dataObject,
                "queryParams": {
                    "custId": custid,
                    "lang": kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara"
                }
            };
            kony.print("Input params1----->" + JSON.stringify(serviceOptions));
            modelObj.fetch(serviceOptions, getAccountsforNicknameSettingsSuccess, getAccountDataforQuickPreviewAccountsError);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function getAccountsforNicknameSettingsSuccess(response) {
    kony.print("getAccountsforNicknameSettingsSuccess:::" + JSON.stringify(response));
    var accountsRawData = response;
    var accountsFilteredData = [];
    var accountsMasterdata = [];
    if (!isEmpty(response)) {
        kony.print("accountsRawData length:::" + JSON.stringify(accountsRawData.length));
        if (accountsRawData !== undefined && accountsRawData !== null && accountsRawData.length > 0) {
            frmMyAccountSettingsKA.segChooseAccounts.widgetDataMap = {
                lblAccountName: "lblAccountName",
                lblAccountNumber: "lblAccountNumber",
                lblAccNickname: "lblAccNickname",
                lblACCHideShow: "lblACCHideShow",
                lblDefaultAccTransfer: "lblDefaultAccTransfer",
                lblDefaultAccPayment: "lblDefaultAccPayment"
            };
            for (var z in accountsRawData) {
                if ((accountsRawData[z]["accountType"] == "S" || accountsRawData[z]["accountType"] == "C" || accountsRawData[z]["accountType"] == "Y" || accountsRawData[z]["accountType"] == "CC" || accountsRawData[z]["accountType"] == "B") && accountsRawData[z]["productCode"] != "320") {
                    accountsFilteredData.push(accountsRawData[z]);
                }
            }
            kony.print("accountsFilteredData filterdd:::" + JSON.stringify(accountsFilteredData));
            if (accountsFilteredData !== undefined && accountsFilteredData !== null && accountsFilteredData.length > 0) {
                for (var k in accountsFilteredData) {
                    kony.print("k :::" + k);
                    var segRowData1 = {};
                    var accountNumber = "";
                    var accountName = "";
                    if (!isEmpty(accountsFilteredData[k]["accountNumber"])) accountNumber = accountsFilteredData[k]["accountNumber"];
                    else if (!isEmpty(accountsFilteredData[k]["accountID"])) accountNumber = accountsFilteredData[k]["accountID"];
                    //accountName = (accountsFilteredData[k]["AccNickName"] !== undefined && accountsFilteredData[k]["AccNickName"] !== "")?accountsFilteredData[k]["AccNickName"]:accountsFilteredData[k]["accountName"];
                    if (isEmpty(accountsFilteredData[k]["AccNickName"])) {
                        accountName = accountsFilteredData[k]["accountName"];
                        accountNumber = {
                            "text": accountNumber,
                            "isVisible": true
                        }
                        if (accountName !== null && accountName !== "") {
                            if (accountName.length > 40) accountName = accountName.substring(0, 33) + "  ...";
                            accountName = accountName;
                        }
                    } else {
                        accountName = accountsFilteredData[k]["AccNickName"];
                        accountNumber = {
                            "text": accountNumber,
                            "isVisible": false
                        }
                        if (accountName !== null && accountName !== "") {
                            if (accountName.length > 40) accountName = accountName.substring(0, 40) + "  ...";
                        }
                    }
                    //         if(accountName !== null && accountName !==""  ){
                    //         if(accountName.length > 30)	 
                    //           accountName = accountName.substring(0,20) + "  .  .  .";
                    //            }
                    segRowData1["lblAccountName"] = accountName;
                    segRowData1["lblAccNickname"] = accountsFilteredData[k]["AccNickName"];
                    segRowData1["lblACCHideShow"] = accountsFilteredData[k]["AccHideShowFlag"];
                    segRowData1["lblAccountNumber"] = accountNumber;
                    segRowData1["lblAccName"] = accountsFilteredData[k]["accountName"];
                    accountsMasterdata.push(segRowData1);
                }
                kony.print("accountsMasterdata" + JSON.stringify(accountsMasterdata));
                frmMyAccountSettingsKA.segChooseAccounts.setData(accountsMasterdata);
            }
        }
    }
    frmMyAccountSettingsKA.segChooseAccounts.setData(accountsMasterdata);
    kony.print("seg length" + frmMyAccountSettingsKA.segChooseAccounts.data.length);
    if (frmMyAccountSettingsKA.segChooseAccounts.data.length > 0) {
        kony.print("Inside segfrmMyAccountSettingsKA");
        frmMyAccountSettingsKA.segChooseAccounts.isVisible = true;
        frmMyAccountSettingsKA.LabelNoRecordsKA.isVisible = false;
    } else {
        kony.print("no data");
        frmMyAccountSettingsKA.segChooseAccounts.isVisible = false;
        frmMyAccountSettingsKA.LabelNoRecordsKA.isVisible = true;
    }
    frmMyAccountSettingsKA.show();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}
//Set data to Edit Nickname screen
function setDatatoEditNickNameScreen(data, section, row) {
    kony.print("setDatatoEditNickNameScreen::" + JSON.stringify(data) + "section::" + section + "row::" + row);
    var selectedData = frmMyAccountSettingsKA.segChooseAccounts.data[parseInt(row)];
    kony.print("selectedData:" + JSON.stringify(selectedData));
    //frmEditAccountSettings.btnEdit.text = 0+""; 
    var accountName = "";
    if (!isEmpty(selectedData.lblAccountNumber.text)) {
        if (isEmpty(selectedData.lblAccNickname)) {
            accountName = selectedData.lblAccName;
            if (accountName !== null && accountName !== "") {
                if (accountName.length > 30) accountName = accountName.substring(0, 22) + "  ...";
                accountName = accountName + " " + selectedData.lblAccountNumber.text;
            }
        } else {
            accountName = selectedData.lblAccNickname;
            if (accountName !== null && accountName !== "") {
                if (accountName.length > 40) accountName = accountName.substring(0, 40) + "  ...";
            }
        }
        frmEditAccountSettings.lblAccNumber.text = accountName;
    } else {
        frmEditAccountSettings.lblAccNumber.text = "";
    }
    if (!isEmpty(selectedData.lblAccNickname)) {
        frmEditAccountSettings.txtAddNickName.text = selectedData.lblAccNickname;
        //animateLabel("UP","lblAddNickName",frmEditAccountSettings.txtAddNickName.text);
    } else {
        frmEditAccountSettings.txtAddNickName.text = "";
        //animateLabel("DOWN","lblAddNickName",frmEditAccountSettings.txtAddNickName.text);
    }
    if (!isEmpty(selectedData.lblACCHideShow)) {
        if (selectedData.lblACCHideShow === "Y" || selectedData.lblACCHideShow === "T") {
            frmEditAccountSettings.flxShowHideSwitchOff.isVisible = false;
            frmEditAccountSettings.flxShowHideSwitchOn.isVisible = true;
        } else {
            frmEditAccountSettings.flxShowHideSwitchOff.isVisible = true;
            frmEditAccountSettings.flxShowHideSwitchOn.isVisible = false;
        }
    } else {
        frmEditAccountSettings.flxShowHideSwitchOff.isVisible = false;
        frmEditAccountSettings.flxShowHideSwitchOn.isVisible = true;
    }
    kony.print("gblDefaultAccPayment::" + gblDefaultAccPayment);
    if (!isEmpty(gblDefaultAccPayment)) {
        if (gblDefaultAccPayment === selectedData.lblAccountNumber.text) {
            frmEditAccountSettings.flxDefaultPaymentSwitchOff.isVisible = false;
            frmEditAccountSettings.flxDefaultPaymentSwitchOn.isVisible = true;
        } else {
            frmEditAccountSettings.flxDefaultPaymentSwitchOff.isVisible = true;
            frmEditAccountSettings.flxDefaultPaymentSwitchOn.isVisible = false;
        }
    } else {
        frmEditAccountSettings.flxDefaultPaymentSwitchOff.isVisible = true;
        frmEditAccountSettings.flxDefaultPaymentSwitchOn.isVisible = false;
    }
    kony.print("gblDefaultAccTransfer::" + gblDefaultAccTransfer);
    if (!isEmpty(gblDefaultAccTransfer)) {
        if (gblDefaultAccTransfer === selectedData.lblAccountNumber.text) {
            frmEditAccountSettings.flxAccountTransferSwitchOff.isVisible = false;
            frmEditAccountSettings.flxAccountTransferSwitchOn.isVisible = true;
        } else {
            frmEditAccountSettings.flxAccountTransferSwitchOff.isVisible = true;
            frmEditAccountSettings.flxAccountTransferSwitchOn.isVisible = false;
        }
    } else {
        frmEditAccountSettings.flxAccountTransferSwitchOff.isVisible = true;
        frmEditAccountSettings.flxAccountTransferSwitchOn.isVisible = false;
    }
    frmEditAccountSettings.show();
}
//Save nickname and account settings
function callSetNicknameAletSettings() {
    kony.print("callSetNicknameAletSettings:::::" + JSON.stringify(frmMyAccountSettingsKA.segChooseAccounts.selectedRowIndex));
    var Defaultpayment = "N";
    var DefaultTransfer = "N";
    var HideShowFlag = "N";
    var NickNameFlag = "F";
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        ShowLoadingScreen();
        var selectedData = frmMyAccountSettingsKA.segChooseAccounts.data[parseInt(frmMyAccountSettingsKA.segChooseAccounts.selectedRowIndex[1])];
        kony.print("selectedData::" + JSON.stringify(selectedData));
        if (frmEditAccountSettings.flxDefaultPaymentSwitchOn.isVisible) {
            Defaultpayment = "Y";
            gblDefaultAccPayment = selectedData.lblAccountNumber.text;
        } else {
            Defaultpayment = "N";
            if (gblDefaultAccPayment === selectedData.lblAccountNumber.text) gblDefaultAccPayment = "";
        }
        if (frmEditAccountSettings.flxShowHideSwitchOn.isVisible) {
            HideShowFlag = "Y";
        } else {
            HideShowFlag = "N";
        }
        if (frmEditAccountSettings.flxAccountTransferSwitchOn.isVisible) {
            DefaultTransfer = "Y";
            gblDefaultAccTransfer = selectedData.lblAccountNumber.text;
        } else {
            DefaultTransfer = "N";
            if (gblDefaultAccTransfer === selectedData.lblAccountNumber.text) gblDefaultAccTransfer = "";
        }
        if (selectedData.lblAccNickname === frmEditAccountSettings.txtAddNickName.text) {
            NickNameFlag = "F";
        } else {
            NickNameFlag = "T";
        }
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
        dataObject.addField("custId", custid);
        dataObject.addField("lang", Language);
        dataObject.addField("user_id", user_id);
        dataObject.addField("AccNumber", selectedData.lblAccountNumber.text);
        dataObject.addField("AccName", selectedData.lblAccName);
        dataObject.addField("AccNickName", frmEditAccountSettings.txtAddNickName.text);
        dataObject.addField("DefaultTransfer", DefaultTransfer);
        dataObject.addField("Defaultpayment", Defaultpayment);
        dataObject.addField("HideShowFlag", HideShowFlag);
        dataObject.addField("NickNameFlag", NickNameFlag);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params1 SetNickName-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("SetNickName", serviceOptions, callSetNickNameSuccess, callSetNickNameError);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function callSetNickNameSuccess(response) {
    kony.print("callSetNickNameSuccess:::" + JSON.stringify(response));
    var selectedData = frmMyAccountSettingsKA.segChooseAccounts.data[parseInt(frmMyAccountSettingsKA.segChooseAccounts.selectedRowIndex[1])];
    kony.print("selectedData::" + JSON.stringify(selectedData));
    var accountData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
    if (!isEmpty(selectedData.lblAccountNumber.text)) {
        for (var i in accountData) {
            if (selectedData.lblAccountNumber.text === accountData[i].accountID) {
                accountData[i].AccNickName = frmEditAccountSettings.txtAddNickName.text;
                break;
            }
        }
    }
    getAccountsforNicknameSettings();
}

function callSetNickNameError(err) {
    kony.print("callSetNickNameError:::" + JSON.stringify(err));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
    customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
}
//Save callSaveDataToQuickAccessAccountListDB
function callSaveDataToQuickAccessAccountListDB() {
    kony.print("callSaveDataToQuickAccessAccountListDB:::::");
    var AccountList = [];
    var QbalFlag = gblQbalShowFlag;
    var QTraFlag = gblQtransFlagShowFlag;
    var AccData = [];
    var AorQFlag = "Q";
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        //ShowLoadingScreen();
        AccData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
        AorQFlag = "Q";
        if (QbalFlag === "Y") {
            gblQuickAccessFlowFlag = "E";
        } else {
            gblQuickAccessFlowFlag = "D";
        }
        if (!(isEmpty(AccData)) && AccData.length > 0) {
            kony.print("length" + AccData.length);
            AccountList = [];
            for (var i in AccData) {
                var HideShowFlagt = "";
                if (!isEmpty(AccData[i]["QBHideShowFlag"]) && AccData[i]["QBHideShowFlag"] === "T") HideShowFlagt = "T";
                else HideShowFlagt = "F";
                var accObj = {
                    AccountNumber: AccData[i].accountID,
                    AccName: AccData[i].accountName,
                    PreferenceNumber: i,
                    HideShowFlag: HideShowFlagt,
                    NickName: ""
                };
                AccountList.push(accObj);
            }
        } else {
            AccountList = [];
        }
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        kony.print("AccountList::" + JSON.stringify(AccountList));
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
        dataObject.addField("custId", custid);
        dataObject.addField("Language", Language);
        dataObject.addField("user_id", user_id);
        dataObject.addField("FlowFlag", gblQuickAccessFlowFlag);
        dataObject.addField("QbalFlag", QbalFlag);
        dataObject.addField("QTraFlag", QTraFlag);
        dataObject.addField("AccountList", (JSON.stringify(AccountList)).replace(/"/g, "'"));
        dataObject.addField("AorQFlag", AorQFlag);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params1SAvee-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("SetQuickAccessList", serviceOptions, callSaveDataToQuickAccessAccountListDBSuccess, callSaveDataToQuickAccessAccountListDBError);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function callSaveDataToQuickAccessAccountListDBSuccess(response) {
    kony.print("callSaveDataToQuickAccessAccountListDBSuccess:::" + JSON.stringify(response));
}

function callSaveDataToQuickAccessAccountListDBError(err) {
    kony.print("callSaveDataToQuickAccessAccountListDBError:::" + JSON.stringify(err));
}

function getWidthWidget(context) {
    kony.print("getWidthWidget:::" + JSON.stringify(context));
    alert("getWidthWidget:::" + JSON.stringify(context));
    alert("this" + this.parent.nameAccount1);
}