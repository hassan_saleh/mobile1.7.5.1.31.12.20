function AS_Image_f413013b0a954673b0533cafb5b5b403(eventobject, x, y) {
    //kony.timer.schedule("scanCardTimer",scanCardTimerFn, 15, false);
    if (isAndroid()) {
        BojScan.BlinkCardMethodInvoker(microBlinkCallBack, SCANCARDLICENSEANDROID);
    } else {
        //Creates an object of class 'iPhoneMicroBlink'
        var iPhoneMicroBlinkObject = new BojScan.iPhoneMicroBlink(SCANCARDLICENSEIOS);
        //Invokes method 'scanCardWithCallback' on the object
        iPhoneMicroBlinkObject.scanCardWithCallback(
            /**Function*/
            microBlinkCallBack);
    }
}