//Created by Arpan
kony = kony || {};
kony.boj = kony.boj || {};
kony.boj.selectedPayee = "";
kony.boj.AllPayeeList = [];
kony.boj.deletePayeeFrom = "";
kony.boj.deleteFlagPayee = false;
kony.boj.lang = "";
kony.boj.onManagePayeeClick = function(selectedItems) {
    billType = "POSTPAID";
    kony.boj.selectedPayee = selectedItems;
    if (selectedItems.category === "prepaid") // hassan prePaid
        serv_getPrePaidBeneData(selectedItems); // hassan prePaid
    else // hassan prePaid
        kony.boj.getBillNumberDetails();
};
kony.boj.setAnimationToPayeeSegment = function() {
    var lang = kony.store.getItem("langPrefObj");
    frmManagePayeeKA.managepayeesegment.rowTemplate = flxManagePayeeNormal;
};
kony.boj.preshowManagePayee = function() {
    kony.boj.deleteFlagPayee = false;
    frmManagePayeeKA.btnAll.skin = "slButtonWhiteTab";
    frmManagePayeeKA.btnPrePaid.skin = "slButtonWhiteTabDisabled";
    frmManagePayeeKA.btnPostPaid.skin = "slButtonWhiteTabDisabled";
    frmManagePayeeKA.managepayeesegment.isVisible = true;
    frmManagePayeeKA.lblSearchResult.isVisible = false;
    if (!isEmpty(frmManagePayeeKA.tbxSearch.text)) {
        show_BILLER_LIST("all");
    }
    frmManagePayeeKA.tbxSearch.text = "";
};
kony.boj.getBillNumberDetails = function() {
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Payee", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Payee");
        dataObject.addField("custId", custid);
        dataObject.addField("lang", kony.boj.lang);
        dataObject.addField("p_BillerCode", kony.boj.selectedPayee.BillerCode);
        dataObject.addField("p_ServiceType", kony.boj.selectedPayee.ServiceType);
        dataObject.addField("p_BillingNo", kony.boj.selectedPayee.BillingNumber);
        dataObject.addField("p_BillNo", kony.boj.selectedPayee.BillingNumber);
        dataObject.addField("p_DateFlag", "N");
        var endDate = new Date();
        var startDate = kony.boj.getPreviousDateByMonth(endDate, -10);
        dataObject.addField("p_StartDt", startDate.getFullYear() + "-" + ("0" + (startDate.getMonth() + 1)).slice(-2) + "-" + ("0" + startDate.getDate()).slice(-2));
        dataObject.addField("p_EndDt", endDate.getFullYear() + "-" + ("0" + (endDate.getMonth() + 1)).slice(-2) + "-" + ("0" + endDate.getDate()).slice(-2));
        dataObject.addField("p_CustInfoFlag", "Y");
        dataObject.addField("p_IdType", "");
        dataObject.addField("p_ID", "");
        dataObject.addField("p_Nation", "");
        dataObject.addField("p_IncPayments", "Y");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params BillNumberDetailDetails-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("GetBillDetails", serviceOptions, kony.boj.BillNumberDetailSuccess, kony.boj.BillNumberDetailError);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.hideLoadingScreen("");
        var Message = getErrorMessage("");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
    }
};
kony.boj.BillNumberDetailSuccess = function(response) {
    kony.print("Success BillNumberDetailPostpaid-->" + JSON.stringify(response));
    //alert("Success BillNumberDetailPostpaid-->"+ JSON.stringify(response) );
    kony.application.dismissLoadingScreen();
    frmPayeeDetailsKA.segTransaction.setData([]);
    frmPayeeDetailsKA.lblNoRecords.isVisible = false;
    if (response.ErrorCode === "00000") {
        var temp = [];
        if (response.BillerDetails !== undefined && response.BillerDetails !== null && response.BillerDetails[0].dueamount !== undefined && response.BillerDetails[0].dueamount !== "") {
            frmPayeeDetailsKA.lblDueAmount.text = geti18Value("i18n.bills.DueAmount") + " :  " + setDecimal(response.BillerDetails[0].dueamount, 3) + " JOD";
        } else {
            frmPayeeDetailsKA.lblDueAmount.text = kony.boj.selectedPayee.dueamount;
        }
        if (response.BillerHistory !== undefined) {
            for (var i = 0; i < 10 && i < response.BillerHistory.length; i++) {
                temp.push(response.BillerHistory[i]);
                temp[i].list = response.BillerHistory[i].paidamt + " JOD " + response.BillerHistory[i].pmtstatus;
            }
            frmPayeeDetailsKA.segTransaction.widgetDataMap = {
                lblTime: "processdate",
                lblBiller: "list"
            };
            frmPayeeDetailsKA.segTransaction.setData(temp);
        } else {
            frmPayeeDetailsKA.segTransaction.removeAll();
            frmPayeeDetailsKA.lblNoRecords.isVisible = true;
        }
    } else {
        var Message = getErrorMessage(response.ErrorCode);
        customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
        return;
    }
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmPayeeDetailsKA");
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
    navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navigationObject.setRequestOptions("form", {
        "headers": {}
    });
    listController.performAction("navigateTo", ["frmPayeeDetailsKA", navigationObject]);
};
kony.boj.BillNumberDetailError = function(response) {
    kony.application.dismissLoadingScreen();
    var Message = geti18nkey("i18n.common.somethingwentwrong");
    customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
    kony.print("Failure BillNumberDetailPostpaid-->" + JSON.stringify(response));
};
kony.boj.preshowPayeeDetails = function() {
    frmPayeeDetailsKA.lblName.text = kony.boj.selectedPayee.ServiceType; //NickName
    frmPayeeDetailsKA.lblNickName.text = kony.boj.selectedPayee.NickName;
    frmPayeeDetailsKA.lblType.text = "POSTPAID"; //kony.boj.selectedPayee.BillerCode;
    frmPayeeDetailsKA.lblBillerCode.text = kony.boj.selectedPayee.BillerCode;
    frmPayeeDetailsKA.lblAccountNumber.text = kony.boj.selectedPayee.BillingNumber;
    //   frmPayeeDetailsKA.lblDueAmount.text = kony.boj.selectedPayee.dueamount;
    frmPayeeDetailsKA.lblServiceType.text = kony.boj.selectedPayee.ServiceType;
    if (kony.boj.selectedPayee.NickName != null && kony.boj.selectedPayee.NickName != "") frmPayeeDetailsKA.lblInitial.text = kony.boj.selectedPayee.NickName.substring(0, 2).toUpperCase();
    else frmPayeeDetailsKA.lblInitial.text = "";
    frmPayeeDetailsKA.lblOperation.text = "R";
    frmPayeeDetailsKA.lblDummy.text = "";
    //rama added for testing
    kony.print("frmPayeeDetailsKA.lblAccountNumber.text *" + frmPayeeDetailsKA.lblAccountNumber.text + "**" + kony.boj.selectedPayee.BillingNumber);
    kony.print("frmPayeeDetailsKA.lblServiceType.text *" + frmPayeeDetailsKA.lblServiceType.text);
    kony.print("kony.boj.selectedPayee.BillerCode * " + kony.boj.selectedPayee.BillerCode);
};
kony.boj.searchPayee = function(searchTerm) {
    var data = [],
        isChecked = false,
        initial = "";
    //selectedIndex_BULK_PAYMENT = [];
    validate_BULK_BILL_SELECTION();
    searchTerm = searchTerm.toLowerCase();
    //   ServiceType
    //   BillingNumber
    //   BillingCode
    var flow = "all";
    if (frmManagePayeeKA.btnAll.skin === "slButtonWhiteTab") {
        //all payee
        flow = "all";
    } else if (frmManagePayeeKA.btnPrePaid.skin === "slButtonWhiteTab") {
        //Prepaid flow payee
        flow = "prepaid";
    } else if (frmManagePayeeKA.btnPostPaid.skin === "slButtonWhiteTab") {
        //postpaid flow payee
        flow = "postpaid";
    }
    kony.print(frmManagePayeeKA.btnAll.skin + " :: " + frmManagePayeeKA.btnPrePaid.skin + " :: " + frmManagePayeeKA.btnPostPaid.skin);
    kony.print(":flow is  : " + flow);
    var temp = [];
    if (flow === "all") {
        temp = kony.boj.AllPayeeList;
    } else {
        for (var i in kony.boj.AllPayeeList) {
            if (kony.boj.AllPayeeList[i].category === flow) {
                kony.boj.AllPayeeList[i].initial = kony.boj.AllPayeeList[i].NickName.substring(0, 2).toUpperCase();
                kony.boj.AllPayeeList[i].lblTick = "";
                kony.boj.AllPayeeList[i].btnSetting.text = "q";
                temp.push(kony.boj.AllPayeeList[i]);
            }
        }
    }
    kony.print("tmo : " + JSON.stringify(temp));
    for (var i in temp) {
        if (temp[i].NickName.toLowerCase().indexOf(searchTerm) != -1 || temp[i].BillingNumber.toLowerCase().indexOf(searchTerm) != -1) {
            //        kony.boj.AllPayeeList[i].BillerCode.toLowerCase().indexOf(searchTerm) != -1){
            isChecked = false;
            for (var index in selectedIndex_BULK_PAYMENT) {
                if (selectedIndex_BULK_PAYMENT[index] == i) {
                    isChecked = true;
                    break;
                }
            }
            initial = temp[i].NickName.substring(0, 2).toUpperCase();
            if (isChecked === false) {
                temp[i].initial = initial;
                temp[i].lblTick = "";
                temp[i].btnSetting.text = "q";
            } else {
                temp[i].initial = "";
                temp[i].lblTick = "r";
                temp[i].btnSetting.text = "p";
            }
            temp[i].icon = {
                backgroundColor: kony.boj.getBackGroundColour(initial)
            };
            data.push(temp[i]);
        }
    }
    frmManagePayeeKA.managepayeesegment.setData([]);
    kony.print("search results ::" + JSON.stringify(data));
    frmManagePayeeKA.managepayeesegment.widgetDataMap = {
        payeename: "NickName",
        accountnumber: "BillingNumber",
        lblBillerType: "BillerCode",
        lblInitial: "initial",
        flxIcon1: "icon",
        lblTick: "lblTick",
        btnEditBillerDetails: "btnEditBillerDetails",
        verticalDivider: "verticalDivider"
    };
    if (data.length === 0) {
        frmManagePayeeKA.managepayeesegment.isVisible = false;
        frmManagePayeeKA.lblSearchResult.isVisible = true;
    } else {
        frmManagePayeeKA.managepayeesegment.isVisible = true;
        frmManagePayeeKA.lblSearchResult.isVisible = false;
        frmManagePayeeKA.managepayeesegment.setData(data);
    }
};
kony.boj.animateRowOnSwipe = function(widgetRef, gestureInfo, context) {
    //   alert(JSON.stringify(widgetRef));
    //   alert(JSON.stringify(gestureInfo.swipeDirection));
    try {
        if (gestureInfo.swipeDirection === 0) {
            var deviceInfo = kony.os.deviceInfo();
            if (deviceInfo.name !== "iPhone") {
                kony.boj.deleteFlagPayee = false;
                kony.boj.onManagePayeeClick(context.widgetInfo.selectedRowItems[0]);
            }
        } else if (gestureInfo.swipeDirection === 1) {
            //code for delete
            if (kony.boj.deleteFlagPayee === true) return;
            kony.boj.deleteFlagPayee = true;
            kony.boj.selectedPayee = context.widgetInfo.selectedRowItems[0];
            animationdef = {
                100: {
                    left: "-21%"
                }
            };
            animDef = kony.ui.createAnimation(animationdef);
            animationConfig = {
                duration: 1,
                fillMode: kony.anim.FILL_MODE_FORWARDS,
                "iterationCount": 1
            };
            callBack = {
                animationStart: function() {},
                animationEnd: function() {}
            };
            widgetRef.animate(animDef, animationConfig, callBack);
        } else if (gestureInfo.swipeDirection === 2) {
            //code for hide delete
            kony.boj.deleteFlagPayee = false;
            kony.boj.selectedPayee = {};
            animationdef = {
                100: {
                    left: "1%"
                }
            };
            animDef = kony.ui.createAnimation(animationdef);
            animationConfig = {
                duration: 1,
                fillMode: kony.anim.FILL_MODE_FORWARDS,
                "iterationCount": 1
            };
            callBack = {
                animationStart: function() {},
                animationEnd: function() {}
            };
            widgetRef.animate(animDef, animationConfig, callBack);
        }
    } catch (e) {
        kony.print("Error in Bill Swipe:" + JSON.stringify(e));
    }
};
kony.boj.animateRowOnSwipeAr = function(widgetRef, gestureInfo, context) {
    //   alert(JSON.stringify(widgetRef));
    //   alert(JSON.stringify(gestureInfo.swipeDirection));
    try {
        if (gestureInfo.swipeDirection === 0) {
            //       var deviceInfo = kony.os.deviceInfo();
            //       if(deviceInfo.name !== "iPhone"){
            //         kony.boj.deleteFlagPayee = false;
            //         kony.boj.onManagePayeeClick(context.widgetInfo.selectedRowItems[0]);
            //       }
        } else if (gestureInfo.swipeDirection === 1) {
            //code for hide delete
            kony.boj.deleteFlagPayee = false;
            kony.boj.selectedPayee = {};
            animationdef = {
                100: {
                    right: "1%"
                }
            };
            animDef = kony.ui.createAnimation(animationdef);
            animationConfig = {
                duration: 1,
                fillMode: kony.anim.FILL_MODE_FORWARDS,
                "iterationCount": 1
            };
            callBack = {
                animationStart: function() {},
                animationEnd: function() {}
            };
            widgetRef.animate(animDef, animationConfig, callBack);
        } else if (gestureInfo.swipeDirection === 2) {
            //code for delete
            if (kony.boj.deleteFlagPayee === true) return;
            kony.boj.deleteFlagPayee = true;
            kony.boj.selectedPayee = context.widgetInfo.selectedRowItems[0];
            animationdef = {
                100: {
                    right: "-30%"
                }
            };
            animDef = kony.ui.createAnimation(animationdef);
            animationConfig = {
                duration: 1,
                fillMode: kony.anim.FILL_MODE_FORWARDS,
                "iterationCount": 1
            };
            callBack = {
                animationStart: function() {},
                animationEnd: function() {}
            };
            widgetRef.animate(animDef, animationConfig, callBack);
        }
    } catch (err) {
        kony.print("Error in Bill Swipe:" + JSON.stringify(err));
    }
};
kony.boj.managePayeeOnTap = function(widgetRef, context) {
    try {
        //alert("on tap recognized ::"+JSON.stringify(context));
        kony.boj.deleteFlagPayee = false;
        kony.boj.onManagePayeeClick(context.widgetInfo.selectedRowItems[0]);
    } catch (e) {
        kony.print("Exception_managePayeeOnTap ::" + e);
    }
}
kony.boj.deleteBillerFromSegment = function(eventobject, context) {
    if (context.widgetInfo.selectedRowItems[0].category === "prepaid") { // hassan prepaid
        gblToOTPFrom = "DeleteBiller";
        customAlertPopup(geti18nkey("i18n.common.DeleteBill"), geti18nkey("i18n.common.deleteBillText"), function() {
            kony.boj.selectedPayee = context.widgetInfo.selectedRowItems[0];
            serv_addNewPrePaidBene();
        }, function() {
            kony.boj.selectedPayee = {};
            popupCommonAlertDimiss()
        }, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
    } else customAlertPopup(geti18nkey("i18n.common.DeleteBill"), geti18nkey("i18n.common.deleteBillText"), function() {
        kony.boj.selectedPayee = context.widgetInfo.selectedRowItems[0];
        bojDeleteBillFromSegment();
    }, function() {
        kony.boj.selectedPayee = {};
        popupCommonAlertDimiss()
    }, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
};

function bojDeleteBillFromSegment() {
    kony.boj.deletePayeeFrom = "ManagePayee";
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmPayeeDetailsKA");
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
    navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navigationObject.setRequestOptions("form", {
        "headers": {}
    });
    kony.boj.preshowPayeeDetails();
    gblToOTPFrom = "DeleteBiller";
    ShowLoadingScreen();
    deleteBillerCustomVerbCall();
    //listController.performAction("deleteData");
}

function deleteBillerCustomVerbCall() {
    kony.print("deleteBillerCustomVerbCall:");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Payee", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Payee");
    dataObject.addField("custId", custid);
    dataObject.addField("lang", (kony.store.getItem("langPrefObj") === "en") ? "eng" : "ara");
    dataObject.addField("p_txn_type", "R");
    dataObject.addField("p_BillerCode", kony.boj.selectedPayee.BillerCode);
    dataObject.addField("p_ServiceType", kony.boj.selectedPayee.ServiceType);
    dataObject.addField("p_BillingNo", kony.boj.selectedPayee.BillingNumber);
    dataObject.addField("p_NickName", kony.boj.selectedPayee.NickName);
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    kony.print("Delete biller Params:" + JSON.stringify(dataObject))
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("delete", serviceOptions, success_deleteBillerDetails, failure_DELETEBILLERDETAILS);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function deleteBillerCustomVerbCall() {
    kony.print("deleteBillerCustomVerbCall:");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
        "access": "online",
        "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Payee", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Payee");
    dataObject.addField("custId", custid);
    dataObject.addField("lang", (kony.store.getItem("langPrefObj") === "en") ? "eng" : "ara");
    dataObject.addField("p_txn_type", "R");
    dataObject.addField("p_BillerCode", kony.boj.selectedPayee.BillerCode);
    dataObject.addField("p_ServiceType", kony.boj.selectedPayee.ServiceType);
    dataObject.addField("p_BillingNo", kony.boj.selectedPayee.BillingNumber);
    dataObject.addField("p_NickName", kony.boj.selectedPayee.NickName);
    var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
    };
    kony.print("Delete biller Params:" + JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("delete", serviceOptions, success_deleteBillerDetails, failure_DELETEBILLERDETAILS);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function success_deleteBillerDetails(res) {
    kony.print("success_deleteBillerDetails:" + JSON.stringify(res));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
    if (res.opstatus == "0") {
        customAlertPopup(geti18Value("i18n.common.BillDeleted"), geti18Value("i18n.bills.DeleteBill"), navToBillerListScreen, "");
        kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
    }
}

function navToBillerListScreen() {
    popupCommonAlert.dismiss();
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmManagePayeeKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("managepayeesegment", {
        "headers": {},
        "queryParams": {
            "custId": custid,
            "P_BENE_TYPE": "PRE"
        }
    });
    controller.loadDataAndShowForm(navObject);
}

function failure_DELETEBILLERDETAILS(err) {
    kony.print("failure_DELETEBILLERDETAILS:" + JSON.stringify(err));
    //Handle error case
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
}
kony.boj.getPreviousDateByMonth = function(date, months) {
    var result = new Date(date),
        expectedMonth = ((date.getMonth() + months) % 12 + 12) % 12;
    result.setMonth(result.getMonth() + months);
    if (result.getMonth() !== expectedMonth) {
        result.setDate(0);
    }
    return result;
};

function deletePayeeFromInside() {
    ShowLoadingScreen();
    kony.boj.deletePayeeFrom = "PayeeDetails";
    gblToOTPFrom = "DeleteBiller";
    INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    listController = INSTANCE.getFormController("frmPayeeDetailsKA");
    deleteBillerCustomVerbCall();
}