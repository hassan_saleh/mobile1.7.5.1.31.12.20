function AS_onTextChangeDebitCardNum(eventobject, changedtext) {
    return AS_TextField_adb3fcf860f4475fa9c9a8bf1ed73d68(eventobject, changedtext);
}

function AS_TextField_adb3fcf860f4475fa9c9a8bf1ed73d68(eventobject, changedtext) {
    onTextChangeScanCard();
    //onTextChangeCardNum();
    if (frmRegisterUser.txtCardNum.text.length == 16) {
        frmRegisterUser.flxLine.skin = "sknFlxGreenLine";
        if (kony.store.getItem("langPrefObj") === "en") {
            frmRegisterUser.lblCircle1.skin = "sknPINEmptyDark";
            frmRegisterUser.lblCircle1.text = "";
        } else {
            frmRegisterUser.lblCircle4.skin = "sknPINEmptyDark";
            frmRegisterUser.lblCircle4.text = "";
        }
        frmRegisterUser.txtPIN.setFocus(true);
    } else {
        frmRegisterUser.lblCircle1.skin = "sknPINEmpty";
        frmRegisterUser.lblCircle1.text = "s";
    }
    isNextEnabled();
    isClearButtonVisible(frmRegisterUser.txtCardNum, frmRegisterUser.lblClose);
    frmRegisterUser.lblInvalidCredentialsKA.setVisibility(false);
    if (frmRegisterUser.txtCardNum.text !== null && frmRegisterUser.txtPIN.text !== null && frmRegisterUser.txtMobileNumber.text !== null) {
        var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);
        if (cardNo.length == 16 && frmRegisterUser.txtPIN.text.length == 4) {
            frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
        }
    } else {
        frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
    }
}