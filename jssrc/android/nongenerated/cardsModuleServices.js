/***************************
 *
 *
 *
 *****************************/
function serv_INSTANTCASHBACKCONFIRM() {
    try {
        var redeem_PTS = frmInstantCash.txtFieldAmount.text;
        kony.print("redeem points ::" + redeem_PTS);
        var inputParams = {
            "custId": custid,
            "cardNum": kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num,
            "redeemPoints": redeem_PTS,
            "requestDate": new Date().getDate() + "/" + ((new Date().getMonth()) + 1) + "/" + new Date().getFullYear()
        };
        /*******LOGGER SERVICE CONFIGURATION****/
        logObj[0] = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
        logObj[6] = redeem_PTS;
        logObj[7] = inputParams.requestDate;
        logObj[13] = "INSTANT CASH";
        /******************************/
        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCcInstantCashback");
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            appMFConfiguration.invokeOperation("prCcInstantCashback", {}, inputParams, success_serv_INSTANTCASHBACKCONFIRM, function(err) {
                kony.print("failed ::" + JSON.stringify(err));
                logObj[16] = err;
                logObj[17] = "FAILED";
                logObj[18] = "ERROR IN SERVICE INSTANTCASHBACK";
                kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "Cards", "", "");
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            });
            loggerCall();
        } else {}
        //     	serv_CARDSCUSTOMVERB("CcInstantCashBack",servData);
    } catch (e) {
        kony.print("Exception_serv_CCINSTANTCASHBACK ::" + e);
    }
}

function serv_INTERNETANDMAILORDERLIMIT() {
    try {
        var servData = [];
        servData.push({
            "key": "p_card_no",
            "value": kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num
        });
        var limitStatus = null;
        if ((parseFloat(frmEnableInternetTransactionKA.txtEcomLimit.text) > 0)) {
            limitStatus = "A";
        } else {
            limitStatus = "D";
        }
        servData.push({
            "key": "p_internetaction",
            "value": "A"
        });
        if (limitStatus === "A") {
            servData.push({
                "key": "p_internetdailylimit",
                "value": "" + parseInt(frmEnableInternetTransactionKA.txtEcomLimit.text)
            });
        } else {
            servData.push({
                "key": "p_internetdailylimit",
                "value": 0
            });
        }
        limitStatus = null;
        if ((parseFloat(frmEnableInternetTransactionKA.txtMailLimit.text) > 0)) {
            limitStatus = "A";
        } else {
            limitStatus = "D";
        }
        servData.push({
            "key": "p_mailaction",
            "value": "A"
        });
        if (limitStatus === "A") {
            servData.push({
                "key": "p_maildailylimit",
                "value": "" + parseInt(frmEnableInternetTransactionKA.txtMailLimit.text)
            });
        } else {
            servData.push({
                "key": "p_maildailylimit",
                "value": 0
            });
        }
        if (frmEnableInternetTransactionKA.btnDaily.skin === "sknBtnBGWhiteBlue105Rd10") {
            servData.push({
                "key": "p_periodicity",
                "value": "4"
            });
            logObj[5] = "4";
        } else if (frmEnableInternetTransactionKA.btnWeekly.skin === "sknBtnBGWhiteBlue105Rd10") {
            servData.push({
                "key": "p_periodicity",
                "value": "3"
            });
            logObj[5] = "3";
        } else if (frmEnableInternetTransactionKA.btnMonthly.skin === "sknBtnBGWhiteBlue105Rd10") {
            servData.push({
                "key": "p_periodicity",
                "value": "5"
            });
            logObj[5] = "5";
        }
        /*******LOGGER SERVICE CONFIGURATION****/
        logObj[0] = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
        logObj[6] = frmEnableInternetTransactionKA.txtEcomLimit.text;
        logObj[7] = frmEnableInternetTransactionKA.txtMailLimit.text;
        logObj[13] = "UPDATE INTERNET MAILORDER LIMIT";
        /******************/
        serv_CARDSCUSTOMVERB("InternetMailorder", servData);
    } catch (e) {
        kony.print("Exception_serv_INTERNETANDMAILORDERLIMIT ::" + e);
    }
}

function serv_UPDATEATMPOSLIMIT() {
    try {
        var servData = [];
        servData.push({
            "key": "cardCode",
            "value": kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_code
        });
        if (frmATMPOSLimit.btnDaily.skin === "sknBtnBGWhiteBlue105Rd10") {
            servData.push({
                "key": "period",
                "value": "4"
            });
            logObj[5] = "4";
        } else if (frmATMPOSLimit.btnWeekly.skin === "sknBtnBGWhiteBlue105Rd10") {
            servData.push({
                "key": "period",
                "value": "3"
            });
            logObj[5] = "4";
        } else if (frmATMPOSLimit.btnMonthly.skin === "sknBtnBGWhiteBlue105Rd10") {
            servData.push({
                "key": "period",
                "value": "5"
            });
            logObj[5] = "4";
        }
        servData.push({
            "key": "atmlimit",
            "value": frmATMPOSLimit.txtWithdrawalLimit.text
        });
        servData.push({
            "key": "poclimit",
            "value": frmATMPOSLimit.txtPOSLimit.text
        });
        /*******LOGGER SERVICE CONFIGURATION****/
        logObj[0] = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
        logObj[1] = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_code;
        logObj[6] = frmATMPOSLimit.txtWithdrawalLimit.text;
        logObj[7] = frmATMPOSLimit.txtPOSLimit.text;
        logObj[13] = "UPDATE ATM/POS LIMIT";
        /******************/
        serv_CARDSCUSTOMVERB("updateATMPOSLimit", servData);
    } catch (e) {
        kony.print("Exception_serv_UPDATEATMPOSLIMIT ::" + e);
    }
}

function serv_UNBLOCKOIN() {
    try {
        var servData = [];
        servData.push({
            "key": "p_card_no",
            "value": kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num
        });
        logObj[0] = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
        logObj[13] = "UNBLOCK PIN";
        serv_CARDSCUSTOMVERB("ublockPin", servData);
    } catch (e) {
        kony.print("Exception_serv_UNBLOCKOIN ::" + e);
    }
}

function serv_APPLYCARDS() {
    try {
        var fields = kony.newCARD.requiredFields[kony.newCARD.cardTYPE];
        kony.print("Card Type ::" + kony.newCARD.cardTYPE + " :: fields :: " + fields);
        var servData = [],
            delMode = "",
            count = 0,
            notes = "";
        for (var i in fields) {
            if (kony.newCARD.fields[fields[i]][3] !== "" && fields[i] !== "Notes") {
                if (fields[i] === "AccountNumber" && (kony.newCARD.lastSelectionFlag === "NC" || kony.newCARD.lastSelectionFlag === "NDC")) {
                    servData.push({
                        "key": "p_acc_br",
                        "value": kony.store.getItem("frmAccount")[0].branchNumber
                    });
                    servData.push({
                        "key": kony.newCARD.fields[fields[i]][3],
                        "value": frmApplyNewCardsKA[kony.newCARD.fields[fields[i]][1]].text
                    });
                    logObj[count] = frmApplyNewCardsKA[kony.newCARD.fields[fields[i]][1]].text;
                    logObj[count + 1] = kony.store.getItem("frmAccount")[0].branchNumber;
                } else if (fields[i] === "CreditCardNumber" && kony.newCARD.maincardTYPE !== "Prepaid") {
                    if (kony.newCARD.lastSelectionFlag === "RDC" || kony.newCARD.lastSelectionFlag === "NSD" || kony.newCARD.lastSelectionFlag === "RPC" || kony.newCARD.lastSelectionFlag === "RWC") {
                        servData.push({
                            "key": "p_acc_br",
                            "value": ""
                        });
                        servData.push({
                            "key": "p_acc_no",
                            "value": mask_CreditCardNumber(kony.store.getItem("frmAccount").card_num)
                        });
                    } else {
                        servData.push({
                            "key": kony.newCARD.fields[fields[i]][3],
                            "value": mask_CreditCardNumber(kony.store.getItem("frmAccount").card_num)
                        });
                    }
                    logObj[count] = mask_CreditCardNumber(kony.store.getItem("frmAccount").card_num);
                } else if (fields[i] === "DeliveryMode") {
                    delMode = (frmApplyNewCardsKA.btnDeliveryModeBranch.text === "t") ? frmApplyNewCardsKA.lblBranchName.text : frmApplyNewCardsKA.txtAddress.text;
                    logObj[count] = delMode;
                    servData.push({
                        "key": kony.newCARD.fields[fields[i]][3],
                        "value": delMode
                    });
                } else if (fields[i] !== "Branch") {
                    servData.push({
                        "key": kony.newCARD.fields[fields[i]][3],
                        "value": frmApplyNewCardsKA[kony.newCARD.fields[fields[i]][1]].text
                    });
                    logObj[count] = frmApplyNewCardsKA[kony.newCARD.fields[fields[i]][1]].text;
                }
                count = count + 1;
            } else if (fields[i] === "Notes") {
                notes = notes + frmApplyNewCardsKA.txtNotes.text + ";" + kony.newCARD.cardTYPE + ";";
            }
        }
        logObj[13] = "APPLY CARDS";
        servData.push({
            "key": "Flag",
            "value": kony.newCARD.lastSelectionFlag
        });
        for (var i in servData) {
            if (servData[i].key !== "p_notes") notes = notes + servData[i].key + "=" + servData[i].value + ";";
        }
        servData.push({
            "key": "p_notes",
            "value": notes
        });
        serv_CARDSCUSTOMVERB("ApplyCards", servData);
    } catch (e) {
        kony.print("Exception_serv_APPLYCARDS ::" + e);
    }
}

function serv_GET_DEBIT_CARD_LINKED_ACCOUNTS(cardData) {
    try {
        serv_CARDSCUSTOMVERB("dcLinkedAccounts", [{
            "key": "cardCode",
            "value": cardData.card_code
        }, {
            "key": "cardNum",
            "value": cardData.card_num
        }]);
    } catch (e) {
        kony.print("Exception_serv_GET_DEBIT_CARD_LINKED_ACCOUNTS ::" + e);
    }
}

function serv_CARDSCUSTOMVERB(operationName, fields) {
    try {
        kony.print("service fields ::" + JSON.stringify(fields));
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Cards", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Cards");
        dataObject.addField("custId", custid);
        for (var i in fields) {
            dataObject.addField(fields[i].key, fields[i].value);
        }
        dataObject.addField("p_user_id", "BOJMOB");
        dataObject.addField("p_channel", "MOBILE");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("service input paramerters ::" + JSON.stringify(dataObject));
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb(operationName, serviceOptions, function(res) {
                kony.print("Success ::" + JSON.stringify(res));
                if (operationName === "InternetMailorder") {
                    success_serv_INTERNETANDMAILORDERLIMIT(res);
                } else if (operationName === "CcInstantCashBack") {
                    success_serv_INSTANTCASHBACKCONFIRM(res);
                } else if (operationName === "ApplyCards") {
                    success_serv_APPLYCARDS(res);
                } else if (operationName === "ublockPin") {
                    success_serv_UNBLOCKPIN(res);
                } else if (operationName === "updateATMPOSLimit") {
                    success_serv_UPDATEATMPOSLIMIT(res);
                } else if (operationName === "dcLinkedAccounts") {
                    success_serv_GET_DEBIT_CARD_LINKED_ACCOUNTS(res);
                } else if (operationName === "pinRemainder") {
                    success_serv_GET_CARD_PIN(res);
                }
            }, function(err) {
                kony.print("failed ::" + JSON.stringify(err));
                if (operationName === "InternetMailorder") {
                    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
                } else {
                    kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "CardsLanding", "", "");
                }
                logObj[16] = err;
                logObj[17] = "FAILED";
                logObj[18] = "ERROR IN SERVICE EXECUTION";
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                loggerCall();
            });
        }
    } catch (e) {
        kony.print("Exception_serv_CARDSCUSTOMVERB ::" + e);
    }
}

function success_serv_INSTANTCASHBACKCONFIRM(res) {
    try {
        kony.print("success instant cash ::" + JSON.stringify(res));
        if ((res.opstatus !== undefined && res.opstatus !== null) && (res.opstatus === 0 || res.opstatus === "0")) {
            if (res.status === "00000" && (res.res_code === "30" || res.res_code === "93")) {
                logObj[16] = res.res_code;
                logObj[17] = "FAILED";
                logObj[18] = res.res_msg;
                kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), res.res_msg, geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "Cards", "", "");
            } else if (res.status === "00000" && res.res_code === "00") {
                //             	is_CARDPAYMENTSUCCESS = true;
                logObj[16] = res.res_code;
                logObj[17] = "SUCCESS";
                logObj[18] = "REDEEMED POINTS SUCCESSFULLY";
                update_CARDDATA("instantCash");
                kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.instantcash.successmsg"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "Cards", "", "");
            } else if (res.res_code == "68" || res.res_code == 68 || res.res_code == "89" || res.res_code == 89) {
                logObj[16] = res.res_code;
                logObj[17] = "FAILED";
                logObj[18] = geti18Value("i18n.instantcash.maxredemptionpoint");
                kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.instantcash.maxredemptionpoint"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "Cards", "", "");
            } else {
                logObj[16] = res.res_code;
                logObj[17] = "FAILED";
                logObj[18] = res.res_msg;
                kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), res.res_msg, geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "Cards", "", "");
            }
        }
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        loggerCall();
    } catch (e) {
        logObj[16] = e;
        logObj[17] = "FAILED";
        logObj[18] = "Exception INSTANT CASH REDEMPTION";
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        loggerCall();
        kony.print("Exception_serv_INSTANTCASHBACKCONFIRM ::" + e);
    }
}

function success_serv_INTERNETANDMAILORDERLIMIT(res) {
    try {
        if ((res.opstatus !== undefined && res.opstatus !== null) && (res.opstatus === 0 || res.opstatus === "0")) {
            if (res.ErrorCode === "00000") {
                is_CARDPAYMENTSUCCESS = true;
                update_CARDDATA("ecommerce");
                logObj[16] = res.ErrorCode;
                logObj[17] = "SUCCESS";
                logObj[18] = geti18Value("i18n.ecommerce.success");
                customAlertPopup(geti18Value("i18n.common.success"), geti18Value("i18n.ecommerce.success"), popupCommonAlertDimiss, "");
            } else {
                logObj[16] = res.ErrorCode;
                logObj[17] = "FAILED";
                logObj[18] = geti18Value("i18n.ecommerce.failed");
                customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
            }
        } else {
            logObj[16] = res.ErrorCode;
            logObj[17] = "FAILED";
            logObj[18] = geti18Value("i18n.ecommerce.failed");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.ecommerce.failed"), popupCommonAlertDimiss, "");
        }
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        loggerCall();
    } catch (e) {
        logObj[16] = e;
        logObj[17] = "FAILED";
        logObj[18] = "Exception INTERNET MAILORDER LIMIT UPDATE";
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        loggerCall();
        kony.print("success_serv_INTERNETANDMAILORDERLIMIT ::" + e);
    }
}

function success_serv_APPLYCARDS(res) {
    try {
        var errMsg = "";
        if ((res.opstatus !== undefined && res.opstatus !== null) && (res.opstatus === 0 || res.opstatus === "0")) {
            if (res.ErrorCode === "00000") {
                var msg = geti18Value("i18n.applycards.submitted") + " " + res.ref_no + ". " + geti18Value("i18n.applycards.remarks");
                if (kony.newCARD.cardTYPE === "ReplaceDebitCards") {
                    msg = geti18Value("i18n.applycards.submitted") + " " + res.ref_no + ". " + geti18Value("i18n.replacecards.blockmessage") + " " + geti18Value("i18n.applycards.remarks");;
                }
                kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), msg, geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "CardsLanding", "", "");
                logObj[16] = res.ref_no;
                logObj[17] = "SUCCESS";
                logObj[18] = kony.newCARD.maincardTYPE + "-" + kony.newCARD.cardTYPE + " Requested Successful";
            } else {
                if (res.ErrorCode === "1019") errMsg = geti18Value("i18n.requestNewPin.already");
                logObj[16] = res.ErrorCode;
                logObj[17] = "FAILED";
                logObj[18] = kony.newCARD.maincardTYPE + "-" + kony.newCARD.cardTYPE + " " + errMsg;
                kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), errMsg, geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "CardsLanding", "", "");
            }
        } else {
            logObj[16] = res.ErrorCode;
            logObj[17] = "FAILED";
            logObj[18] = kony.newCARD.maincardTYPE + "-" + kony.newCARD.cardTYPE + " Request Failed";
            kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18nkey("i18n.cards.gotoCards"), "CardsLanding", "", "");
        }
        frmApplyNewCardsKA.destroy();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        loggerCall();
    } catch (e) {
        logObj[16] = e;
        logObj[17] = "FAILED";
        logObj[18] = kony.newCARD.maincardTYPE + "-" + kony.newCARD.cardTYPE + " Exception";
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_success_serv_APPLYCARDS ::" + e);
        loggerCall();
        kony.newCARD.maincardTYPE = "";
    }
    kony.newCARD.maincardTYPE = "";
}

function success_serv_UNBLOCKPIN(res) {
    try {
        if ((res.opstatus !== undefined && res.opstatus !== null) && (res.opstatus === 0 || res.opstatus === "0")) {
            if (res.result === "00000") {
                logObj[17] = "SUCCESS";
                logObj[18] = kony.i18n.getLocalizedString("i18n.errorMsg.unblockccpin");
                customAlertPopup(geti18Value("i18n.common.success"), kony.i18n.getLocalizedString("i18n.errorMsg.unblockccpin"), popupCommonAlertDimiss, "");
            } else {
                var Message = getErrorMessage(res.result);
                kony.print("res.result " + res.result);
                kony.print("Message " + Message);
                logObj[16] = res.result;
                logObj[17] = "FAILED";
                logObj[18] = Message;
                customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
            }
        }
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        loggerCall();
    } catch (e) {
        logObj[16] = e;
        logObj[17] = "FAILED";
        logObj[18] = "Exception UNBLOCK PIN";
        kony.print("success_serv_UNBLOCKPIN ::" + e);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        loggerCall();
    }
}

function success_serv_UPDATEATMPOSLIMIT(res) {
    try {
        var msg = "Failed to update.",
            title = geti18Value("i18n.common.success");
        if (res.res_code_pos === "00000" && res.res_code_atm === "00000") {
            msg = geti18Value("i18n.ecommerce.success");
            update_CARDDATA("ATMPOS");
            logObj[16] = res.res_code_pos;
            logObj[17] = "SUCCESS";
        } else if (res.res_code_pos === "00000" && res.res_code_atm !== "00000") {
            msg = geti18Value("i18n.atmpos.possuccessmsg");
            update_CARDDATA("POS");
            logObj[16] = res.res_code_atm;
            logObj[17] = "FAILED";
            title = geti18Value("i18n.Bene.Failed");
        } else if (res.res_code_pos !== "00000" && res.res_code_atm === "00000") {
            msg = geti18Value("i18n.atmpos.atmsuccessmsg");
            update_CARDDATA("ATM");
            logObj[16] = res.res_code_pos;
            logObj[17] = "FAILED";
            title = geti18Value("i18n.Bene.Failed");
        } else if (res.res_code_pos === "00116" && res.res_code_atm === "00116") {
            msg = geti18Value("i18n.ecommerce.failed");
            logObj[16] = res.res_code_pos + " " + res.res_code_atm;
            logObj[17] = "FAILED";
            title = geti18Value("i18n.Bene.Failed");
        }
        logObj[18] = msg;
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(title, msg, popupCommonAlertDimiss, null, geti18Value("i18n.NUO.OKay"), "");
        loggerCall();
    } catch (e) {
        logObj[16] = e;
        logObj[17] = "FAILED";
        logObj[18] = "Exception UPDATE ATM/POS LIMIT";
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_success_serv_UPDATEATMPOSLIMIT ::" + e);
        loggerCall();
    }
}

function success_serv_GET_DEBIT_CARD_LINKED_ACCOUNTS(res) {
    try {
        if (res.ErrorCode === "00000" && !isEmpty(res.LinkedAccounts)) {
            if (!isEmpty(kony.retailBanking.globalData.accountsDashboardData.accountsData)) process_LINKED_ACCOUNTS_DATA(kony.retailBanking.globalData.accountsDashboardData.accountsData, res.LinkedAccounts);
            else customAlertPopup(geti18Value("i18n.common.Attention"), geti18Value("i18n.alerts.NoAccountsMsg"), popupCommonAlertDimiss, "");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            var msg = getErrorMessage(res.ErrorCode);
            customAlertPopup(geti18Value("i18n.maps.Info"), msg, popupCommonAlertDimiss, null, geti18Value("i18n.NUO.OKay"), "");
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_success_serv_GET_DEBIT_CARD_LINKED_ACCOUNTS ::" + e);
    }
}

function serv_CARD_LINK_ACCOUNT() {
    try {
        serv_DATA_CARD_LINK = {
            "link": null,
            "unlink": null,
            "defaultAccount": null
        };
        card_LINK_ACCOUNT_SERV = false;
        card_UNLINK_ACCOUNT_SERV = false;
        card_DEFAULT_ACCOUNT_SERV = false;
        var servData = get_ACCOUNT_LINK_STATUS(frmCardLinkedAccounts.segCardLinkedAccounts.data);
        kony.print("servData ::" + JSON.stringify(servData));
        var card_num = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_num;
        var card_code = kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_code;
        var link_servData = {
            "custId": "",
            "cardCode": "",
            "cardNum": "",
            "newBrch": "",
            "newAcc": "",
            "p_channel": "",
            "p_user_id": "",
            "loop_count": "",
            "loop_seperator": ":"
        };
        var unlink_servData = {
            "custId": "",
            "cardCode": "",
            "cardNum": "",
            "accBrch": "",
            "accNum": "",
            "p_channel": "",
            "p_user_id": "",
            "loop_count": "",
            "loop_seperator": ":"
        };
        var defaultAcc_servData = null;
        if (servData[0].length > 0) {
            for (var i in servData[0]) {
                link_servData.custId = ((i + 1) == servData[0].length) ? link_servData.custId + custid : link_servData.custId + custid + ":";
                link_servData.cardCode = ((i + 1) == servData[0].length) ? link_servData.cardCode + card_code : link_servData.cardCode + card_code + ":";
                link_servData.cardNum = ((i + 1) == servData[0].length) ? link_servData.cardNum + card_num : link_servData.cardNum + card_num + ":";
                link_servData.newBrch = ((i + 1) == servData[0].length) ? link_servData.newBrch + servData[0][i].branchNumber : link_servData.newBrch + servData[0][i].branchNumber + ":";
                link_servData.newAcc = ((i + 1) == servData[0].length) ? link_servData.newAcc + servData[0][i].lblAccountNumber.text : link_servData.newAcc + servData[0][i].lblAccountNumber.text + ":";
                link_servData.p_channel = ((i + 1) == servData[0].length) ? link_servData.p_channel + "MOBILE" : link_servData.p_channel + "MOBILE:";
                link_servData.p_user_id = ((i + 1) == servData[0].length) ? link_servData.p_user_id + "BOJMOB" : link_servData.p_user_id + "BOJMOB:";
            }
            link_servData.loop_count = servData[0].length;
        }
        if (servData[1].length > 0) {
            for (var i in servData[1]) {
                unlink_servData.custId = ((i + 1) == servData[1].length) ? unlink_servData.custId + custid : unlink_servData.custId + custid + ":";
                unlink_servData.cardCode = ((i + 1) == servData[1].length) ? unlink_servData.cardCode + card_code : unlink_servData.cardCode + card_code + ":";
                unlink_servData.cardNum = ((i + 1) == servData[1].length) ? unlink_servData.cardNum + card_num : unlink_servData.cardNum + card_num + ":";
                unlink_servData.accBrch = ((i + 1) == servData[1].length) ? unlink_servData.accBrch + servData[1][i].branchNumber : unlink_servData.accBrch + servData[1][i].branchNumber + ":";
                unlink_servData.accNum = ((i + 1) == servData[1].length) ? unlink_servData.accNum + servData[1][i].lblAccountNumber.text : unlink_servData.accNum + servData[1][i].lblAccountNumber.text + ":";
                unlink_servData.p_channel = ((i + 1) == servData[1].length) ? unlink_servData.p_channel + "MOBILE" : unlink_servData.p_channel + "MOBILE:";
                unlink_servData.p_user_id = ((i + 1) == servData[1].length) ? unlink_servData.p_user_id + "BOJMOB" : unlink_servData.p_user_id + "BOJMOB:";
            }
            unlink_servData.loop_count = servData[1].length;
        }
        if (servData[2].length > 0) {
            defaultAcc_servData = {
                "custId": custid,
                "cardCode": card_code,
                "cardNum": card_num,
                "newBrch": servData[2][0].branchNumber,
                "newAcc": servData[2][0].lblAccountNumber.text,
                "p_channel": "MOBILE",
                "p_user_id": "BOJMOB"
            };
        }
        kony.print("Linked Account ServData ::" + JSON.stringify(link_servData));
        kony.print("UnLinked Account ServData ::" + JSON.stringify(unlink_servData));
        kony.print("defaultAcc_servData Account ServData ::" + JSON.stringify(defaultAcc_servData));
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        if (link_servData.custId !== "") {
            kony.print("link accounts service execution");
            card_LINK_ACCOUNT_SERV = true;
            var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("loopCardLinkAccount");
            appMFConfiguration.invokeOperation("LinkAccountDebitCard", {}, link_servData, success_serv_CARD_LINK_ACCOUNT, function(err) {
                kony.print("response err ::" + JSON.stringify(err));
            });
        }
        if (unlink_servData.custId !== "") {
            kony.print("Unlink accounts service execution");
            card_UNLINK_ACCOUNT_SERV = true;
            var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("loopCardUnLinkAccount");
            appMFConfiguration.invokeOperation("UnLinkAccountsDebitCard", {}, unlink_servData, success_serv_CARD_UNLINK_ACCOUNT, function(err) {
                kony.print("response err ::" + JSON.stringify(err));
            });
        }
        if (defaultAcc_servData !== {} && defaultAcc_servData !== null) {
            kony.print("default accounts service execution");
            card_DEFAULT_ACCOUNT_SERV = true;
            var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJDcChangeDefaultAcc");
            appMFConfiguration.invokeOperation("prDcChangeDefaultAcc", {}, defaultAcc_servData, success_serv_CARD_DEFAULT_ACCOUNT, function(err) {
                kony.print("response err ::" + JSON.stringify(err));
            });
        }
        if (!card_LINK_ACCOUNT_SERV && !card_UNLINK_ACCOUNT_SERV && !card_DEFAULT_ACCOUNT_SERV) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_serv_CARD_LINK_ACCOUNT ::" + e);
    }
}

function success_serv_CARD_LINK_ACCOUNT(res) {
    try {
        kony.print("card link response ::" + JSON.stringify(res));
        //     	if(res.errorCode === "00000"){
        card_LINK_ACCOUNT_SERV = false;
        if (!isEmpty(res.LoopDataset)) data_CARD_LINK_SUCCESS_SCREEN(res.LoopDataset, "link");
        else data_CARD_LINK_SUCCESS_SCREEN(null, "link");
        if (!card_LINK_ACCOUNT_SERV && !card_UNLINK_ACCOUNT_SERV && !card_DEFAULT_ACCOUNT_SERV) {
            kony.print("serv_DATA_CARD_LINK ::" + JSON.stringify(serv_DATA_CARD_LINK));
            //navigate_CARD_LINK_ACCOUNT_CONFIRMATION(); // hassan linked account success screen
            is_CARDPAYMENTSUCCESS = true;
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.debitcardlinkedacc.linkedacc"), geti18Value("i18n.linked.success"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.cards.gotoCards"), "Cards", "", "", "" + "" + ""); // hassan linked account success screen
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); // hassan linked account success screen
        }
        //         }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Excpetion_success_serv_CARD_LINK_ACCOUNT ::" + e);
    }
}

function success_serv_CARD_UNLINK_ACCOUNT(res) {
    try {
        kony.print("card Unlink response ::" + JSON.stringify(res));
        //     	if(res.errorCode === "00000"){
        card_UNLINK_ACCOUNT_SERV = false;
        if (!isEmpty(res.LoopDataset)) data_CARD_LINK_SUCCESS_SCREEN(res.LoopDataset, "unlink");
        else data_CARD_LINK_SUCCESS_SCREEN(null, "unlink");
        if (!card_LINK_ACCOUNT_SERV && !card_UNLINK_ACCOUNT_SERV && !card_DEFAULT_ACCOUNT_SERV) {
            kony.print("serv_DATA_CARD_LINK ::" + JSON.stringify(serv_DATA_CARD_LINK));
            //navigate_CARD_LINK_ACCOUNT_CONFIRMATION(); // hassan linked account success screen
            is_CARDPAYMENTSUCCESS = true;
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.debitcardlinkedacc.linkedacc"), geti18Value("i18n.linked.success"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.cards.gotoCards"), "Cards", "", "", "" + "" + ""); // hassan linked account success screen
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); // hassan linked account success screen
        }
        //         }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Excpetion_success_serv_CARD_UNLINK_ACCOUNT ::" + e);
    }
}

function success_serv_CARD_DEFAULT_ACCOUNT(res) {
    try {
        kony.print("Default response ::" + JSON.stringify(res));
        //     	if(res.errorCode === "00000"){
        card_DEFAULT_ACCOUNT_SERV = false;
        if (res.errorCode === "00000") data_CARD_LINK_SUCCESS_SCREEN(res.errorCode, "defaultAccount");
        else data_CARD_LINK_SUCCESS_SCREEN(null, "defaultAccount");
        if (!card_LINK_ACCOUNT_SERV && !card_UNLINK_ACCOUNT_SERV && !card_DEFAULT_ACCOUNT_SERV) {
            kony.print("serv_DATA_CARD_LINK ::" + JSON.stringify(serv_DATA_CARD_LINK));
            navigate_CARD_LINK_ACCOUNT_CONFIRMATION();
        }
        //         }
    } catch (e) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception_success_serv_CARD_DEFAULT_ACCOUNT ::" + e);
    }
}