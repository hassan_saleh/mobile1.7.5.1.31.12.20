function afwanNavigatetoJoMopayInitiate(eventobject) {
    return AS_Button_d1bd134b24a44e1c96ab9b12d66c00cf(eventobject);
}

function AS_Button_d1bd134b24a44e1c96ab9b12d66c00cf(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        frmJoMoPay.show();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}