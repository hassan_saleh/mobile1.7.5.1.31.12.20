function animateRegisterConfrmPasswordLabelDown(eventobject, changedtext) {
    return AS_TextField_d73fbb049c8848d184432e4d2c245e79(eventobject, changedtext);
}

function AS_TextField_d73fbb049c8848d184432e4d2c245e79(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidPassword(frmEnrolluserLandingKA.tbxPasswordKA.text) && validateConfirmPassword(frmEnrolluserLandingKA.tbxPasswordKA.text, frmEnrolluserLandingKA.tbxConfrmPwdKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxGreenLine";
        confrmPasswordFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxOrangeLine";
        confrmPasswordFlag = false;
    }
    if (kony.sdk.isNetworkAvailable()) {
        if (frmEnrolluserLandingKA.lblNext.skin == "sknLblNextEnabled" && gblFromModule !== "ChangePassword" && gblFromModule !== "ChangeUsername") {
            ShowLoadingScreen();
            callSave();
        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
    animateLabel("DOWN", "lblConfirmPassword", frmEnrolluserLandingKA.tbxConfrmPwdKA.text);
}