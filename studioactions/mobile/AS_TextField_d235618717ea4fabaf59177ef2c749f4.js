function AS_TextField_d235618717ea4fabaf59177ef2c749f4(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    animateLabel("DOWN", "lblUsername", frmEnrolluserLandingKA.tbxUsernameKA.text);
}