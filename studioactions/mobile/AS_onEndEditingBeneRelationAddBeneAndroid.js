function AS_onEndEditingBeneRelationAddBeneAndroid(eventobject, changedtext) {
    return AS_TextField_b87316127ce746d2b963172eb694dc2a(eventobject, changedtext);
}

function AS_TextField_b87316127ce746d2b963172eb694dc2a(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxBeneRelation.text !== "") {
        frmAddExternalAccountKA.flxUnderlineBeneRelation.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineBeneRelation.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblBeneRelation", frmAddExternalAccountKA.tbxBeneRelation.text);
}