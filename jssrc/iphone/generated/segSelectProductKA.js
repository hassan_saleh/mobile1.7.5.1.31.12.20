function initializesegSelectProductKA() {
    flxSegMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "120dp",
        "id": "flxSegMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxSegMain.setDefaultUnit(kony.flex.DP);
    var flxSegMain2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSegMain2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkgGrayf7f7f7",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSegMain2.setDefaultUnit(kony.flex.DP);
    var lblAccName = new kony.ui.Label({
        "id": "lblAccName",
        "isVisible": true,
        "left": "2%",
        "skin": "sknonboardingHeader107",
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblData1 = new kony.ui.Label({
        "id": "lblData1",
        "isVisible": true,
        "left": "2%",
        "skin": "sknRegularFormlblfornuo",
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblData2 = new kony.ui.Label({
        "id": "lblData2",
        "isVisible": true,
        "left": "2%",
        "skin": "sknRegularFormlblfornuo1",
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnViewDetailsKA = new kony.ui.Button({
        "focusSkin": "sknSecondaryActionWithBorderFocus90KA",
        "height": "20%",
        "id": "btnViewDetailsKA",
        "isVisible": true,
        "left": "2%",
        "skin": "sknsecondaryAction1KA",
        "text": kony.i18n.getLocalizedString("i18n.NUO.ViewDetails"),
        "top": "66%",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxSegMain2.add(lblAccName, lblData1, lblData2, btnViewDetailsKA);
    var flxDivider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "5dp",
        "id": "flxDivider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkg",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDivider1.setDefaultUnit(kony.flex.DP);
    flxDivider1.add();
    var imgUntick = new kony.ui.Image2({
        "centerY": "50%",
        "height": "35dp",
        "id": "imgUntick",
        "isVisible": true,
        "left": "85%",
        "skin": "slImage",
        "src": "inactive1.png",
        "width": "35dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSegMain.add(flxSegMain2, flxDivider1, imgUntick);
}