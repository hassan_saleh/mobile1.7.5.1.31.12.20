function AS_TextField_h56fcccb056144b4b1d494add2b2cf3b(eventobject, changedtext) {
    try {
        if (frmEstatementLandingKA.lblEmail2.top !== "15%") {
            animateLabel("UP", "lblEmail2", frmEstatementLandingKA.txtEmail2.text);
        }
        if (frmEstatementLandingKA.txtEmail1.text !== null && frmEstatementLandingKA.txtEmail1.text !== "" && isValidEmaill(frmEstatementLandingKA.txtEmail1.text) && isValidEmaill(frmEstatementLandingKA.txtEmail2.text)) {
            if (frmEstatementLandingKA.flxSwitchOnTouchLogin.isVisible && frmEstatementLandingKA.lblTermsandConditionsCheckBox.text === "p") {
                frmEstatementLandingKA.lblNext.skin = "sknLblNextEnabled";
            } else {
                frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
            }
        } else {
            frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        }
        frmEstatementLandingKA.lblInvalidEmail2.setVisibility(false);
    } catch (err) {
        kony.print("err:" + err);
    }
}