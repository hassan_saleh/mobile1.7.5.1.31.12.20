function purposeList(subCategory){

  var arrList = [[
	["1","Personal"],[
	"1","شخصي"],[
	"2","Salaries and Wages"],[
	"2","رواتب و أجور"],[
	"3","Investment Remittances"],[
	"3","تحويلات الاستثمار"],[
	"4","Transportation and Tourism"],[
	"4","النقل و السياحة"],[
	"5","Training and Delegation"],[
	"5","التدريب و الابتعاث"],[
	"6","Import and Export"],[
	"6","الاستيراد والتصدير"],[
	"7","External Aid"],[
	"7","مساعدات خارجية"],[
	"8","Services"],[
	"8","خدمات"],[
	"9","Funding"],[
	"9","التمويل"],[
	"10","Diplomacy"],[
	"10","دبلوماسية"],[
	"11","Loans"],[
	"11","قروض"],[
	"12","General"],[
	"12","عام"]
],
[{
	"1":[[
        "101", "Invoice Payment & Purchase"],[
        "101", "تسديد فواتير ومشتريات"],[
        "102", "Utility Bill Payment"],[
        "102", "فواتير الخدمات"],[
        "103", "Prepaid Cards Recharging"],[
        "103", "تغذية البطاقات المدفوعة مسبقا"],[
        "104", "Standing Orders"],[
        "104", "أوامر الدفع الثابتة"],[
        "105", "Personal Donations"],[
        "105", "تبرعات الأفراد "],[
        "106", "Family Assistance and Expenses"],[
        "106", "نفقات ومساعدات عائلية"],[
        "107", "Individual Social Security Subs"],[
        "107", "اشتراك الأفراد الإختياري في الضمان"],[
        "108", "Associations Subscriptions"],[
        "108", "اشتراكات النقابات"],[
        "109", "Saving and Funding Account "],[
        "109", "التحويل لغايات توفير/ادخار/ تغذية الحساب"],[
        "110", "Heritance"],[
        "110", "ميراث"],[
        "111", "End of Service indemnity"],[
        "111", "مكافآة نهاية الخدمة"],[
        "112", "Trust"],[
        "112", "امانات/التحويلات لغايات حجزالاموال"],[
        "0114", "Financial Aid"],[
        "0114", "معونة مالية"]
	],
	"2":[[
        "201", "Public Sector Employees Salaries"],[
        "201", "اجور/رواتب/مياومات/مكافئات موظفي الدولة"],[
        "202", "Laborers Salaries"],[
        "202", "اجور/رواتب/مياومات/مكافئات عاملين الدولة"],[
        "203", "Private Sector Staff Salaries "],[
        "203", "أجور و رواتب القطاع الخاص"],[
        "204", "Jordanian Diplomatic Staff Salaries"],[
        "204", "أجور ورواتب الهيئات الدبلوماسية الاردنية"],[
        "205", "Foreign Diplomatic Salaries"],[
        "205", "أجور ورواتب الهيئات الدبلوماسية الاجنبية"],[
        "206", "Overseas Incoming Salaries  "],[
        "206", "رواتب واردة من الخارج"],[
        "207", "Civil/Military Retirement Salaries"],[
        "207", "رواتب تقاعدية مدنية وعسكرية"],[
        "208", "Social Security Retirement Salaries"],[
        "208", "رواتب تقاعدية ضمان اجتماعي"],[
        "209", "Establishment Social Security Subs"],[
        "209", "اشتراكات المؤسسات في الضمان الاجتماعي "]
	],
	"3":[[
        "301", "Investment Revenues "],[
        "301", "عوائد الاستثمار"],[
        "302", "Brokrage Investment"],[
        "302", "تحويلات المتاجرة بالأسواق العالمية"],[
        "303", "Insurance "],[
        "303", "حوالات التامين "],[
        "304", "Subs to inter nonmonetary org"],[
        "304", "مساهمات في رأس مال المؤسسات الدولية"],[
        "305", "Local Investment"],[
        "305", "تحويلات إستثمارية - محلية"],[
        "306", "External Investment"],[
        "306", "تحويلات إستثمارية - خارجية"],[
        "307", "Tender bond Guarantee"],[
        "307", "حوالة شراء دعوة عطاء"]
	],
	"4":[[
        "401", "Air Freight"],[
        "401", "النقل والشحن الجوي"],[
        "402", "Land Freight"],[
        "402", "النقل والشحن البري"],[
        "403", "Sea Freight"],[
        "403", "النقل والشحن البحري"],[
        "404", "Travel and Tourism"],[
        "404", "السياحة والسفر"]
	],
	"5":[[
        "501", "Governmental Delegation Transfers"],[
        "501", "حوالات الإبتعاث الحكومية "],[
        "502", "Private Sector Delegation Transfers"],[
        "502", "حوالات إبتعاث القطاع الخاص "],[
        "503", "Governmental Education"],[
        "503", "التعليم الحكومي"],[
        "504", "Private Sector Education"],[
        "504", "تعليم القطاع الخاص"]
    ],
	"6":[[
        "601", "Public Sector Exportation"],[
        "601", "تصدير القطاع العام"],[
        "602", "Private Sector Exportation"],[
        "602", "تصدير القطاع الخاص"],[
        "603", "Public Sector Importation"],[
        "603", "استيراد القطاع العام"],[
        "604", "Private Sector Importation"],[
        "604", "استيراد القطاع الخاص"]
    ],
	"7":[[
        "701", "Religious Communities Aid"],[
        "701", "مساعدات هيئات دينية"],[
        "702", "International Communities Aid"],[
        "702", "مساعدات هيئات دولية"],[
        "703", "Arab Communities Aid"],[
        "703", "مساعدات عربية"],[
        "704", "UN Aid"],[
        "704", "مساعدات الأمم المتحدة"],[
        "705", "Charity Communities Aid"],[
        "705", "مساعدات الهيئات الخيرية"]
    ],
	"8":[[
        "801", "Telecommunication Services"],[
        "801", "خدمات إتصالات"],[
        "802", "Financial Services"],[
        "802", "خدمات مالية "],[
        "803", "Information Technology Services"],[
        "803", "خدمات تكنولوجيا المعلومات "],[
        "804", "Consulting Services"],[
        "804", "الخدمات الاستشارية"],[
        "805", "Construction Services"],[
        "805", "خدمات إنشائية"],[
        "806", "Maintenance & Assembling Services"],[
        "806", "خدمات الصيانة والتجميع"],[
        "807", "Marketing and Media Services"],[
        "807", "خدمات دعاية وتسويق"],[
        "808", "Mining Services"],[
        "808", "خدمات التعدين"],[
        "809", "Medical & Health Services"],[
        "809", "خدمات صحية / طبية "],[
        "810", "Cultural,Educ+Entertainment Serv"],[
        "810", "خدمات ثقافية وتعليمية وترفيهية"],[
        "811", "Rental Expenses"],[
        "811", "مصاريف إيجارات"],[
        "812", "Real Estate"],[
        "812", "عقار"],[
        "813", "Taxes"],[
        "813", "ضرائب"],[
        "814", "Fees"],[
        "814", "رسوم"],[
        "815", "Commissions"],[
        "815", "عمولات"],[
        "816", "Franchise and License Fees"],[
        "816", "رسوم الرخص والامتياز"],[
        "817", "Cheque Collection"],[
        "817", "تحصيل الشيكات خارج جلسة المقاصة"],[
        "818", "Membership Fees"],[
        "818", "رسوم الاشتراكات والعضويات"]
    ],
	"9":[[
        "901", "Municipality Funds"],[
        "901", "التمويل المقدم للبلديات"],[
        "902", "Government Funds"],[
        "902", "التمويل المقدم للحكومة"],[
        "903", "Private Sector Funds"],[
        "903", "التمويل المقدم للقطاع الخاص"],[
        "904", "External Incoming Funds"],[
        "904", "التمويل الوارد من حسابات في الخارج"]
    ],
	"10":[[
		"1001", "International Comm+Embassies Rem"],[
        "1001", "تحويلات السفارات والهيئات الدولية"],[
        "1002", "Permanent Diplomatic Missions"],[
        "1002", "البعثات الدبلوماسية الدائمة"],[
        "1003", "Temporary Diplomatic Missions"],[
        "1003", "البعثات الدبلوماسية المؤقتة"],[
        "1004", "Jordanian Embassies Income"],[
        "1004", "تحويلات السفارات الاردنية"]
    ],
	"11":[[
        "1101", "Long-Term Loans Install/ Public Sec"],[
        "1101", "تسديد أقساط قروض طويلة الأجل/ قطاع عام"],[
        "1102", "Term Loans int Install/Public Sec"],[
        "1102", "تسديد فوائد قروض طويلة الأجل/ قطاع عام"],[
        "1103", "Short-Term Loans Install/Public Sec"],[
        "1103", "تسديد أقساط قروض قصيرة الأجل/ قطاع عام"],[
        "1104", "ShortTerm Loan Install/Public Sec"],[
        "1104", "تسديد فوائد قروض قصيرة الأجل/ قطاع عام"],[
        "1105", "Long-Term Loans Install/Private Sec"],[
        "1105", "تسديد أقساط قروض طويلة الأجل/ قطاع خاص"],[
        "1106", "L-Term Loans int Instal/Public Sec"],[
        "1106", "تسديد فوائد قروض طويلة الأجل/ قطاع خاص"],[
        "1107", "ShortTerm Loan  Install/Privat  Sec"],[
        "1107", "تسديد أقساط قروض قصيرة الأجل/ قطاع خاص"],[
        "1108", "Sh-Term Loan int Install/Privat Sec"],[
        "1108", "تسديد فوائد قروض قصيرة الأجل/ قطاع خاص"],[
        "1109", "loans Install Against Gov Guarantee"],[
        "1109", "تسديد أقساط القروض بكفالة الحكومة"],[
        "1110", "Loans Int Install Against Gov Guar"],[
        "1110", "تسديد فوائد القروض بكفالة الحكومة"],[
        "1111", "Credit Card Payment "],[
        "1111", "دفعة بطاقات الإئتمان"],[
        "1112", "Personal Loan Payment"],[
        "1112", "تسديد قروض شخصية"]
    ],
	"12":[[
        "1201", "Rerouting"],[
        "1201", "إعادة تحويل(حوالةصادرةمقابل حوالة واردة)"],[
        "1202", "Scientific Research Support"],[
        "1202", "دعم البحث العلمي"]
    ]
}]
];



  var langSelected = kony.store.getItem("langPrefObj");
  var Language = langSelected.toUpperCase();
  var masterData =[];
  var i;
  if(Language == "EN"){
    i=0;
  }
  else{
    i=1;
  }
  var tempData = null;
  frmPOT.segFre.removeAll();
  kony.print("subCategory ::"+subCategory);
  if(subCategory !== "" && subCategory !== null){
  	tempData = arrList[1][0][subCategory];
    frmPOT.lblTitle.text = geti18Value("i18n.transfer.transfersubpurpose");
  }else{
 	tempData = arrList[0];
    frmPOT.lblTitle.text = geti18Value("i18n.transfer.transfermainpurpose");
  }
  kony.print("temp Data ::"+JSON.stringify(tempData));
  for(;i<tempData.length;i=i+2){
    frmPOT.segFre.addAll([{lblKey:tempData[i][0], lblfrequency:tempData[i][1]}]);  
  }

  frmPOT.show();
}