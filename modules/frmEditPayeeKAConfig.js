var frmEditPayeeKAConfig = {
    "formid": "frmEditPayeeKA",
    "frmEditPayeeKA": {
        "entity": "Payee",
        "objectServiceName": "RBObjects",
        "objectServiceOptions" : {"access":"online"},
    },
    "newPayeeNameTextfield": {
        "fieldprops": {
            "entity": "Payee",
            "field": "companyName",
            "widgettype": "TextField"
        }
    },
	    "CopynewPayeeNameTextfield07a3d8dc6f56e4f": {
        "fieldprops": {
            "entity": "Payee",
            "field": "payeeNickName",
            "widgettype": "TextField"
        }
    },
	    "newPayeeAccountNumberTextField": {
        "fieldprops": {
            "entity": "Payee",
            "field": "accountNumber",
            "widgettype": "TextField"
        }
    },
	    "CopyusernameTextField0cec050157b0f48": {
        "fieldprops": {
            "entity": "Payee",
            "field": "zipCode",
            "widgettype": "TextField"
        }
    },
   "CopyusernameTextField0f7d7f563446f42" : {
        "fieldprops": {
            "entity": "Payee",
            "field": "payeeId",
            "widgettype": "TextField"
        }
   }
}