function onTouchEndNextBulkPaymentConfirmation(eventobject, x, y) {
    return AS_Label_fa017bcba5074991b0526f59eaf371c1(eventobject, x, y);
}

function AS_Label_fa017bcba5074991b0526f59eaf371c1(eventobject, x, y) {
    if (frmBulkPaymentKA.lblHiddenSelBillsAmt.text > 0) {
        frmBulkPaymentKA.tbxAmount.text = frmBulkPaymentKA.lblHiddenSelBillsAmt.text;
        frmBulkPaymentKA.tbxAmount.setEnabled(false);
        frmBulkPaymentKA.flxConfirmPayment.isVisible = true;
        frmBulkPaymentKA.flxMainContainer.isVisible = false;
        frmBulkPaymentKA.lblNextPayNow.setEnabled(true);
    } else customAlertPopup(geti18Value("i18n.common.alert"), geti18Value("i18n.bills.bulkPaymentPay"), popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
    /*if (frmBulkPaymentKA.lblHiddenSelBillsAmt.text === '0' || frmBulkPaymentKA.lblHiddenSelBillsAmt.text === 0)
      {
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.bills.bulkPaymentPay"), popupCommonAlertDimiss, "");
      }*/
}