function AS_animateRegisterPasswordLabelDownIphone(eventobject, changedtext) {
    return AS_TextField_bab41adf25d1428d8d71724205585ab6(eventobject, changedtext);
}

function AS_TextField_bab41adf25d1428d8d71724205585ab6(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidPassword(frmEnrolluserLandingKA.tbxPasswordKA.text)) {
        frmEnrolluserLandingKA.flxUnderlinePassword.skin = "sknFlxGreenLine";
        frmEnrolluserLandingKA.flxPasswordValidation.isVisible = false;
        passwordFlag = true;
    } else {
        frmEnrolluserLandingKA.flxPasswordValidation.isVisible = true;
        frmEnrolluserLandingKA.flxUnderlinePassword.skin = "sknFlxOrangeLine";
        passwordFlag = false;
    }
    animateLabel("DOWN", "lblPassword", frmEnrolluserLandingKA.tbxPasswordKA.text);
}