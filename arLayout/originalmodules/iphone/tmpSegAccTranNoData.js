function initializetmpSegAccTranNoData() {
    flxNoData = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "63dp",
        "id": "flxNoData",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxsegBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNoData.setDefaultUnit(kony.flex.DP);
    var lblNoData = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNoData",
        "isVisible": true,
        "left": "15dp",
        "skin": "lblSegName",
        "text": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
        "top": "43%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxNoData.add(lblNoData);
}