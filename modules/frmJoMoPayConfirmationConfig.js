var frmJoMoPayConfirmationConfig={
	"formid":"frmJoMoPayConfirmation",
	"frmJoMoPayConfirmation":{
		"entity":"Transactions",
		"objectServiceName":"RBObjects",
		"objectServiceOptions" : {"access":"online"}
	},
	"lblTransactionType":{
		"fieldprops": {
			"entity":"Transactions",
			"field":"transactionType",
			"widgettype":"Label"
		}
	},
	"lblFromAccountNumber":{
		"fieldprops": {
			"entity":"Transactions",
			"field":"fromAccountNumber",
			"widgettype":"Label"
		}
	},
	"lblSourceBranchCode":{
		"fieldprops": {
			"entity":"Transactions",
			"field":"SourceBranchCode",
			"widgettype":"Label"
		}
	},
	"lblJomopayType":{
		"fieldprops": {
			"entity":"Transactions",
			"field":"jomopayType",
			"widgettype":"Label"
		}
	},
	"lblPhoneNo":{
		"fieldprops": {
			"entity":"Transactions",
			"field":"toAccountName",
			"widgettype":"Label"
		}
	},
	"lblAmount":{
		"fieldprops": {
			"entity":"Transactions",
			"field":"amount",
			"widgettype":"Label"
		}
	},
	"lblFee":{
		"fieldprops": {
			"entity":"Transactions",
			"field":"fees",
			"widgettype":"Label"
		}
	},
	"lblTransferFlag":{
		"fieldprops": {
			"entity":"Transactions",
			"field":"TransferFlag",
			"widgettype":"Label"
		}
	}
}