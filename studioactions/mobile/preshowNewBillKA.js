function preshowNewBillKA(eventobject) {
    return AS_Form_d8093a4ba4d745c4908d0a6853f2db03(eventobject);
}

function AS_Form_d8093a4ba4d745c4908d0a6853f2db03(eventobject) {
    if (kony.store.getItem("langPrefObj") === "en") kony.boj.lang = "eng";
    else kony.boj.lang = "ara";
    animateLabel("DOWN", "lblAmount", frmNewBillKA.tbxAmount.text);
    animateLabel("DOWN", "lblBillerNumber", frmNewBillKA.tbxBillerNumber.text);
    kony.print("Biller Reset Flag ::" + kony.boj.BillerResetFlag);
    if (kony.boj.BillerResetFlag) {
        resetBiller();
        kony.boj.resetSelectedItemsBiller("PrePaid");
        kony.boj.resetSelectedItemsBiller("PostPaid");
        kony.boj.selectedBillerType = "PostPaid";
        kony.boj.BillerResetFlag = false;
    }
    if (frmNewBillKA.lblPaymentMode.text == "Accounts") {
        frmNewBillKA.lblPaymentMode.skin = "sknLblNextDisabled"
    }
    nextFromNewBillScreen();
    var amount = frmNewBillKA.tbxAmount.text;
    if (amount === null || amount === "" || amount == "." || (Number(amount) < 0.01) || amount === 0.00) {} else {
        frmNewBillKA.tbxAmount.text = setDecimal(frmNewBillKA.tbxAmount.text.replace(/,/g, ""), 3);
    }
    changeUnderlineskin();
    frmNewBillKA.flxMain.showFadingEdges = false;
    frmNewBillKA.flxInnerPopup.showFadingEdges = false;
    //frmNewBillKA.btnBillsPayAccounts.text = "t"; // hassan pay from credit card
    //frmNewBillKA.btnBillsPayCards.text = "s"; // hassan pay from credit card
}