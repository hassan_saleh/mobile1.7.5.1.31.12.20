function initializeCopyFBox0103104335ef34f() {
    CopyFBox0103104335ef34f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "CopyFBox0103104335ef34f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox0103104335ef34f.setDefaultUnit(kony.flex.DP);
    var lblServiceList = new kony.ui.Label({
        "id": "lblServiceList",
        "isVisible": true,
        "left": "0dp",
        "minHeight": "20dp",
        "skin": "sknNumber",
        "text": "Sun Closed",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    CopyFBox0103104335ef34f.add(lblServiceList);
}